﻿using Calculator.Bl.Contracts;
using System;

namespace Calculator.Bl.Tools.Trigonometry
{
    public class CosTrigonometry:ITrigonometry
    {
        public float Trigonometry(float firstNumber)
        { 
            return (float)Math.Cos(firstNumber*Math.PI / 180d);
        }
    }
}