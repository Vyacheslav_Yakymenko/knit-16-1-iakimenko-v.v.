﻿using Calculator.Bl.Contracts;
using System;

namespace Calculator.Bl.Tools.Trigonometry
{
    public class CtgTrigonometry:ITrigonometry
    {
        public float Trigonometry(float firstNumber)
        {
            return 1f/(float)Math.Tan(firstNumber * Math.PI / 180d);
        }
    }
}
