﻿using Calculator.Bl.Contracts;
using System;

namespace Calculator.Bl.Tools.Trigonometry
{
    public class TanTrigonometry:ITrigonometry
    {
        public float Trigonometry(float firstNumber)
        {
            return (float)Math.Tan(firstNumber * Math.PI / 180d);
        }
    }
}
