﻿using Calculator.Bl.Contracts;
using System;

namespace Calculator.Bl.Tools.Trigonometry
{
    public class SinTrigonometry:ITrigonometry
    {
        public float Trigonometry(float firstNumber)
        {
            return (float)Math.Sin(firstNumber * Math.PI / 180d);
        }
    }
}
