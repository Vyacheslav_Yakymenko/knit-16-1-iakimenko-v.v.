﻿using Calculator.Bl.Contracts;
using System;

namespace Calculator.Bl.Tools.Operation
{
    public class SubtrOperation :IOperation
    {
       public float Operation(float firstNumber, float secondNumber)
        {
            return firstNumber - secondNumber;
        }
    }
}
