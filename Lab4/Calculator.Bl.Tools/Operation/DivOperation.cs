﻿using Calculator.Bl.Contracts;

namespace Calculator.Bl.Tools.Operation
{
    public class DivOperation:IOperation
    {
        public float Operation(float firstNumber, float secondNumber)
        {
            return firstNumber / secondNumber;
        }
    }
}
