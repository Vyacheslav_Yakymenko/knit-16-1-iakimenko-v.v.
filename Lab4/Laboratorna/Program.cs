﻿
using System;
using Lab.Manager;

namespace Laboratorna4
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Create();
            }
            catch(OverflowException)
            {
                Console.WriteLine("\aError!(OverflowException)");
            }
            catch(Exception)
            {
                Console.WriteLine("\aError!");
            }
            finally
            {
                Console.ReadKey();
            }
        }
        static void Create()
        {
            Manager ExampleCalculator = new Manager();
            ExampleCalculator.DoOperation();
        }
    }
}
