﻿using Calculator.Bl.Contracts;
using Calculator.Bl.Tools.Operation;
using Calculator.Bl.Tools.Trigonometry;
using Lab.Enum;
using System;


namespace Lab.Manager
{
    class Manager
    {
        private ChoiceOperation operValue;
        private float firstNumber;
        private float secondNumber;
        public Manager()
        {
            Choice();
        }
        private void Choice()
        {
            Console.WriteLine("Choice:\n1-Mathematical operations\n2-Trigonometry");
            this.operValue = (ChoiceOperation)Int32.Parse(Console.ReadLine());
            if (operValue == ChoiceOperation.Operation)
            {
                Console.Write("a = ");
                this.firstNumber = Single.Parse(Console.ReadLine());
                Console.Write("b = ");
                this.secondNumber = Single.Parse(Console.ReadLine());
            }
            else if (operValue == ChoiceOperation.Trigonometry)
            {
                Console.Write("a = ");
                this.firstNumber = Single.Parse(Console.ReadLine());
            }
            else
                Console.WriteLine("\aError!(1-2)");
        }
        private void СallOperation(IOperation example)
        {
            string rezult = example.Operation(this.firstNumber, this.secondNumber).ToString();
            Console.WriteLine("Result:"+rezult);
        }
        private void СallOperation(ITrigonometry example)
        {
            string rezult = example.Trigonometry(this.firstNumber).ToString();
            Console.WriteLine("Result:"+rezult);
        }
        private void MathOperation()
        {
            Console.WriteLine("1-a/b\n2-a*b\n3-a-b\n4-a+b");
            EnumOperation key = (EnumOperation)Int32.Parse(Console.ReadLine());
            switch (key)
            {
                case EnumOperation.One:
                    СallOperation(new DivOperation());
                    break;  
                case EnumOperation.Two:
                    СallOperation(new MultOperation());
                    break;         
                case EnumOperation.Three:
                    СallOperation(new SubtrOperation());
                    break;
                case EnumOperation.Four:
                    СallOperation(new SumOperation());
                    break;
                default:
                    Console.WriteLine("\aError!(1 - 4)");
                    break;
            }
        }
        private void TrigOperation()
        {
            Console.WriteLine("1-cos(a)\n2-ctg(a)\n3-sin(a)\n4-tg(a)\na - degrees");
            EnumOperation key = (EnumOperation)Int32.Parse(Console.ReadLine());
            switch (key)
            {
                case EnumOperation.One:
                    СallOperation(new CosTrigonometry());
                    break;
                case EnumOperation.Two:
                    СallOperation(new CtgTrigonometry());
                    break;
                case EnumOperation.Three:
                    СallOperation(new SinTrigonometry());
                    break;
                case EnumOperation.Four:
                    СallOperation(new TanTrigonometry());
                    break;
                default:
                    Console.WriteLine("\aError!(1-4)");
                    break;
            }
        }
        public void DoOperation()
        {
            if(this.operValue== ChoiceOperation.Operation)
            {
                MathOperation();
            }
            else if(this.operValue == ChoiceOperation.Trigonometry)
            {
                TrigOperation();
            }
        }

    }
}
