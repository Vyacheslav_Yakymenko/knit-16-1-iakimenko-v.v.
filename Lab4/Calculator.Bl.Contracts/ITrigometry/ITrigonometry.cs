﻿
namespace Calculator.Bl.Contracts
{
    public interface ITrigonometry
    {
         float Trigonometry(float firstNumber);
    }
}
