﻿

namespace Calculator.Bl.Contracts
{
    public interface IOperation
    {
        float Operation(float firstNumber, float secondNumber=0f);
    }
}
