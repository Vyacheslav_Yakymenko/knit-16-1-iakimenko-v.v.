﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_Calculator
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            
        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Button a = (Button)sender;
            op.Text += a.Content.ToString();
        }

        private void Result_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Result();
            }
            catch (Exception exc)
            {
                op.Text = "Error!";
            }
        }

        private void Result()
        {
            String op;
            int iOp = 0;
            if (op.Text.Contains("+"))
            {
                iOp = op.Text.IndexOf("+");
            }
            else if (op.Text.Contains("-"))
            {
                iOp = op.Text.IndexOf("-");
            }
            else if (op.Text.Contains("*"))
            {
                iOp = op.Text.IndexOf("*");
            }
            else if (op.Text.Contains("/"))
            {
                iOp = op.Text.IndexOf("/");
            }
            else
            {  
            }

            op = op.Text.Substring(iOp, 1);
            double op1 = Convert.ToDouble(op.Text.Substring(0, iOp));
            double op2 = Convert.ToDouble(op.Text.Substring(iOp + 1, op.Text.Length - iOp - 1));

            if (op == "+")
            {
                op.Text += "=" + (op1 + op2);
            }
            else if (op == "-")
            {
                op.Text += "=" + (op1 - op2);
            }
            else if (op == "*")
            {
                op.Text += "=" + (op1 * op2);
            }
            else
            {
                op.Text += "=" + (op1 / op2);
            }
        }

        private void Off_Click_1(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Del_Click(object sender, RoutedEventArgs e)
        {
            op.Text = "";
        }

        private void R_Click(object sender, RoutedEventArgs e)
        {
            if (op.Text.Length > 0)
            {
                op.Text = op.Text.Substring(0, op.Text.Length - 1);
            }
        }
    }
}
/*
 * yakymenko knit-16-1
 * /*
