﻿using System;

namespace Сalculator.Application
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Manager firstCalculator = new Manager();
                firstCalculator.Result();
            }
            catch(DivideByZeroException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception)
            {
                Console.WriteLine("Error!\a");
            }
            finally
            {
                Console.ReadKey();
            }
        }
    }
}
