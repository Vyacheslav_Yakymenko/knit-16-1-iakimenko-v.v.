﻿using System;

namespace Lab1
{
    class Program
    {   
        static void Main(string[] args)
        {
            try
            {
                Student Bob = new Student();
                Bob.WritVal();
                Console.WriteLine(Jack);
                Student Bob = new Student
                {
                    FistName = "Bob",
                    LastName = "March",
                    GroupName = "KNIT-16-1",
                    UnivName = "TOH",
                    _Age = -22,
                };
                Console.WriteLine(John);
            }
            catch(FormatException e)
            {
                Console.WriteLine("\aError!\t"+e.Message);
            }
            catch(Exception ex)
            {
                Console.WriteLine("\aError!\t"+ex.Message);
            }
            finally
            {
                Console.ReadKey();
            }
        }
    }
}
