﻿using System;

namespace MyListBL
{
    public class ChangValueEventArgs<TValue>:EventArgs
    {
        private  TValue _value;
        public TValue Value
        {
            get { return _value; }
            set { _value = value; }
        }
        public ChangValueEventArgs(TValue value)
        {
            _value = value;
        }
    }
}
