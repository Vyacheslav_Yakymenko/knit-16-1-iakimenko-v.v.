﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sudoku2 {
    /// <summary>
    /// Implements a 16x16 sudoku board
    /// </summary>
    public sealed class SudBoard16 : SudBoard {
        
        /// <summary>
        /// Class ctor
        /// </summary>
        public SudBoard16() : base(4) {
        }
        
        /// <summary>
        /// Copy ctor
        /// </summary>
        /// <param name="brd">  Template board</param>
        public SudBoard16(SudBoard16 brd) : this() {
            brd.CopyTo(this);
        }

        /// <summary>
        /// Clone the sudoku board
        /// </summary>
        /// <returns>
        /// Clone of the sudoku board
        /// </returns>
        public override SudBoard Clone() {
            return(new SudBoard16(this));
        }
        
        /// <summary>
        /// Checks if a value is valid for the specified cell. A valid value doesn't duplicate a value in a square, line or column.
        /// </summary>
        /// <param name="iPos"> Position of the cell (0..max)</param>
        /// <param name="iVal"> Value of the cell (1..max)</param>
        /// <returns>true if valid, false if not.
        /// </returns>
        protected override bool IsValueValid(int iPos, int iVal) {
#if DEBUG
            if (iVal <= 0 || iVal > m_iLineSize || iPos >= m_iBoardSize || m_arrBoardVal[iPos] != 0) {
                throw new System.ApplicationException("Coding error. Out of bound");
            }
#endif
            return((m_LineValCount[iPos >> 4, iVal]             == 0  &&
                    m_ColValCount[iPos & 15,  iVal]             == 0  &&
                    m_SqrValCount[m_arrPosToSqr[iPos], iVal]    == 0) ? true : false);
        }
    } // Class
} // Namespace
