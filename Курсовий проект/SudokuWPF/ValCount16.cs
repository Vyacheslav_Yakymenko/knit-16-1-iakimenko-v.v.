﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sudoku2 {
    /// <summary>
    /// Handle the count of each values for lines/columns or squares (16x16 board version)
    /// </summary>
    public sealed class ValCount16 : ValCount {
        /// <summary>
        /// ctor
        /// </summary>
        public ValCount16() : base(16) {
        }

        /// <summary>
        /// Copy ctor
        /// </summary>
        /// <param name="valCount"> Template</param>
        public ValCount16(ValCount9 valCount) : this() {
            valCount.CopyTo(this);
        }

        /// <summary>
        /// Return/set the count of this value for the specified slot
        /// </summary>
        /// <param name="iPos"> Group position</param>
        /// <param name="iVal"> Value to  query</param>
        public override int this[int iPos, int iVal] {
            get {
                return(m_arrCount[(iPos << 4) + iVal - 1]);
            }
        }

        /// <summary>
        /// Add or remove a reference to a value for a position
        /// </summary>
        /// <param name="iPos">     Group position</param>
        /// <param name="iVal">     Value</param>
        /// <param name="iInc">     -1 or 1</param>
        public override void Add(int iPos, int iVal, int iInc) {
            int iAbsPos;
            int iOldVal;
            
#if DEBUG
            if (iInc != -1 && iInc != 1) {
                throw new System.ApplicationException("Coding error. iInc must be 1 or -1");
            }
#endif                        
            iAbsPos = (iPos << 4) + iVal - 1;
            iOldVal = m_arrCount[iAbsPos];
            if (iInc == -1 && iOldVal == 1) {
                m_arrUsedSlotPerGroup[iPos]--;
            } else if (iInc == 1 && iOldVal == 0) {
                m_arrUsedSlotPerGroup[iPos]++;
            }
            m_arrCount[iAbsPos] += (short)iInc;
        }
    } // Class
} // Namespace
