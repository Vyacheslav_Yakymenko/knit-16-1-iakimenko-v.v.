﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Sudoku2;

namespace SudokuWPF {
    /// <summary>
    /// Implements a control providing user interface for a single sudoku board
    /// </summary>
    /// TODO:
    ///     Extend the size of the ValuePicker
    public partial class SudControl : UserControl {

        #region Types Definition

        /// <summary>
        /// Type of marker around a cell
        /// </summary>
        public enum MarkerE {
            /// <summary>No marker</summary>
            None    =   0,
            /// <summary>Green marker</summary>
            Green   =   1,
            /// <summary>Blue marker</summary>
            Blue    =   2,
            /// <summary>Red marker</summary>
            Red     =   3
        };
        
        /// <summary>
        /// Type of symbols used to display the board value
        /// </summary>
        public enum SymbolTypeE {
            /// <summary>Use number for 9x9 and 16x16</summary>
            Number          =   0,
            /// <summary>Use letter for 9x9 and 16x16</summary>
            Letter          =   1,
            /// <summary>Use number for 9x9 and letter for 16x16</summary>
            LetterFor16x16    =   2,
            /// <summary>Use symbol</summary>
            Symbol          =   3
        };

        /// <summary>
        /// Input method
        /// </summary>
        public enum InputMethodE {
            /// <summary>No method</summary>
            None    = 0,
            /// <summary>Alternating the value with a click</summary>
            Click   = 1,
            /// <summary>Using the value picker control</summary>
            Picker  = 2
        };

        /// <summary>
        /// Check if any kind of help as been used to solve the board
        /// </summary>
        public enum CheatInfoE {
            /// <summary>No help</summary>
            None    = 0,
            /// <summary>Hint has been demanded</summary>
            Hint    = 1,
            /// <summary>Board has been solved</summary>
            Solve   = 2
        };

        /// <summary>
        /// Argument for the CellClick event
        /// </summary>
        public class CellClickEventArg : EventArgs {
            /// <summary>Cell being clicked</summary>
            public CellPos              Cell;
            /// <summary>Mouse button</summary>
            public MouseButton          Button;
            /// <summary>
            /// Public ctor
            /// </summary>
            /// <param name="button">   Button</param>
            /// <param name="cell">     Cell being clicked</param>
            public          CellClickEventArg(CellPos cell, MouseButton button) { Cell = cell; Button = button;  }
        }

        #endregion

        #region Cell Implementation
        
        /// <summary>
        /// Implementation of a cell
        /// </summary>
        public class Cell : Border {

            /// <summary>Standard border thickness if visible</summary>
            private static Thickness    m_thicknessStd = new Thickness(2,2,2,2);
            /// <summary>Standard border thickness if visible</summary>
            private static Thickness    m_thicknessHidden = new Thickness(0,0,0,0);
            /// <summary>Red solid brush</summary>
            private static Brush        m_brushRed = new SolidColorBrush(Color.FromRgb(255, 0, 0));
            /// <summary>Green solid brush</summary>
            private static Brush        m_brushGreen = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            /// <summary>Blue solid brush</summary>
            private static Brush        m_brushBlue = new SolidColorBrush(Color.FromRgb(0, 0, 255));
            /// <summary>Marker</summary>
            private Border              m_borderMarker;
            /// <summary>Cell father</summary>
            private SudControl          m_sudControl;
            /// <summary>Contained textblock</summary>
            private TextBlock           m_textBlock;
            /// <summary>Cell X position</summary>
            private CellPos             m_tCellPos;
            /// <summary>Currently displayed marker</summary>
            private MarkerE             m_eCurMarker;
            /// <summary>Toggle green marker around this cell</summary>
            private MenuItem            m_miSetGreenMarker;
            /// <summary>Toggle blue marker around this cell</summary>
            private MenuItem            m_miSetBlueMarker;
            /// <summary>Toggle red marker around this cell</summary>
            private MenuItem            m_miSetRedMarker;
            /// <summary>Toogle red marker for all cells with this cell value</summary>
            private MenuItem            m_miSetRedMarkerForThisValue;
            /// <summary>Visualize the cell value</summary>
            private MenuItem            m_miVisualizeThisValue;
            /// <summary>Normal font  size</summary>
            private double              m_dNormalFontSize;
            /// <summary>Hint font size</summary>
            private double              m_dHintFontSize;
            
            /// <summary>
            /// Class ctor
            /// </summary>
            /// <param name="sudControl">   Cell father control</param>
            /// <param name="iXPos">        Cell X Position</param>
            /// <param name="iYPos">        Cell Y Position</param>
            public Cell(SudControl sudControl, int iXPos, int iYPos) { 
                m_sudControl                    = sudControl;
                m_tCellPos                      = new CellPos(iXPos, iYPos);
                Width                           = Double.NaN;
                Height                          = Double.NaN;
                BorderThickness                 = m_thicknessHidden;
                m_borderMarker                  = new Border();
                m_borderMarker.Margin           = m_thicknessStd;
                m_borderMarker.CornerRadius     = new CornerRadius(30);
                m_borderMarker.Width            = Double.NaN;
                m_borderMarker.Height           = Double.NaN;
                m_borderMarker.BorderThickness  = m_thicknessHidden;
                m_textBlock                     = new TextBlock();
                m_textBlock.TextAlignment       = TextAlignment.Center;
                m_textBlock.VerticalAlignment   = VerticalAlignment.Center;
                m_textBlock.Width               = Double.NaN;
                m_textBlock.Height              = Double.NaN;
                m_borderMarker.Child            = m_textBlock;
                Child                           = m_borderMarker;
                m_dNormalFontSize               = m_textBlock.FontSize;
                m_dHintFontSize                 = m_textBlock.FontSize / 4;
                RefreshState();
            }

            /// <summary>
            /// Update a menu item header so it represent a Set or Remove item
            /// </summary>
            /// <param name="mi"></param>
            /// <param name="bRemove"></param>
            private void SpiceHeader(MenuItem mi, bool bRemove) {
                if (bRemove) {
                    mi.Header = ((string)mi.Header).Replace("Set ", "Remove ");
                } else {
                    mi.Header = ((string)mi.Header).Replace("Remove ", "Set ");
                }
            }
            
            /// <summary>
            /// Attach the context menu to this cell
            /// </summary>
            private void AttachContextMenu() {
                MarkerE     eMarker;
                
                m_textBlock.ContextMenu             = (ContextMenu)FindResource("ctxMenu");
                m_textBlock.ContextMenuClosing     += new ContextMenuEventHandler(m_textBlock_ContextMenuClosing);
                m_miSetGreenMarker                  = m_textBlock.ContextMenu.Items[0] as MenuItem;
                m_miSetBlueMarker                   = m_textBlock.ContextMenu.Items[1] as MenuItem;
                m_miSetRedMarker                    = m_textBlock.ContextMenu.Items[2] as MenuItem;
                m_miSetRedMarkerForThisValue        = m_textBlock.ContextMenu.Items[3] as MenuItem;
                m_miVisualizeThisValue              = m_textBlock.ContextMenu.Items[4] as MenuItem;
                m_miSetGreenMarker.Click           += new RoutedEventHandler(MenuItem_Click);
                m_miSetBlueMarker.Click            += new RoutedEventHandler(MenuItem_Click);
                m_miSetRedMarker.Click             += new RoutedEventHandler(MenuItem_Click);
                m_miSetRedMarkerForThisValue.Click += new RoutedEventHandler(MenuItem_Click);
                m_miVisualizeThisValue.Click       += new RoutedEventHandler(MenuItem_Click);
                eMarker                             = m_sudControl.GetMarker(m_tCellPos);
                m_miVisualizeThisValue.IsEnabled    = (m_sudControl.Board[m_tCellPos] != 0);
                SpiceHeader(m_miSetGreenMarker,             (eMarker == MarkerE.Green));
                SpiceHeader(m_miSetBlueMarker,              (eMarker == MarkerE.Blue));
                SpiceHeader(m_miSetRedMarker,               (eMarker == MarkerE.Red));
                SpiceHeader(m_miSetRedMarkerForThisValue,   (eMarker == MarkerE.Red));
            }

            /// <summary>
            /// Detach the context menu from this cell
            /// </summary>
            private void DetachContextMenu() {
                m_miSetGreenMarker.Click           -= new RoutedEventHandler(MenuItem_Click);
                m_miSetBlueMarker.Click            -= new RoutedEventHandler(MenuItem_Click);
                m_miSetRedMarker.Click             -= new RoutedEventHandler(MenuItem_Click);
                m_miSetRedMarkerForThisValue.Click -= new RoutedEventHandler(MenuItem_Click);
                m_miVisualizeThisValue.Click       -= new RoutedEventHandler(MenuItem_Click);
                m_textBlock.ContextMenu             = null;
            }
            
            /// <summary>
            /// Refresh the text value
            /// </summary>
            private void RefreshTextVal() {
                SymbolTypeE eSymbolType;
                int         iVal;
                int         iRectCount;
                bool        bShowHint;

                eSymbolType = m_sudControl.SymbolType;
                iVal        = m_sudControl.Board[m_tCellPos];
                iRectCount  = m_sudControl.Board.LineSize;
                bShowHint   = m_sudControl.ShowHint;
                if (iVal == 0) {
                    m_textBlock.Text = (bShowHint) ? m_sudControl.Board.GetPossibleValCount(m_tCellPos).ToString() : "";
                } else {
                    if (eSymbolType == SymbolTypeE.Symbol) {
                        m_textBlock.Text = ((Char)(iVal + 63)).ToString();
                    } else {
                        m_textBlock.Text = (eSymbolType == SymbolTypeE.Number || (iRectCount == 9 && eSymbolType == SymbolTypeE.LetterFor16x16)) ? iVal.ToString() : ((Char)(iVal - 1 + (int)'A')).ToString();
                    }
                }
            }
            
            /// <summary>
            /// Refresh the marker
            /// </summary>
            public void RefreshMarker() {
                MarkerE eCellMarker;
                
                eCellMarker = m_sudControl.GetMarker(m_tCellPos);
                if (m_eCurMarker != eCellMarker) {
                    switch(eCellMarker) {
                    case MarkerE.None:
                        m_borderMarker.BorderThickness = m_thicknessHidden;
                        break;
                    case MarkerE.Red:
                        m_borderMarker.BorderThickness = m_thicknessStd;
                        m_borderMarker.BorderBrush     = m_brushRed;
                        break;
                    case MarkerE.Green:
                        m_borderMarker.BorderThickness = m_thicknessStd;
                        m_borderMarker.BorderBrush     = m_brushGreen;
                        break;
                    case MarkerE.Blue:
                        m_borderMarker.BorderThickness = m_thicknessStd;
                        m_borderMarker.BorderBrush     = m_brushBlue;
                        break;
                    }
                    m_eCurMarker = eCellMarker;
                }
            }

            /// <summary>
            /// Refresh the font/color/value states
            /// </summary>
            public void RefreshState() {
                SudBoard.ValueTypeE eValType;
                SymbolTypeE         eSymbolType;
                FontFamily          fontFamily;
                FontWeight          fontWeight;
                Brush               brush;
                double              dFontSize;
                bool                bShowHint;

                eValType    = m_sudControl.Board.ValueType(m_tCellPos);
                eSymbolType = m_sudControl.SymbolType;
                bShowHint   = m_sudControl.ShowHint;
                if (eValType == SudBoard.ValueTypeE.Fix) {
                    fontFamily  = (eSymbolType == SymbolTypeE.Symbol) ? m_sudControl.SymbolValueFontFamily : m_sudControl.FixValueFontFamily;
                    fontWeight  = FontWeights.Bold;
                    brush       = m_sudControl.FixValueBrush;
                    dFontSize   = m_dNormalFontSize;
                } else if (eValType == SudBoard.ValueTypeE.User || !bShowHint) {
                    fontFamily  = (eSymbolType == SymbolTypeE.Symbol) ? m_sudControl.SymbolValueFontFamily : m_sudControl.UserValueFontFamily;
                    fontWeight  = FontWeights.Normal;
                    brush       = m_sudControl.UserValueBrush;
                    dFontSize   = m_dNormalFontSize;
                } else {
                    fontFamily  = m_sudControl.HintValueFontFamily;
                    fontWeight  = FontWeights.Normal;
                    brush       = m_sudControl.HintValueBrush;
                    dFontSize   = m_dHintFontSize;
                }
                if (m_textBlock.FontFamily != fontFamily) {
                    m_textBlock.FontFamily = fontFamily;
                }
                if (m_textBlock.Foreground != brush) {
                    m_textBlock.Foreground = brush;
                }
                if (m_textBlock.FontWeight != fontWeight) {
                    m_textBlock.FontWeight = fontWeight;
                }
                if (m_textBlock.FontSize != dFontSize) {
                    m_textBlock.FontSize = dFontSize;
                }
                RefreshMarker();
                RefreshTextVal();
            }

            /// <summary>
            /// Hide the value picker
            /// </summary>
            /// <param name="sudControl">   Sudoku control</param>
            public static void HideValuePicker(SudControl sudControl) {
                Cell   cellParent;
                
                cellParent = sudControl.m_valPickerCtl.Parent as Cell;
                if (cellParent != null) {
                    sudControl.GetColorMarkerMaskFromValuePicker(cellParent.m_tCellPos);
                    cellParent.Child = cellParent.m_borderMarker;
                }
            }
            
            /// <summary>
            /// Show the value picker
            /// </summary>
            public void ShowValuePicker() {
                ValPickerControl    valuePicker;
                
                valuePicker = m_sudControl.m_valPickerCtl;
                if (valuePicker.Parent != this) {
                    HideValuePicker(m_sudControl);
                    m_sudControl.SetColorMarkerMaskToValuePicker(m_tCellPos);
                    valuePicker.ValueMask   = (m_sudControl.OnlyValidMove || m_sudControl.BoardDesignMode) ? m_sudControl.Board.GetPossibleValMask(m_tCellPos) : -1;
                    valuePicker.RefreshCellStates();
                    Child                   = valuePicker;
                }
            }

            /// <summary>
            /// Returned the cell position of the value picker
            /// </summary>
            /// <param name="sudControl">   Sudoku Control</param>
            /// <returns></returns>
            public static CellPos ValuePickerPos(SudControl sudControl) {
                CellPos cellPosRetVal;
                Cell    cellParent;
                
                cellParent      = sudControl.m_valPickerCtl.Parent as Cell;
                cellPosRetVal   = (cellParent != null) ? cellParent.m_tCellPos : new CellPos(-1, -1);
                return(cellPosRetVal);
            }
            
            /// <summary>
            /// Recompute the font size when the value picker size changed
            /// </summary>
            /// <param name="sizeInfo"> New size</param>
            protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo) {
                m_dNormalFontSize                   = ActualWidth / 1.5;
                m_dHintFontSize                     = m_dNormalFontSize / 4;
                m_sudControl.m_valPickerCtl.Margin  = (m_sudControl.SquareSize != 4 || m_sudControl.SymbolType != SymbolTypeE.Number) ? new Thickness(0) : new Thickness(-ActualWidth / 2);
                RefreshState();
                base.OnRenderSizeChanged(sizeInfo);
            }
            
            /// <summary>
            /// Called when the mouse enter the cell
            /// </summary>
            /// <param name="e">    Event argument</param>
            protected override void OnMouseEnter(MouseEventArgs e) {
                base.OnMouseEnter(e);
                if (m_sudControl.InputMethod == InputMethodE.Picker && m_sudControl.ShowValuePickerOnMouseMove && m_sudControl.Board[m_tCellPos] == 0) {
                    ShowValuePicker();
                }
            }

            /// <summary>
            /// Called when the mouse leave the cell
            /// </summary>
            /// <param name="e">    Event argument</param>
            protected override void OnMouseLeave(MouseEventArgs e) {
                base.OnMouseLeave(e);
                HideValuePicker(m_sudControl);
            }

            /// <summary>
            /// Intercept OnMouseDown event
            /// </summary>
            /// <param name="e">    Event argument</param>
            protected override void OnMouseDown(MouseButtonEventArgs e) {
                base.OnMouseDown(e);
                if (e.ChangedButton == MouseButton.Right && m_sudControl.AutoHandleRightClick) {
                    AttachContextMenu();
                }
            }
            
            /// <summary>
            /// Intercept OnMouseUp event
            /// </summary>
            /// <param name="e">    Event argument</param>
            protected override void OnMouseUp(MouseButtonEventArgs e) {
                bool        bDesignMode;
                bool        bOnlyValidMove;
                bool        bUser;
                bool        bRecordMove;
                
                m_sudControl.OnCellClick(new CellClickEventArg(m_tCellPos, e.ChangedButton));
                base.OnMouseUp(e);
                if (e.ChangedButton == MouseButton.Left) {
                    bDesignMode     = m_sudControl.BoardDesignMode;
                    bUser           = !bDesignMode;
                    bRecordMove     = !bDesignMode;
                    bOnlyValidMove  = m_sudControl.OnlyValidMove | bDesignMode;
                    switch(m_sudControl.InputMethod) {
                    case InputMethodE.Click:
                        if (m_sudControl.PlayNextCellVal(m_tCellPos, bUser, bRecordMove, bOnlyValidMove)) {
                            m_sudControl.OnBoardValueChanged(EventArgs.Empty);
                        }
                        break;
                    case InputMethodE.Picker:
                        if (bDesignMode) {
                            if (m_sudControl.Board[m_tCellPos] != 0) {
                                m_sudControl.PlayAt(m_tCellPos, 0, false, false, false);
                                m_sudControl.OnBoardValueChanged(EventArgs.Empty);
                            }
                            ShowValuePicker();
                        } else {
                            if (m_sudControl.Board.ValueType(m_tCellPos) == SudBoard.ValueTypeE.User) {
                                m_sudControl.PlayAt(m_tCellPos, 0, false);
                                m_sudControl.OnBoardValueChanged(EventArgs.Empty);
                                ShowValuePicker();
                            } else if (m_sudControl.Board[m_tCellPos] == 0) {
                                ShowValuePicker();
                            }
                        }
                        break;
                    case InputMethodE.None:
                        break;
                    }
                }
            }

            /// <summary>
            /// Called when the context menu is closing
            /// </summary>
            /// <param name="sender">   Sender object</param>
            /// <param name="e">        Event argument</param>
            void m_textBlock_ContextMenuClosing(object sender, ContextMenuEventArgs e) {
                DetachContextMenu();
            }

            /// <summary>
            /// Called when a menu item is clicked
            /// </summary>
            /// <param name="sender">   Sender object</param>
            /// <param name="e">        Event argument</param>
            void MenuItem_Click(object sender, RoutedEventArgs e) {
                if (sender == m_miSetGreenMarker) {
                    m_sudControl.ToggleMarker(m_tCellPos, MarkerE.Green);
                    e.Handled = true;
                } else if (sender == m_miSetBlueMarker) {
                    m_sudControl.ToggleMarker(m_tCellPos, MarkerE.Blue);
                    e.Handled = true;
                } else if (sender == m_miSetRedMarker) {
                    m_sudControl.ToggleMarker(m_tCellPos, MarkerE.Red);
                    e.Handled = true;
                } else if (sender == m_miSetRedMarkerForThisValue) {
                    m_sudControl.SetMarkerForSpecificValCell(m_sudControl.Board[m_tCellPos], (m_sudControl.m_arrMarker[m_tCellPos.X, m_tCellPos.Y] == MarkerE.Red) ? MarkerE.None : MarkerE.Red);
                    e.Handled = true;
                } else if (sender == m_miVisualizeThisValue) {
                    m_sudControl.VisualizeThisValue(m_sudControl.Board[m_tCellPos]);
                    e.Handled = true;
                }
            }
        }

        #endregion

        #region Dependency Properties

        /// <summary>
        /// Safely creates a font family
        /// </summary>
        /// <param name="strName">  Font family name</param>
        /// <returns>
        /// Font family
        /// </returns>
        public static FontFamily CreateFontFamily(string strName) {
            FontFamily  fontFamilyRetVal;

            try {
                fontFamilyRetVal = new FontFamily(strName);
            } catch(System.Exception) {
                fontFamilyRetVal = new FontFamily("Arial");
            }
            return(fontFamilyRetVal);
        }

        /// <summary>Defines the SymbolType dependency property</summary>
        public static readonly DependencyProperty   SymbolTypeProperty = DependencyProperty.Register("SymbolType",
                                                                                                     typeof(SymbolTypeE),
                                                                                                     typeof(SudControl),
                                                                                                     new PropertyMetadata(SymbolTypeE.Number, new PropertyChangedCallback(OnCellInfoChanged)));
        /// <summary>Defines the ValidMove dependency property</summary>
        public static readonly DependencyProperty   OnlyValidMoveProperty = DependencyProperty.Register("OnlyValidMove",
                                                                                                        typeof(bool),
                                                                                                        typeof(SudControl),
                                                                                                        new PropertyMetadata(false));
        /// <summary>Defines the HandleRight dependency property</summary>
        public static readonly DependencyProperty   AutoHandleRightClickProperty = DependencyProperty.Register("AutoHandleRightClick",
                                                                                                               typeof(bool),
                                                                                                               typeof(SudControl),
                                                                                                               new PropertyMetadata(true));
        /// <summary>Defines the InputMethod dependency property</summary>
        public static readonly DependencyProperty   InputMethodProperty =  DependencyProperty.Register("InputMethod",
                                                                                                       typeof(InputMethodE),
                                                                                                       typeof(SudControl),
                                                                                                       new PropertyMetadata(InputMethodE.Picker, new PropertyChangedCallback(OnInputMethodChanged)));
        /// <summary>Defines the ShowValuePicker dependency property</summary>
        public static readonly DependencyProperty   ShowValuePickerOnMouseMoveProperty = DependencyProperty.Register("ShowValuePickerOnMouseMove",
                                                                                                                     typeof(bool),
                                                                                                                     typeof(SudControl),
                                                                                                                     new PropertyMetadata(true));
        /// <summary>Defines the FixValueFontFamily dependency property</summary>
        public static readonly DependencyProperty   FixValueFontFamilyProperty = DependencyProperty.Register("FixValueFontFamily",
                                                                                                             typeof(FontFamily),
                                                                                                             typeof(SudControl),
                                                                                                             new PropertyMetadata(CreateFontFamily("Arial"), new PropertyChangedCallback(OnCellInfoChanged)));
        /// <summary>Defines the FixValueBrush dependency property</summary>
        public static readonly DependencyProperty   FixValueBrushProperty = DependencyProperty.Register("FixValueBrush",
                                                                                                        typeof(Brush),
                                                                                                        typeof(SudControl),
                                                                                                        new PropertyMetadata(new SolidColorBrush(Color.FromRgb(132, 85, 83)), new PropertyChangedCallback(OnCellInfoChanged)));
        /// <summary>Defines the UserValueFontFamily dependency property</summary>
        public static readonly DependencyProperty   UserValueFontFamilyProperty = DependencyProperty.Register("UserValueFontFamily",
                                                                                                              typeof(FontFamily),
                                                                                                              typeof(SudControl),
                                                                                                              new PropertyMetadata(CreateFontFamily("Arial"), new PropertyChangedCallback(OnCellInfoChanged)));
        /// <summary>Defines the UserValueBrush dependency property</summary>
        public static readonly DependencyProperty   UserValueBrushProperty = DependencyProperty.Register("UserValueBrush",
                                                                                                         typeof(Brush),
                                                                                                         typeof(SudControl),
                                                                                                         new PropertyMetadata(new SolidColorBrush(Color.FromRgb(174, 128, 125)), new PropertyChangedCallback(OnCellInfoChanged)));
        /// <summary>Defines the SymbolValueFontFamily dependency property</summary>
        public static readonly DependencyProperty   SymbolValueFontFamilyProperty = DependencyProperty.Register("SymbolValueFontFamily",
                                                                                                                typeof(FontFamily),
                                                                                                                typeof(SudControl),
                                                                                                                new PropertyMetadata(CreateFontFamily("Wingdings"), new PropertyChangedCallback(OnCellInfoChanged)));
        /// <summary>Defines the HintValueFontFamily dependency property</summary>
        public static readonly DependencyProperty   HintValueFontFamilyProperty = DependencyProperty.Register("HintValueFontFamily",
                                                                                                              typeof(FontFamily),
                                                                                                              typeof(SudControl),
                                                                                                              new PropertyMetadata(CreateFontFamily("Arial"), new PropertyChangedCallback(OnCellInfoChanged)));
        /// <summary>Defines the HintValueBrush dependency property</summary>
        public static readonly DependencyProperty   HintValueBrushProperty = DependencyProperty.Register("HintValueBrush",
                                                                                                         typeof(Brush),
                                                                                                         typeof(SudControl),
                                                                                                         new PropertyMetadata(new SolidColorBrush(Color.FromRgb(174, 128, 125)), new PropertyChangedCallback(OnCellInfoChanged)));
        /// <summary>Defines the BoardBackground dependency property</summary>
        public static readonly DependencyProperty   BoardBackgroundProperty = DependencyProperty.Register("BoardBackground",
                                                                                                          typeof(Brush),
                                                                                                          typeof(SudControl),
                                                                                                          new PropertyMetadata(new SolidColorBrush(Color.FromRgb(255, 255, 255)), new PropertyChangedCallback(OnBoardBackgroundChanged)));
        /// <summary>Defines the ThickLineColor dependency property</summary>
        public static readonly DependencyProperty   ThickLineColorProperty = DependencyProperty.Register("ThickLineColor",
                                                                                                         typeof(Color),
                                                                                                         typeof(SudControl),
                                                                                                         new PropertyMetadata(Color.FromRgb(0, 0, 0), new PropertyChangedCallback(OnBoardInfoChanged)));
        /// <summary>Defines the ThinLineColor dependency property</summary>
        public static readonly DependencyProperty   ThinLineColorProperty = DependencyProperty.Register("ThinLineColor",
                                                                                                        typeof(Color),
                                                                                                        typeof(SudControl),
                                                                                                        new PropertyMetadata(Color.FromRgb(0, 0, 0), new PropertyChangedCallback(OnBoardInfoChanged)));
        /// <summary>Defines the ShowHint dependency property</summary>
        public static readonly DependencyProperty   ShowHintProperty = DependencyProperty.Register("ShowHint",
                                                                                                   typeof(bool),
                                                                                                   typeof(SudControl),
                                                                                                   new PropertyMetadata(false, new PropertyChangedCallback(OnCellInfoChanged)));
        /// <summary>Defines the ShowMarker dependency property</summary>
        public static readonly DependencyProperty   ShowMarkerProperty = DependencyProperty.Register("ShowMarker",
                                                                                                     typeof(bool),
                                                                                                     typeof(SudControl),
                                                                                                     new PropertyMetadata(true, new PropertyChangedCallback(OnCellInfoChanged)));

        #endregion

        #region Fields
        /// <summary>
        /// Delegate for CellClick event
        /// </summary>
        /// <param name="sender">   Sender object</param>
        /// <param name="e">        Event argument</param>
        public delegate void        delCellClick(object sender, CellClickEventArg e);
        /// <summary>Trigger when a cell is clicked</summary>
        public event delCellClick   CellClick;
        /// <summary>Called when a board value is changed by a user selection</summary>
        public event EventHandler   BoardValueChanged;
        /// <summary>Called when a board marker value changed</summary>
        public event EventHandler   BoardMarkerChanged;
        /// <summary>Attached sudoku board</summary>
        private SudBoard            m_sudBoard;
        /// <summary>Sudoku board use for implementing a many step undo</summary>
        private SudBoard            m_sudBoardUndo;
        /// <summary>Sudoku board use for implementing a many step redo</summary>
        private SudBoard            m_sudBoardRedo;
        /// <summary>Array of cell textblock</summary>
        private Cell[]              m_arrCellTextBlock;
        /// <summary>true if in design mode</summary>
        private bool                m_bDesignMode;
        /// <summary>Array of markers</summary>
        private MarkerE[,]          m_arrMarker;
        /// <summary>Mask for green marker on value pickup control</summary>
        private int[,]              m_arrGreenMarkerMask;
        /// <summary>Mask for blue marker on value pickup control</summary>
        private int[,]              m_arrBlueMarkerMask;
        /// <summary>Mask for red marker on value pickup control</summary>
        private int[,]              m_arrRedMarkerMask;
        /// <summary>Number of markers</summary>
        private int                 m_iMarkerCount;
        /// <summary>Current filename. Null if untitled.</summary>
        private string              m_strFileName;
        /// <summary>Next value to visualize</summary>
        private int                 m_iValToVisualize;
        /// <summary>Kind of help as been used to solve the board</summary>
        private CheatInfoE          m_eCheatInfo;
        /// <summary>Value picker</summary>
        private ValPickerControl    m_valPickerCtl;

        #endregion

        #region Ctor

        /// <summary>
        /// Class ctor
        /// </summary>
        public SudControl() {
            InitializeComponent();
            m_sudBoard                          = null;
            m_arrCellTextBlock                  = null;
            m_bDesignMode                       = false;
            m_iMarkerCount                      = 0;
            m_strFileName                       = null;
            m_sudBoardUndo                      = null;
            m_sudBoardRedo                      = null;
            m_eCheatInfo                        = CheatInfoE.None;
            m_arrMarker                         = new MarkerE[16,16];
            m_arrGreenMarkerMask                = new int[16, 16];
            m_arrBlueMarkerMask                 = new int[16, 16];
            m_arrRedMarkerMask                  = new int[16, 16];
            m_valPickerCtl                      = new ValPickerControl();
            m_valPickerCtl.Name                 = "ValPicker";
            m_valPickerCtl.SymbolType           = SymbolType;
            m_valPickerCtl.Width                = Double.NaN;
            m_valPickerCtl.Height               = Double.NaN;
            m_valPickerCtl.HorizontalAlignment  = HorizontalAlignment.Stretch;
            m_valPickerCtl.VerticalAlignment    = VerticalAlignment.Stretch;
            m_valPickerCtl.Margin               = new Thickness(0, 0, 0, 0);
            m_valPickerCtl.Background           = Background;
            m_valPickerCtl.ValueSelected       += new ValPickerControl.ValueSelectedHandler(m_valPickerCtl_ValueSelected);
            m_iValToVisualize                   = 0;
            Loaded                             += new RoutedEventHandler(SudControl_Loaded);
        }


        /// <summary>
        /// Creates a sudoku control clone which can be used for printing
        /// </summary>
        /// <param name="board">        Sudoku board to attach to the clone</param>
        /// <param name="bShowMarker">  true to show marker</param>
        /// <param name="bShowHint">    true to show hint</param>
        /// <returns>
        /// New sudoku control
        /// </returns>
        public SudControl CloneForPrinting(SudBoard board, bool bShowMarker, bool bShowHint) {
            SudControl  ctlRetVal;

            ctlRetVal                           = new SudControl();
            ctlRetVal.Background                = new SolidColorBrush(Colors.White);
            ctlRetVal.BoardBackground           = ctlRetVal.Background;
            ctlRetVal.FixValueFontFamily        = FixValueFontFamily;
            ctlRetVal.FixValueBrush             = new SolidColorBrush(Colors.Black);
            ctlRetVal.UserValueFontFamily       = UserValueFontFamily;
            ctlRetVal.UserValueBrush            = new SolidColorBrush(Colors.DarkGray);
            ctlRetVal.HintValueFontFamily       = HintValueFontFamily;
            ctlRetVal.HintValueBrush            = ctlRetVal.FixValueBrush;
            ctlRetVal.ThinLineColor             = Colors.DarkGray;
            ctlRetVal.ThickLineColor            = Colors.Black;
            ctlRetVal.Board                     = board;
            ctlRetVal.SymbolType                = SymbolType;
            ctlRetVal.ShowMarker                = bShowMarker;
            ctlRetVal.ShowHint                  = bShowHint;
            return(ctlRetVal);
        }

        #endregion

        #region Refresh
        
        /// <summary>
        /// Refresh the board contents
        /// </summary>
        private void RefreshBoard() {
            Grid            gridChild;
            Cell            cell;
            Border          border;
            Border          borderChild;
            int             iSqrSize;
            int             iLineSize;
            int             iGridSize;
            int             iStartLine;
            int             iStartCol;
            int             iXPos;
            int             iYPos;
            int             iPos;
            Thickness       thicknessThick;
            Thickness       thicknessThin;
            SolidColorBrush brushThick;
            SolidColorBrush brushThin;
            
            gridRoot.Children.Clear();
            gridRoot.ColumnDefinitions.Clear();
            gridRoot.RowDefinitions.Clear();
            if (m_sudBoard != null) {
                iSqrSize            = m_sudBoard.SquareSize;
                iLineSize           = iSqrSize * iSqrSize;
                iGridSize           = iLineSize * iLineSize;
                m_arrCellTextBlock  = new Cell[iGridSize];
                thicknessThick      = new Thickness(1);
                thicknessThin       = new Thickness(1);
                brushThick          = new SolidColorBrush(ThickLineColor);
                brushThin           = new SolidColorBrush(ThinLineColor);
                for (int i = 0; i < iSqrSize; i++) {
                    gridRoot.ColumnDefinitions.Add(new ColumnDefinition());
                    gridRoot.RowDefinitions.Add(new RowDefinition());
                }
                for (int i = 0; i < iLineSize; i++) {
                    iStartLine              = i / iSqrSize * iSqrSize;
                    iStartCol               = (i % iSqrSize) * iSqrSize;
                    border                  = new Border();
                    border.BorderThickness  = thicknessThick;
                    border.BorderBrush      = brushThick;
                    Grid.SetColumn(border, i % iSqrSize);
                    Grid.SetRow(border, i / iSqrSize);
                    gridChild               = new Grid();
                    for (int j = 0; j < iSqrSize; j++) {
                        gridChild.ColumnDefinitions.Add(new ColumnDefinition());
                        gridChild.RowDefinitions.Add(new RowDefinition());
                    }
                    for (int j = 0; j < iLineSize; j++) {
                        borderChild                     = new Border();
                        borderChild.BorderThickness     = thicknessThin;
                        borderChild.BorderBrush         = brushThin;
                        Grid.SetColumn(borderChild, j % iSqrSize);
                        Grid.SetRow(borderChild, j / iSqrSize);
                        iXPos                           = iStartCol + (j % iSqrSize);
                        iYPos                           = iStartLine + j / iSqrSize;
                        cell                            = new Cell(this, iXPos, iYPos);
                        iPos                            = iYPos * iLineSize + iXPos;
                        m_arrCellTextBlock[iPos]        = cell;
                        borderChild.Child               = cell;
                        gridChild.Children.Add(borderChild);
                    }
                    border.Child = gridChild;
                    gridRoot.Children.Add(border);
                }
            }
        }

        /// <summary>
        /// Refresh a cell marker
        /// </summary>
        /// <param name="tCellPos"> Cell position</param>
        private void RefreshCellMarker(CellPos tCellPos) {
            Cell           cell;

            if (m_arrCellTextBlock != null) {
                cell = m_arrCellTextBlock[tCellPos.Y * m_sudBoard.LineSize + tCellPos.X];
                cell.RefreshMarker();
            }
        }

        /// <summary>
        /// Refresh the state of all cells
        /// </summary>
        /// <param name="tCellPos"> Cell position</param>
        private void RefreshCellState(CellPos tCellPos) {
            Cell           cell;

            if (m_arrCellTextBlock != null) {
                cell = m_arrCellTextBlock[tCellPos.Y * m_sudBoard.LineSize + tCellPos.X];
                cell.RefreshState();
            }
        }

        /// <summary>
        /// Refresh the state of all cells
        /// </summary>
        public void RefreshCellStates() {
            if (m_sudBoard != null) {
                foreach (Cell cell in m_arrCellTextBlock) {
                    cell.RefreshState();
                }
                m_valPickerCtl.SymbolType = SymbolType;
                m_valPickerCtl.FontFamily = (SymbolType == SymbolTypeE.Symbol) ? SymbolValueFontFamily : UserValueFontFamily;
            }
        }

        #endregion

        #region Properties Notification

        /// <summary>
        /// Notification when cell info need to be refreshed
        /// </summary>
        /// <param name="o">    Dependency object</param>
        /// <param name="e">    Event argument</param>
        protected static void OnCellInfoChanged(DependencyObject o, DependencyPropertyChangedEventArgs e) {
            SudControl  sudControl;

            sudControl = o as SudControl;
            if (sudControl != null) {
                sudControl.RefreshCellStates();
                sudControl.m_valPickerCtl.ValidValueBrush   = sudControl.FixValueBrush;
                sudControl.m_valPickerCtl.UnvalidValueBrush = sudControl.UserValueBrush;
            }
        }

        /// <summary>
        /// Notification when board need to be rebuild
        /// </summary>
        /// <param name="o">    Dependency object</param>
        /// <param name="e">    Event argument</param>
        protected static void OnBoardInfoChanged(DependencyObject o, DependencyPropertyChangedEventArgs e) {
            SudControl  sudControl;

            sudControl = o as SudControl;
            if (sudControl != null) {
                sudControl.RefreshBoard();
            }
        }

        /// <summary>
        /// Notification when board background changed
        /// </summary>
        /// <param name="o">    Dependency object</param>
        /// <param name="e">    Event argument</param>
        protected static void OnBoardBackgroundChanged(DependencyObject o, DependencyPropertyChangedEventArgs e) {
            SudControl  sudControl;

            sudControl = o as SudControl;
            if (sudControl != null) {
                sudControl.gridRoot.Background       = sudControl.BoardBackground;
                sudControl.m_valPickerCtl.Background = sudControl.BoardBackground;
            }
        }

        /// <summary>
        /// Notification when input method changed
        /// </summary>
        /// <param name="o">    Dependency object</param>
        /// <param name="e">    Event argument</param>
        protected static void OnInputMethodChanged(DependencyObject o, DependencyPropertyChangedEventArgs e) {
            SudControl  sudControl;

            sudControl = o as SudControl;
            if (sudControl != null) {
                Cell.HideValuePicker(sudControl);
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Returns the board square size
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public int SquareSize {
            get {
                return((m_sudBoard == null) ? 3 : m_sudBoard.SquareSize);
            }
        }

        /// <summary>
        /// Returns the board line size
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public int LineSize {
            get {
                return((m_sudBoard == null) ? 9 : m_sudBoard.LineSize);
            }
        }

        /// <summary>
        /// Returns the board cells count
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public int BoardSize {
            get {
                return((m_sudBoard == null) ? 81 : m_sudBoard.BoardSize);
            }
        }

        /// <summary>
        /// Defines the type of sumbols use to represent values (digits, letters, symbols)
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Browsable(true)]
        [Bindable(true)]
        [DefaultValue(false)]
        [Category("Behavior")]
        [Description("Type of symbol use to draw the board")]
        public SymbolTypeE SymbolType {
            get {
                return((SymbolTypeE)GetValue(SymbolTypeProperty));
            }
            set {
                SetValue(SymbolTypeProperty, value);
            }
        }

        /// <summary>
        /// Sudoku board associated with the control
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public SudBoard Board {
            get {
                return(m_sudBoard);
            }
            set {
                int     iSqrSize;
                
                if (m_sudBoard != value) {
                    m_sudBoard = value;
                    RefreshBoard();
                    m_sudBoardUndo  = null;
                    m_sudBoardRedo  = null;
                    iSqrSize                    = (m_sudBoard == null) ? 3 : m_sudBoard.SquareSize;
                    m_valPickerCtl.BoardType    = (iSqrSize == 3) ? ValPickerControl.BoardTypeE.v9x9 : ValPickerControl.BoardTypeE.v16x16;
                    m_iValToVisualize           = 0;
                }
            }
        }

        /// <summary>
        /// Set if only valid values are proposed to the user
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Browsable(true)]
        [Bindable(true)]
        [DefaultValue(false)]
        [Category("Behavior")]
        [Description("Determine if only valid values are proposed to the user")]
        public bool OnlyValidMove {
            get {
                return((bool)GetValue(OnlyValidMoveProperty));
            }
            set {
                SetValue(OnlyValidMoveProperty, value);
            }
        }
        
        /// <summary>
        /// Determine if right click is automaticaly handle
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Browsable(true)]
        [Bindable(true)]
        [DefaultValue(true)]
        [Category("Behavior")]
        [Description("Determine if right click is automaticaly handle")]
        public bool AutoHandleRightClick {
            get {
                return((bool)GetValue(AutoHandleRightClickProperty));
            }
            set {
                SetValue(AutoHandleRightClickProperty, value);
            }
        }
        
        /// <summary>
        /// Input method
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Browsable(true)]
        [Bindable(true)]
        [DefaultValue(2)]
        [Category("Behavior")]
        [Description("Determine which input method is used to pickup value")]
        public InputMethodE InputMethod {
            get {
                return((InputMethodE)GetValue(InputMethodProperty));
            }
            set {
                SetValue(InputMethodProperty, value);
            }
        }

        /// <summary>
        /// Input method
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Browsable(true)]
        [Bindable(true)]
        [DefaultValue(2)]
        [Category("Behavior")]
        [Description("Determine if value picker is shown when the mouse move or only on click")]
        public bool ShowValuePickerOnMouseMove {
            get {
                return((bool)GetValue(ShowValuePickerOnMouseMoveProperty));
            }
            set {
                SetValue(ShowValuePickerOnMouseMoveProperty, value);
            }
        }

        /// <summary>
        /// Font family used to create the fix value font
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Browsable(true)]
        [Bindable(true)]
        [Category("Appearance")]
        [Description("Font family used to create the fix value font")]
        public FontFamily FixValueFontFamily {
            get {
                return((FontFamily)GetValue(FixValueFontFamilyProperty));
            }
            set {
                SetValue(FixValueFontFamilyProperty, value);
            }
        }
        
        /// <summary>
        /// Brush used to draw the fix value font
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Browsable(true)]
        [Bindable(true)]
        [Category("Appearance")]
        [Description("Brush used to draw the fix value")]
        public Brush FixValueBrush {
            get {
                return((Brush)GetValue(FixValueBrushProperty));
            }
            set {
                SetValue(FixValueBrushProperty, value);
            }
        }

        /// <summary>
        /// Font family used to create the fix value font
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Browsable(true)]
        [Bindable(true)]
        [Category("Appearance")]
        [Description("Font family used to create the user value font")]
        public FontFamily UserValueFontFamily {
            get {
                return((FontFamily)GetValue(UserValueFontFamilyProperty));
            }
            set {
                SetValue(UserValueFontFamilyProperty, value);
            }
        }

        /// <summary>
        /// Brush used to draw the fix value font
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Browsable(true)]
        [Bindable(true)]
        [Category("Appearance")]
        [Description("Brush used to draw the user value")]
        public Brush UserValueBrush {
            get {
                return((Brush)GetValue(UserValueBrushProperty));
            }
            set {
                SetValue(UserValueBrushProperty, value);
            }
        }

        /// <summary>
        /// Font family used to draw value in symbol mode
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Browsable(true)]
        [Bindable(true)]
        [Category("Appearance")]
        [Description("Font family used to draw value in symbol mode")]
        public FontFamily SymbolValueFontFamily {
            get {
                return((FontFamily)GetValue(SymbolValueFontFamilyProperty));
            }
            set {
                SetValue(SymbolValueFontFamilyProperty, value);
            }
        }

        /// <summary>
        /// Font family used to create the hint value font
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Browsable(true)]
        [Bindable(true)]
        [Category("Appearance")]
        [Description("Font family used to create the hint value font")]
        public FontFamily HintValueFontFamily {
            get {
                return((FontFamily)GetValue(HintValueFontFamilyProperty));
            }
            set {
                SetValue(HintValueFontFamilyProperty, value);
            }
        }

        /// <summary>
        /// Brush used to draw the fix value font
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Browsable(true)]
        [Bindable(true)]
        [Category("Appearance")]
        [Description("Brush used to draw the hint values")]
        public Brush HintValueBrush {
            get {
                return((Brush)GetValue(HintValueBrushProperty));
            }
            set {
                SetValue(HintValueBrushProperty, value);
            }
        }

        /// <summary>
        /// Brush used to draw the board background
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Browsable(true)]
        [Bindable(true)]
        [Category("Appearance")]
        [Description("Brush used to draw the board background")]
        public Brush BoardBackground {
            get {
                return((Brush)GetValue(BoardBackgroundProperty));
            }
            set {
                SetValue(BoardBackgroundProperty, value);
            }
        }

        /// <summary>
        /// Color used to draw the thick lines of the board frame
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Browsable(true)]
        [Bindable(true)]
        [Category("Appearance")]
        [Description("Color used to draw the thick lines of the board frame")]
        public Color ThickLineColor {
            get {
                return((Color)GetValue(ThickLineColorProperty));
            }
            set {
                SetValue(ThickLineColorProperty, value);
            }
        }

        /// <summary>
        /// Color used to draw the thin lines of the board frame
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Browsable(true)]
        [Bindable(true)]
        [Category("Appearance")]
        [Description("Color used to draw the thin lines of the board frame")]
        public Color ThinLineColor {
            get {
                return((Color)GetValue(ThinLineColorProperty));
            }
            set {
                SetValue(ThinLineColorProperty, value);
            }
        }

        /// <summary>
        /// Start or end the design mode
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public bool BoardDesignMode {
            get {
                return(m_bDesignMode);
            }
            set {
                if (m_bDesignMode != value) {
                    m_bDesignMode = value;
                    if (value) {
                        m_sudBoardUndo  = null;
                        m_sudBoardRedo  = null;
                        m_sudBoard.UndoAllMove(false);
                    }
                    RefreshBoard();
                }
            }
        }

        /// <summary>
        /// Kind of help which has been used to solve the board
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public CheatInfoE CheatInfo {
            get {
                return(m_eCheatInfo);
            }
        }

        /// <summary>
        /// Determine if a global undo is possible
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public bool GlobalUndoActive {
            get {
                return(m_sudBoardUndo != null);
            }
        }

        /// <summary>
        /// Determine if a global redo is possible
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public bool GlobalRedoActive {
            get {
                return(m_sudBoardRedo != null);
            }
        }

        /// <summary>
        /// Determine if hint are shown
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Browsable(true)]
        [Bindable(true)]
        [DefaultValue(false)]
        [Category("Behavior")]
        [Description("Determine if hint are shown")]
        public bool ShowHint {
            get {
                return((bool)GetValue(ShowHintProperty));
            }
            set {
                SetValue(ShowHintProperty, value);
            }
        }

        /// <summary>
        /// Determine if marker are shown
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Browsable(true)]
        [Bindable(true)]
        [DefaultValue(false)]
        [Category("Behavior")]
        [Description("Determine if marker are shown")]
        public bool ShowMarker {
            get {
                return((bool)GetValue(ShowMarkerProperty));
            }
            set {
                SetValue(ShowMarkerProperty, value);
            }
        }
        
        /// <summary>
        /// Number of markers (cell markers and value picker control markers)
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public int MarkerCount {
            get {
                return(m_iMarkerCount);
            }
        }

        /// <summary>
        /// Name of the file containing the board. null if none.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public string FileName {
            get {
                return(m_strFileName);
            }
            set {
                m_strFileName = value;
            }
        }

        #endregion

        #region Moves

        /// <summary>
        /// Playing at the specified position
        /// </summary>
        /// <param name="tCellPos">     Position to play at</param>
        /// <param name="iVal">         Value to play</param>
        /// <param name="bUser">        true if user move</param>
        /// <param name="bRecordMove">  true to record the move</param>
        /// <param name="bOnlyIfValid"> true if only valid move can be accepted</param>
        /// <returns>
        /// true if move made
        /// </returns>
        public bool PlayAt(CellPos tCellPos, int iVal, bool bUser, bool bRecordMove, bool bOnlyIfValid) {
            bool    bRetVal;
            
            m_sudBoardUndo  = null;
            m_sudBoardRedo  = null;
            bRetVal         = m_sudBoard.PlayAt(tCellPos, iVal, bUser, bRecordMove, bOnlyIfValid);
            if (bRetVal) {
                RefreshCellState(tCellPos);
            }
            return(bRetVal);
        }
        
        /// <summary>
        /// Playing at the specified position
        /// </summary>
        /// <param name="tCellPos">     Position to play at</param>
        /// <param name="iVal">         Value to play</param>
        /// <param name="bOnlyIfValid"> true if only valid move can be accepted</param>
        /// <returns>
        /// true if move made
        /// </returns>
        public bool PlayAt(CellPos tCellPos, int iVal, bool bOnlyIfValid) {
            bool    bRetVal;
            
            m_sudBoardUndo  = null;
            m_sudBoardRedo  = null;
            bRetVal         = m_sudBoard.PlayAt(tCellPos, iVal, true, true, bOnlyIfValid);
            if (bRetVal) {
                RefreshCellState(tCellPos);
            }
            return(bRetVal);
        }

        /// <summary>
        /// Play the next cell value
        /// </summary>
        /// <param name="tCellPos">     Cell position</param>
        /// <param name="bUser">        true if user move</param>
        /// <param name="bRecordMove">  true to record the move</param>
        /// <param name="bOnlyIfValid"> true to only play valid move</param>
        /// <returns>
        /// true if succeed, false if not
        /// </returns>
        public bool PlayNextCellVal(CellPos tCellPos, bool bUser, bool bRecordMove, bool bOnlyIfValid) {
            bool    bRetVal;

            m_sudBoardUndo  = null;
            m_sudBoardRedo  = null;
            bRetVal         = m_sudBoard.PlayNextCellVal(tCellPos, bUser, bRecordMove, bOnlyIfValid);
            if (bRetVal) {
                RefreshCellState(tCellPos);
            }
            return(bRetVal);
        }

        /// <summary>
        /// Undo a move
        /// </summary>
        /// <returns>
        /// true if succeed
        /// </returns>
        public bool UndoMove() {
            bool    bRetVal;
            CellPos tCellPos;
            
            if (m_sudBoardUndo != null) {
                m_sudBoard      = m_sudBoardUndo;
                m_sudBoardUndo  = null;
                bRetVal         = true;
                RefreshCellStates();
            } else {
                m_sudBoardRedo  = null;
                bRetVal         = m_sudBoard.UndoMove(out tCellPos, true);
                if (bRetVal) {
                    RefreshCellState(tCellPos);
                }
            }
            return(bRetVal);
        }

        /// <summary>
        /// Undo all moves
        /// </summary>
        public void UndoAllMove() {
            SudBoard    sudBoardRedo;
            int         iMoveCount;
            
            iMoveCount = m_sudBoard.UndoMoveCount;
            if (iMoveCount != 0) {
                sudBoardRedo = m_sudBoard.Clone();
                for (int iIndex = 0; iIndex < iMoveCount; iIndex++) {
                    UndoMove();
                }
                m_sudBoardRedo = sudBoardRedo;
                m_sudBoardUndo = null;
            }
        }

        /// <summary>
        /// Redo a move
        /// </summary>
        /// <returns>
        /// true if succeed, false if not
        /// </returns>
        public bool RedoMove() {
            bool    bRetVal;
            CellPos tCellPos;
            
            if (m_sudBoardRedo != null) {
                m_sudBoard      = m_sudBoardRedo;
                m_sudBoardUndo  = null;
                bRetVal         = true;
                RefreshCellStates();
            } else {
                bRetVal = m_sudBoard.RedoMove(out tCellPos);
                if (bRetVal) {
                    RefreshCellState(tCellPos);
                }
            }
            return(bRetVal);
        }

        /// <summary>
        /// Clear the board
        /// </summary>
        public void Clear() {
            m_sudBoard.Clear();
            m_sudBoardUndo  = null;
            m_sudBoardRedo  = null;
            ClearAllMarker(true);
            RefreshCellStates();
        }

        /// <summary>
        /// Validates the board
        /// </summary>
        /// <returns>
        /// true if board is valid
        /// </returns>
        public bool ValidateBoard() {
            bool        bRetVal = true;
            SudBoard    sudBoard;
            CellPos     tCellPos;
            
            sudBoard = m_sudBoard.Clone();
            if (sudBoard.Solve(true) != 1) {
                MessageBox.Show("No Solution!");
            } else {
                ClearAllMarker(false);
                tCellPos = new CellPos();
                for (tCellPos.Y = 0; tCellPos.Y < m_sudBoard.LineSize; tCellPos.Y++) {
                    for (tCellPos.X = 0; tCellPos.X < m_sudBoard.LineSize; tCellPos.X++) {
                        if (m_sudBoard[tCellPos] != 0 &&
                            m_sudBoard[tCellPos] != sudBoard[tCellPos]) {
                            SetMarker(tCellPos, MarkerE.Red);
                            bRetVal = false;
                        }
                    }
                }
            }
            return(bRetVal);
        }

        /// <summary>
        /// Solve the board
        /// </summary>
        /// <param name="bStopAtFirstSolution"> true to stop at the first solution found</param>
        /// <param name="eLevel">               Difficulty level</param>
        /// <returns>
        /// Number of solution founds (0=None,1=One,2=More than one)
        /// </returns>
        public int Solve(bool bStopAtFirstSolution, out SudBoard.LevelE eLevel) {
            int     iRetVal;
            
            iRetVal = m_sudBoard.Solve(bStopAtFirstSolution, out eLevel);
            RefreshCellStates();
            return(iRetVal);
        }

        /// <summary>
        /// Solve the board
        /// </summary>
        public int Solve() {
            SudBoard.LevelE eLevel;
            
            return(Solve(true, out eLevel));
        }
        
        #endregion

        #region ValuePicker
        
        /// <summary>
        /// Get the number of on bits in the specified value
        /// </summary>
        /// <param name="iMask">    Value to check</param>
        /// <returns>
        /// Number of 1 bits in the value
        /// </returns>
        private int GetBitCount(int iMask) {
            int     iRetVal = 0;

            while (iMask != 0) {
                if ((iMask & 1) != 0) {
                    iRetVal++;
                }
                iMask >>= 1;
            }
            return(iRetVal);
        }

        /// <summary>
        /// Get the color marker mask from the value picker control
        /// </summary>
        /// <param name="tCellPos">     Cell position</param>
        private void GetColorMarkerMaskFromValuePicker(CellPos tCellPos) {
            int     iCount;

            iCount                                          = GetBitCount(m_arrGreenMarkerMask[tCellPos.X, tCellPos.Y]) +
                                                              GetBitCount(m_arrBlueMarkerMask[tCellPos.X, tCellPos.Y]) +
                                                              GetBitCount(m_arrRedMarkerMask[tCellPos.X, tCellPos.Y]);
            m_arrGreenMarkerMask[tCellPos.X, tCellPos.Y]    = m_valPickerCtl.GreenMarkerMask;
            m_arrBlueMarkerMask[tCellPos.X, tCellPos.Y]     = m_valPickerCtl.BlueMarkerMask;
            m_arrRedMarkerMask[tCellPos.X, tCellPos.Y]      = m_valPickerCtl.RedMarkerMask;
            iCount                                         -= GetBitCount(m_arrGreenMarkerMask[tCellPos.X, tCellPos.Y]) +
                                                              GetBitCount(m_arrBlueMarkerMask[tCellPos.X, tCellPos.Y]) +
                                                              GetBitCount(m_arrRedMarkerMask[tCellPos.X, tCellPos.Y]);
            if (iCount != 0) {
                m_iMarkerCount -= iCount;
                OnBoardMarkerChanged(EventArgs.Empty);
            }
        }
        
        /// <summary>
        /// Set the color marker mask to the value picker control
        /// </summary>
        /// <param name="tCellPos">     Cell position</param>
        private void SetColorMarkerMaskToValuePicker(CellPos tCellPos) {
            m_valPickerCtl.GreenMarkerMask  = m_arrGreenMarkerMask[tCellPos.X, tCellPos.Y];
            m_valPickerCtl.BlueMarkerMask   = m_arrBlueMarkerMask[tCellPos.X, tCellPos.Y];
            m_valPickerCtl.RedMarkerMask    = m_arrRedMarkerMask[tCellPos.X, tCellPos.Y];
        }

        #endregion

        #region Sinks
        
        /// <summary>
        /// Trigger the CellClick event
        /// </summary>
        /// <param name="e">    Event argument</param>
        protected virtual void OnCellClick(CellClickEventArg e) {
            if (CellClick != null) {
                CellClick(this, e);
            }
        }

        /// <summary>
        /// Trigger the BoardValueChanged event. To be triggered when one or more board values are changed
        /// </summary>
        /// <param name="e">    Event argument</param>
        protected virtual void OnBoardValueChanged(EventArgs e) {
            if (BoardValueChanged != null) {
                BoardValueChanged(this, e);
            }
        }
        
        /// <summary>
        /// Trigger the BoardMarkerChanged event. To be triggered when one or more cells marker are changed
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnBoardMarkerChanged(EventArgs e) {
            if (BoardMarkerChanged != null) {
                BoardMarkerChanged(this, e);
            }
        }

        /// <summary>
        /// Called when control is loaded
        /// </summary>
        /// <param name="sender">   Sender object</param>
        /// <param name="e">        Event argument</param>
        void SudControl_Loaded(object sender, RoutedEventArgs e) {
            RefreshCellStates();
        }

        /// <summary>
        /// Make the grid square
        /// </summary>
        /// <param name="size"> User control size</param>
        private void MakeSquare(Size size) {
            double  dMinSize;
            double  dHorzMargin;
            double  dVertMargin;

            dMinSize        = (size.Width < size.Height) ? size.Width : size.Height;
            dHorzMargin     = (size.Width - dMinSize) / 2;
            dVertMargin     = (size.Height - dMinSize) / 2;
            gridRoot.Margin = new Thickness(dHorzMargin, dVertMargin, dHorzMargin, dVertMargin);
        }
        
        /// <summary>
        /// Called when the Measure() method is called
        /// </summary>
        /// <param name="constraint">   Size constraint</param>
        /// <returns>
        /// Control size
        /// </returns>
        protected override Size MeasureOverride(Size constraint) {
            MakeSquare(constraint);
 	        return base.MeasureOverride(constraint);
        }

        /// <summary>
        /// Called when a value if selected using the value picker
        /// </summary>
        /// <param name="sender">   Sender object</param>
        /// <param name="e">        Event argument</param>
        void m_valPickerCtl_ValueSelected(object sender, ValPickerControl.ValueSelectedEventArg e) {
            bool    bUser;
            bool    bRecordMove;
            bool    bOnlyValidMove;
            CellPos tCellPos;

            tCellPos        = Cell.ValuePickerPos(this);
            if (tCellPos.X != -1 && tCellPos.Y != -1) {
                bUser           = !m_bDesignMode;
                bRecordMove     = !m_bDesignMode;
                bOnlyValidMove  = OnlyValidMove | m_bDesignMode;
                if (PlayAt(tCellPos, e.Value, bUser, bRecordMove, bOnlyValidMove)) {
                    SetMarker(tCellPos, MarkerE.None);
                    OnBoardValueChanged(System.EventArgs.Empty);
                    Cell.HideValuePicker(this);
                }
            }
        }

        #endregion
        
        #region Markers

        /// <summary>
        /// Gets the marker at the specified cell
        /// </summary>
        /// <param name="tCellPos">     Cell position</param>
        /// <returns>
        /// Marker
        /// </returns>
        public MarkerE GetMarker(CellPos tCellPos) {
            return(m_arrMarker[tCellPos.X, tCellPos.Y]);
        }

        /// <summary>
        /// Set a cell marker
        /// </summary>
        /// <param name="tCellPos">     Cell position</param>
        /// <param name="eMarker">      Marker</param>
        private void SetMarkerInt(CellPos tCellPos, MarkerE eMarker) {
            if (m_arrMarker[tCellPos.X, tCellPos.Y] != eMarker) {
                if (m_arrMarker[tCellPos.X, tCellPos.Y] != MarkerE.None) {
                    m_iMarkerCount--;
                }
                m_arrMarker[tCellPos.X, tCellPos.Y] = eMarker;
                if (eMarker != MarkerE.None) {
                    m_iMarkerCount++;
                }
                RefreshCellMarker(tCellPos);
            }
        }

        /// <summary>
        /// Set a cell marker. Trigger changed event
        /// </summary>
        /// <param name="tCellPos">     Cell position</param>
        /// <param name="eMarker">      Marker</param>
        public void SetMarker(CellPos tCellPos, MarkerE eMarker) {
            SetMarkerInt(tCellPos, eMarker);
            OnBoardMarkerChanged(EventArgs.Empty);
        }

        /// <summary>
        /// Toggle between the different marker
        /// </summary>
        /// <param name="tCellPos">     Cell position</param>
        public void ToggleMarker(CellPos tCellPos) {
            MarkerE eMarker;
            
            eMarker = m_arrMarker[tCellPos.X, tCellPos.Y];
            if (eMarker == MarkerE.Red) {
                eMarker = MarkerE.None;
            } else {
                eMarker = (MarkerE)(eMarker+1);
            }
            SetMarker(tCellPos, eMarker);
        }
                
        /// <summary>
        /// Toggle between the the specified marker and none
        /// </summary>
        /// <param name="tCellPos">     Cell position</param>
        /// <param name="eMarker">      Marker color</param>
        public void ToggleMarker(CellPos tCellPos, MarkerE eMarker) {
            if (m_arrMarker[tCellPos.X, tCellPos.Y] == eMarker) {
                eMarker = MarkerE.None;
            }
            SetMarker(tCellPos, eMarker);
        }

        /// <summary>
        /// Set with a marker all cells containing the specified value
        /// </summary>
        /// <param name="iVal">     Searched value</param>
        /// <param name="eMarker">  Marker to set</param>
        public void SetMarkerForSpecificValCell(int iVal, MarkerE eMarker) {
            int     iLineSize;
            CellPos tCellPos;
            bool    bTriggerEvent;

            iLineSize       = (m_sudBoard == null) ? 9 : m_sudBoard.LineSize;
            tCellPos        = new CellPos();
            bTriggerEvent   = false;
            for (tCellPos.Y = 0; tCellPos.Y < iLineSize; tCellPos.Y++) {
                for (tCellPos.X = 0; tCellPos.X < iLineSize; tCellPos.X++) {
                    if (m_sudBoard[tCellPos] == iVal) {
                        if (m_arrMarker[tCellPos.X, tCellPos.Y] != eMarker) {
                            SetMarker(tCellPos, eMarker);
                            bTriggerEvent = true;
                        }
                    }
                }
            }
            if (bTriggerEvent) {
                OnBoardMarkerChanged(EventArgs.Empty);
            }
        }

        /// <summary>
        /// Removes all markers
        /// </summary>
        /// <param name="bIncludeValPicker">    True to clear value picker one too</param>
        public void ClearAllMarker(bool bIncludeValPicker) {
            int     iLineSize;
            bool    bTriggerEvent;
            
            iLineSize       = (m_sudBoard == null) ? 9 : m_sudBoard.LineSize;
            bTriggerEvent   = false;
            for (int i = 0; i < iLineSize; i++) {
                for (int j = 0; j < iLineSize; j++) {
                    if (m_arrMarker[i, j] != MarkerE.None) {
                        SetMarkerInt(new CellPos(i, j), MarkerE.None);
                        bTriggerEvent = true;
                    }
                    if (m_arrGreenMarkerMask[i, j] != 0) {
                        m_arrGreenMarkerMask[i, j]  = 0;
                        bTriggerEvent               = true;
                    }
                    if (m_arrBlueMarkerMask[i, j] != 0) {
                        m_arrBlueMarkerMask[i, j]  = 0;
                    }
                    if (m_arrRedMarkerMask[i, j] != 0) {
                        m_arrRedMarkerMask[i, j]   = 0;
                        bTriggerEvent              = true;
                    }
                }
            }
            if (bTriggerEvent) {
                m_iMarkerCount = 0;
                OnBoardMarkerChanged(EventArgs.Empty);
            }
        }

        /// <summary>
        /// Removes green markers which are in the same line/column or square as the specified cell
        /// </summary>
        /// <param name="tCellPos">     Cell position</param>
        /// <returns>
        /// Number of markers removed
        /// </returns>
        private int RemoveGreenMarkerTouchByCellPos(CellPos tCellPos) {
            int     iRetVal = 0;
            int     iSqrPos;
            CellPos tSqrPos;
            CellPos tTestPos;

            tTestPos   = new CellPos();
            for (int iPos = 0; iPos < m_sudBoard.LineSize; iPos++) {
                tTestPos.X = tCellPos.X;
                tTestPos.Y = iPos;
                if (GetMarker(tTestPos) == MarkerE.Green) {
                    SetMarkerInt(tTestPos, MarkerE.None);
                    iRetVal++;
                }
                tTestPos.X = iPos;
                tTestPos.Y = tCellPos.Y;
                if (GetMarker(tTestPos) == MarkerE.Green) {
                    SetMarkerInt(tTestPos, MarkerE.None);
                    iRetVal++;
                }
            }
            iSqrPos     = m_sudBoard.GetSquareFromPos(tCellPos);
            tSqrPos     = m_sudBoard.GetPosFromSquare(iSqrPos);
            for (int y = 0; y < m_sudBoard.SquareSize; y++) {
                tTestPos.Y = tSqrPos.Y + y;
                for (int x = 0; x < m_sudBoard.SquareSize; x++) {
                    tTestPos.X = tSqrPos.X + x;
                    if (GetMarker(tTestPos) == MarkerE.Green) {
                        SetMarkerInt(tTestPos, MarkerE.None);
                        iRetVal++;
                    }
                }
            }
            return(iRetVal);
        }

        /// <summary>
        /// Removes green markers which are in the same line/column or square of a blue one
        /// </summary>
        private void RemoveGreenMarkerTouchByBlueOne() {
            CellPos tCellPos;
            int     iCount;

            iCount      = 0;
            tCellPos    = new CellPos();
            for (tCellPos.Y  = 0; tCellPos.Y < m_sudBoard.LineSize; tCellPos.Y++) {
                for (tCellPos.X = 0; tCellPos.X < m_sudBoard.LineSize; tCellPos.X++) { 
                    if (GetMarker(tCellPos) == MarkerE.Blue) {
                        iCount += RemoveGreenMarkerTouchByCellPos(tCellPos);
                    }
                }
            }
            if (iCount != 0) {
                OnBoardMarkerChanged(EventArgs.Empty);
            }
        }
        
        /// <summary>
        /// Visualize the specified value and the area where it can be played
        /// </summary>
        /// <param name="iVal"> Value to visualize</param>
        public void VisualizeThisValue(int iVal) {
            if (ValidateBoard()) {
                SetMarkerForSpecificValCell(iVal, MarkerE.Blue);
                SetMarkerForSpecificValCell(0, MarkerE.Green);
                RemoveGreenMarkerTouchByBlueOne();
            }
        }
        
        /// <summary>
        /// Visualize the current value
        /// </summary>
        public void VisualizeCurrentValue() {
            if (m_iValToVisualize < 1) {
                m_iValToVisualize = 1;
            } else if (m_iValToVisualize > m_sudBoard.LineSize) {
                m_iValToVisualize = 1;
            }
            VisualizeThisValue(m_iValToVisualize);
        }
        
        /// <summary>
        /// Visualize the next value
        /// </summary>
        public void VisualizeNextValue() {
            m_iValToVisualize++;
            if (m_iValToVisualize > m_sudBoard.LineSize) {
                m_iValToVisualize = 1;
            }
            VisualizeThisValue(m_iValToVisualize);
        }
        
        /// <summary>
        /// Visualize the next value
        /// </summary>
        public void VisualizePreviousValue() {
            m_iValToVisualize--;
            if (m_iValToVisualize < 1 || m_iValToVisualize > m_sudBoard.LineSize) {
                m_iValToVisualize = m_sudBoard.LineSize;
            }
            VisualizeThisValue(m_iValToVisualize);
        }

        #endregion

        #region Serialization

        /// <summary>
        /// Load a game from the specified file
        /// </summary>
        /// <param name="strFileName">  Name of the file comtaining the board definition</param>
        /// <param name="iTimeInSec">   Returned the time in second since the game started</param>
        /// <returns>
        /// true if succeed, false if failed
        /// </returns>
        public bool LoadGame(String strFileName, out int iTimeInSec) {
            bool                    bRetVal = false;
            Stream                  stream;
            BinaryReader            reader;
            SudBoard                sudBoard;
            int                     iBodySize;
            byte[]                  byteHeader;
            byte[]                  byteBody;
            byte[]                  byteStack;
            int[]                   arrColorMask;
            int                     iColorMask;
            int                     iPos;
            int                     iMarkerPos;
            int                     iMarkerCount;
            int                     iVal;
            int                     iValType;
            int                     iStackSize;
            int                     iMarker;
            CellPos                 tCellPos;
            MarkerE[]               arrMarker;
            
            iTimeInSec  = 0;
            try {
                stream = System.IO.File.Open(strFileName, System.IO.FileMode.Open);
            } catch(System.Exception) {
                MessageBox.Show("Unable to open the file - " + strFileName);
                stream = null;
            }
            if (stream != null) {
                Cell.HideValuePicker(this);
                try {
                    using (reader = new BinaryReader(stream)) {
                        byteHeader = reader.ReadBytes(5);
                        if (byteHeader.Length == 5 &&
                            byteHeader[0] == (int)'S' &&
                            byteHeader[1] == (int)'U' &&
                            byteHeader[2] == (int)'D' &&
                            (byteHeader[3] == (int)'0' || byteHeader[3] == (int)'1') &&
                            (byteHeader[4] == 9 || byteHeader[4] == 16)) {
                            if (byteHeader[3] == '1') {
                                iTimeInSec = reader.ReadInt32();
                            } else {
                                iTimeInSec = 0;
                            }
                            if (byteHeader[4] == 9) {
                                sudBoard    = new SudBoard9();
                            } else {
                                sudBoard    = new SudBoard16();
                            }
                            iBodySize       = sudBoard.BoardSize * 3;
                            byteBody        = reader.ReadBytes(iBodySize);
                            arrMarker       = new MarkerE[sudBoard.BoardSize];
                            iMarkerCount    = 0;
                            if (byteBody.Length == iBodySize) {
                                bRetVal     = true;
                                iPos        = 0;
                                iMarkerPos  = 0;
                                tCellPos    = new CellPos();
                                while (tCellPos.Y < sudBoard.LineSize && bRetVal) {
                                    tCellPos.X = 0;
                                    while (tCellPos.X < sudBoard.LineSize && bRetVal) {
                                        iVal            = (int)byteBody[iPos++];
                                        iValType        = (int)byteBody[iPos++];
                                        iMarker         = (int)byteBody[iPos++];
                                        if (iVal != 0) {
                                            bRetVal = sudBoard.PlayAt(tCellPos,
                                                                      iVal, 
                                                                      (iValType == 1) ? false : true,
                                                                      (iValType == 1) ? false : true,
                                                                      false);
                                        }
                                        switch(iMarker) {
                                        case 1:
                                            arrMarker[iMarkerPos++] = MarkerE.Blue;
                                            iMarkerCount++;
                                            break;
                                        case 2:
                                            arrMarker[iMarkerPos++] = MarkerE.Green;
                                            iMarkerCount++;
                                            break;
                                        case 3:
                                            arrMarker[iMarkerPos++] = MarkerE.Red;
                                            iMarkerCount++;
                                            break;
                                        default:
                                            arrMarker[iMarkerPos++] = MarkerE.None;
                                            break;
                                        }
                                        tCellPos.X++;
                                    }
                                    tCellPos.Y++;
                                }
                                if (bRetVal) {
                                    iStackSize  = sudBoard.UndoMoveCount * 2;
                                    if (iStackSize != 0) {
                                        byteStack = reader.ReadBytes(iStackSize);
                                        sudBoard.LoadUndoStackFromBinary(byteStack);
                                    }
                                    ClearAllMarker(true);
                                    arrColorMask = new int[sudBoard.BoardSize * 3];
                                    try {
                                        tCellPos.Y = 0;
                                        iMarkerPos  = 0;
                                        while (tCellPos.Y < sudBoard.LineSize && reader.PeekChar() != -1) {
                                            tCellPos.X = 0;
                                            while (tCellPos.X < sudBoard.LineSize && reader.PeekChar() != -1) {
                                                iColorMask                                      = reader.ReadInt32();
                                                m_iMarkerCount                                 += GetBitCount(iColorMask);
                                                m_arrGreenMarkerMask[tCellPos.X, tCellPos.Y]    = iColorMask;
                                                iColorMask                                      = reader.ReadInt32();
                                                m_iMarkerCount                                 += GetBitCount(iColorMask);
                                                m_arrBlueMarkerMask[tCellPos.X, tCellPos.Y]     = iColorMask;
                                                iColorMask                                      = reader.ReadInt32();
                                                m_iMarkerCount                                 += GetBitCount(iColorMask);
                                                m_arrRedMarkerMask[tCellPos.X, tCellPos.Y]      = iColorMask;
                                                tCellPos.X++;
                                            }
                                            tCellPos.Y++;
                                        }
                                    } catch {};
                                    m_strFileName       = strFileName;
                                    Board               = sudBoard;
                                    iMarkerPos          = 0;
                                    for (tCellPos.Y = 0; tCellPos.Y < sudBoard.LineSize; tCellPos.Y++) {
                                        for (tCellPos.X = 0; tCellPos.X < sudBoard.LineSize; tCellPos.X++) {
                                            SetMarker(tCellPos, arrMarker[iMarkerPos++]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (!bRetVal) {
                        MessageBox.Show("The format of the file '" + strFileName + " is invalid.");
                    }
                } catch(SystemException) {
                    MessageBox.Show("The file '" + strFileName + "' seems to be corrupted.");
                }
                stream.Dispose();
            }
            return(bRetVal);
        }
                        
        /// <summary>
        /// Save a game to the specified file
        /// </summary>
        /// <param name="strFileName">  Name of the file comtaining the board definition</param>
        /// <param name="iTimeInSec">   Returned the time in second since the game started</param>
        /// <returns>
        /// true if succeed, false if failed
        /// </returns>
        public bool SaveGame(String strFileName, int iTimeInSec) {
            bool                    bRetVal = false;
            Stream                  stream;
            BinaryWriter            writer;
            int                     iBodySize;
            byte[]                  byteHeader;
            byte[]                  byteBody;
            byte[]                  byteStack;
            int[]                   arrColorMask;
            int                     iPos;
            int                     iVal;
            SudBoard.ValueTypeE     eValType;
            MarkerE                 eMarker;
            CellPos                 tCellPos = new CellPos();
            
            try {
                stream = System.IO.File.Open(strFileName, System.IO.FileMode.Create);
            } catch(System.Exception) {
                MessageBox.Show("Unable to create the file - " + strFileName);
                stream = null;
            }
            if (stream != null) {
                Cell.HideValuePicker(this);
                try {
                    using (writer = new BinaryWriter(stream)) {
                        iBodySize       = m_sudBoard.BoardSize * 3;
                        byteHeader      = new byte[5];
                        byteBody        = new byte[iBodySize];
                        byteStack       = m_sudBoard.SaveUndoStackToBinary();
                        arrColorMask    = new int[m_sudBoard.BoardSize * 3];
                        byteHeader[0]   = (byte)'S';
                        byteHeader[1]   = (byte)'U';
                        byteHeader[2]   = (byte)'D';
                        byteHeader[3]   = (byte)'1';
                        byteHeader[4]   = (byte)m_sudBoard.LineSize;
                        iPos            = 0;
                        for (tCellPos.Y = 0; tCellPos.Y < m_sudBoard.LineSize; tCellPos.Y++) {
                            for (tCellPos.X = 0; tCellPos.X < m_sudBoard.LineSize; tCellPos.X++) {
                                iVal                = m_sudBoard[tCellPos];
                                eValType            = m_sudBoard.ValueType(tCellPos);
                                eMarker             = GetMarker(tCellPos);
                                byteBody[iPos++]    = (byte)iVal;
                                switch(eValType) {
                                case SudBoard.ValueTypeE.Fix:
                                    byteBody[iPos++]  = 1;
                                    break;
                                case SudBoard.ValueTypeE.User:
                                    byteBody[iPos++]  = 2;
                                    break;
                                default:
                                    byteBody[iPos++]  = 0;
                                    break;
                                }
                                switch(eMarker) {
                                case MarkerE.Blue:
                                    byteBody[iPos++]  = 1;
                                    break;
                                case MarkerE.Green:
                                    byteBody[iPos++]  = 2;
                                    break;
                                case MarkerE.Red:
                                    byteBody[iPos++]  = 3;
                                    break;
                                default:
                                    byteBody[iPos++]  = 0;
                                    break;
                                }
                            }
                        }
                        iPos = 0;
                        for (int y = 0; y < m_sudBoard.LineSize; y++) {
                            for (int x = 0; x < m_sudBoard.LineSize; x++) {
                                arrColorMask[iPos++] = m_arrGreenMarkerMask[x, y];
                                arrColorMask[iPos++] = m_arrBlueMarkerMask[x, y];
                                arrColorMask[iPos++] = m_arrRedMarkerMask[x, y];
                            }
                        }
                        writer.Write(byteHeader);
                        writer.Write(iTimeInSec);
                        writer.Write(byteBody);
                        writer.Write(byteStack);
                        foreach (int iMask in arrColorMask) {
                            writer.Write(iMask);
                        }
                        m_strFileName = strFileName;
                        bRetVal       = true;
                    }
                } catch(SystemException) {
                    MessageBox.Show("Unable to write in the file '" + strFileName + "'.");
                }
                stream.Dispose();
            }
            return(bRetVal);
        }
        #endregion

    }
}
