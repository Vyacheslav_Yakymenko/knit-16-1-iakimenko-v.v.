﻿namespace SudokuWPF.Properties {


    // Этот класс позволяет обрабатывать определенные события в классе настроек:
    // Событие SettingChanging возникает до изменения значения параметра.
    // Событие PropertyChanged возникает после изменения значения параметра.
    // Событие SettingsLoaded возникает после загрузки значений настроек.
    // Событие SettingsSaving поднимается до сохранения значений настроек.
    internal sealed partial class Settings {
        
        public Settings() {
            // // Чтобы добавить обработчики событий для сохранения и изменения параметров, раскомментируйте приведенные ниже строки:
            // this.SettingChanging += this.SettingChangingEventHandler;
            //
            // this.SettingsSaving += this.SettingsSavingEventHandler;
            //
        }
        
        private void SettingChangingEventHandler(object sender, System.Configuration.SettingChangingEventArgs e) {
            // Add code to handle the SettingChangingEvent event here.
        }
        
        private void SettingsSavingEventHandler(object sender, System.ComponentModel.CancelEventArgs e) {
            // Add code to handle the SettingsSaving event here.
        }
    }
}
