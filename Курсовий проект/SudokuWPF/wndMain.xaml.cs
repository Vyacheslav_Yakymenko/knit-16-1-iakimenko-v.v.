﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Sudoku2;

namespace SudokuWPF {
    /// <summary>
    /// Interaction logic for wndMain.xaml
    /// </summary>
    ///     
    public partial class wndMain : Window {
        
        #region Commands Definition
        
        /// <summary>List if command</summary>
        public enum SudCmdE {
            /// <summary>Unknown command</summary>
            Unknown,
            /// <summary>Open a board</summary>
            Open,
            /// <summary>Save the board using a specified name</summary>
            SaveAs,
            /// <summary>Save the board using the current name</summary>
            Save,
            /// <summary>Print the board</summary>
            Print,
            /// <summary>Print a batch of boards</summary>
            PrintInBatch,
            /// <summary>Generates a board</summary>
            GenerateABoard,
            /// <summary>Exit the program</summary>
            Exit,
            /// <summary>Clear all markers</summary>
            ClearAllMarkers,
            /// <summary>Clear tried moves</summary>
            ClearTry,
            /// <summary>Clear the boards</summary>
            ClearBoard,
            /// <summary>Toggle board building mode</summary>
            BuildABoard,
            /// <summary>Visualize current value</summary>
            VisualizeCurrent,
            /// <summary>Visualize next value</summary>
            VisualizeNext,
            /// <summary>Visualize previous value</summary>
            VisualizePrevious,
            /// <summary>Undo last move</summary>
            Undo,
            /// <summary>Redo a move</summary>
            Redo,
            /// <summary>Accept only valid value</summary>
            AcceptOnlyValidValue,
            /// <summary>Toggle the use of value picker control to input value</summary>
            UseValuePicker,
            /// <summary>Move the value picker when the mouse move</summary>
            MoveValuePicker,
            /// <summary>Change the board appearance</summary>
            ChangeAppearance,
            /// <summary>Draw the board using numbers</summary>
            DrawUsingNumbers,
            /// <summary>Draw the board using letters</summary>
            DrawUsingLetters,
            /// <summary>Draw the board using numbers (9x9) or letters (16x16)</summary>
            DrawUsingLetters16x16,
            /// <summary>Draw the board using symbols</summary>
            DrawUsingSymbols,
            /// <summary>Toggle show hint</summary>
            ShowHint,
            /// <summary>Solve the board</summary>
            SolveBoard,
            /// <summary>Validates the board</summary>
            ValidateBoard,
            /// <summary>Displays about box</summary>
            Help
        }

        /// <summary>Print in Batch command</summary>
        public static RoutedUICommand CmdPrintInBatch                   = CreateCmd(SudCmdE.PrintInBatch);
        /// <summary>Generate a Board command</summary>
        public static RoutedUICommand CmdGenerateABoard                 = CreateCmd(SudCmdE.GenerateABoard);
        /// <summary>Exit command</summary>
        public static RoutedUICommand CmdExit                           = CreateCmd(SudCmdE.Exit);
        /// <summary>Clear all markers command</summary>
        public static RoutedUICommand CmdClearAllMarkers                = CreateCmd(SudCmdE.ClearAllMarkers);
        /// <summary>Clear tries command</summary>
        public static RoutedUICommand CmdClearTry                       = CreateCmd(SudCmdE.ClearTry);
        /// <summary>Clear the board command</summary>
        public static RoutedUICommand CmdClearBoard                     = CreateCmd(SudCmdE.ClearBoard);
        /// <summary>Build a board command</summary>
        public static RoutedUICommand CmdBuildABoard                    = CreateCmd(SudCmdE.BuildABoard);
        /// <summary>Visualize the current value command</summary>
        public static RoutedUICommand CmdVisualizeCurrent               = CreateCmd(SudCmdE.VisualizeCurrent);
        /// <summary>Visualize the next value command</summary>
        public static RoutedUICommand CmdVisualizeNext                  = CreateCmd(SudCmdE.VisualizeNext);
        /// <summary>Visualize the previous value command</summary>
        public static RoutedUICommand CmdVisualizePrevious              = CreateCmd(SudCmdE.VisualizePrevious);
        /// <summary>Toggle accept only valid value command</summary>
        public static RoutedUICommand CmdAcceptOnlyValidValue           = CreateCmd(SudCmdE.AcceptOnlyValidValue);
        /// <summary>Toggle the use of value picker command</summary>
        public static RoutedUICommand CmdUseValuePicker                 = CreateCmd(SudCmdE.UseValuePicker);
        /// <summary>Toggle the move with the mouse of the value picker command</summary>
        public static RoutedUICommand CmdMoveValuePicker                = CreateCmd(SudCmdE.MoveValuePicker);
        /// <summary>Change apperance command</summary>
        public static RoutedUICommand CmdChangeAppearance               = CreateCmd(SudCmdE.ChangeAppearance);
        /// <summary>Draw using numbers command</summary>
        public static RoutedUICommand CmdDrawUsingNumbers               = CreateCmd(SudCmdE.DrawUsingNumbers);
        /// <summary>Draw using letters command</summary>
        public static RoutedUICommand CmdDrawUsingLetters               = CreateCmd(SudCmdE.DrawUsingLetters);
        /// <summary>Draw using numbers or letters command</summary>
        public static RoutedUICommand CmdDrawUsingLetters16x16          = CreateCmd(SudCmdE.DrawUsingLetters16x16);
        /// <summary>Draw using symbols command</summary>
        public static RoutedUICommand CmdDrawUsingSymbols               = CreateCmd(SudCmdE.DrawUsingSymbols);
        /// <summary>Toggle show hint command</summary>
        public static RoutedUICommand CmdShowHint                       = CreateCmd(SudCmdE.ShowHint);
        /// <summary>Solve the board command</summary>
        public static RoutedUICommand CmdSolveBoard                     = CreateCmd(SudCmdE.SolveBoard);
        /// <summary>Validate the board command</summary>
        public static RoutedUICommand CmdValidateBoard                  = CreateCmd(SudCmdE.ValidateBoard);

        #endregion

        #region Fields
        
        /// <summary>Random number generator</summary>
        private Random                      m_rnd;
        /// <summary>Board generation difficulty level</summary>
        private SudBoard.LevelE             m_eLevel;
        /// <summary>Size of board to generate 3 (9x9) or 4 (16x16)</summary>
        private int                         m_iSqrSize;
        /// <summary>true to have a 10 sec timeout on board generation</summary>
        private bool                        m_bTimeOut;
        /// <summary>Time in second since the game as started</summary>
        private int                         m_iTimeInSec;
        /// <summary>Timer ticking each second</summary>
        private DispatcherTimer             m_timer;
        /// <summary>Return true if the board is dirty</summary>
        private bool                        m_bIsDirty;

        #endregion

        #region Ctor
                
        /// <summary>
        /// Class ctor
        /// </summary>
        public wndMain() {
            SudokuWPF.Properties.Settings   settings;
            
            InitializeComponent();
            settings = SudokuWPF.Properties.Settings.Default;
            if (settings.Top != -1      ||
                settings.Left != -1     ||
                settings.Width != -1    ||
                settings.Height != -1) {
                Left   = settings.Left;
                Top    = settings.Top;
                Width  = settings.Width;
                Height = settings.Height;
            }
            m_rnd                                   = new Random();
            m_eLevel                                = (SudBoard.LevelE)settings.Level;
            m_bTimeOut                              = settings.TimeOut10Sec;
            m_iSqrSize                              = settings.Gen16x16 ? 4 : 3;
            m_iTimeInSec                            = 0;
            m_bIsDirty                              = false;
            m_timer                                 = new DispatcherTimer();
            m_timer.Interval                        = new TimeSpan(0, 0, 1);
            m_timer.Tick                           += new EventHandler(m_timer_Tick);
            sudControl.BoardBackground              = new SolidColorBrush(Color.FromRgb(settings.BackColorRed, settings.BackColorGreen, settings.BackColorBlue));
            sudControl.FixValueFontFamily           = SudControl.CreateFontFamily(settings.FixFamilyName);
            sudControl.FixValueBrush                = new SolidColorBrush(Color.FromRgb(settings.FixColorRed, settings.FixColorGreen, settings.FixColorBlue));
            sudControl.UserValueFontFamily          = SudControl.CreateFontFamily(settings.UserFamilyName);
            sudControl.UserValueBrush               = new SolidColorBrush(Color.FromRgb(settings.UserColorRed, settings.UserColorGreen, settings.UserColorBlue));
            sudControl.HintValueFontFamily          = SudControl.CreateFontFamily(settings.HintFamilyName);
            sudControl.HintValueBrush               = new SolidColorBrush(Color.FromRgb(settings.HintColorRed, settings.HintColorGreen, settings.HintColorBlue));
            sudControl.ThinLineColor                = Color.FromRgb(settings.ThinLineColorRed, settings.ThinLineColorGreen, settings.ThinLineColorBlue);
            sudControl.ThickLineColor               = Color.FromRgb(settings.ThickLineColorRed, settings.ThickLineColorGreen, settings.ThickLineColorBlue);
            sudControl.Board                        = null;
            sudControl.BoardValueChanged           += new EventHandler(sudControl_BoardValueChanged);
            sudControl.BoardMarkerChanged          += new EventHandler(sudControl_BoardMarkerChanged);
            sudControl.SymbolType                   = (SudControl.SymbolTypeE)settings.SymbolType;
            sudControl.OnlyValidMove                = settings.OnlyValidMove;
            sudControl.InputMethod                  = (settings.UseValuePicker) ? SudControl.InputMethodE.Picker : SudControl.InputMethodE.Click;
            sudControl.ShowValuePickerOnMouseMove   = settings.ShowValuePickerOnMove;
            SetTitle();
        }

        #endregion

        #region Commands Routing
                
        /// <summary>
        /// Creates a routed command from a command code
        /// </summary>
        /// <param name="eCmd"> Command code</param>
        /// <returns>
        /// New routed command
        /// </returns>
        private static RoutedUICommand CreateCmd(SudCmdE eCmd) {
            return(new RoutedUICommand("", eCmd.ToString(), typeof(wndMain)));
        }

        /// <summary>
        /// Convert a command name to a command code
        /// </summary>
        /// <param name="strName">  Command name</param>
        /// <returns>
        /// Command code
        /// </returns>
        private SudCmdE NameToCmd(string strName) {
            SudCmdE eRetVal;

            try {
                eRetVal = (SudCmdE)Enum.Parse(typeof(SudCmdE), strName);
            } catch(ArgumentException) {
                eRetVal = SudCmdE.Unknown;
            }
            return(eRetVal);
        }

        /// <summary>
        /// Determine if the specified command can be executed
        /// </summary>
        /// <param name="eCmd">     Command</param>
        /// <returns>
        /// true if can execute, false if not
        /// </returns>
        private bool CmdCanExecute(SudCmdE eCmd) {
            bool        bRetVal;
            bool        bDesignMode;
            bool        bUndoEnabled;
            bool        bRedoEnabled;
            SudBoard    sudBoard;

            bDesignMode     = sudControl.BoardDesignMode;
            bRetVal         = !bDesignMode;
            sudBoard        = sudControl.Board;
            bUndoEnabled    = (sudBoard.UndoMoveCount != 0 || sudControl.GlobalUndoActive);
            bRedoEnabled    = (sudBoard.RedoMoveCount != 0 || sudControl.GlobalRedoActive);
            switch(eCmd) {
            case SudCmdE.Open:
            case SudCmdE.SaveAs:
            case SudCmdE.Save:
            case SudCmdE.Print:
            case SudCmdE.PrintInBatch:
            case SudCmdE.GenerateABoard:
            case SudCmdE.VisualizeCurrent:
            case SudCmdE.VisualizeNext:
            case SudCmdE.VisualizePrevious:
            case SudCmdE.AcceptOnlyValidValue:
            case SudCmdE.ChangeAppearance:
            case SudCmdE.DrawUsingNumbers:
            case SudCmdE.DrawUsingLetters:
            case SudCmdE.DrawUsingLetters16x16:
            case SudCmdE.DrawUsingSymbols:
            case SudCmdE.ShowHint:
            case SudCmdE.SolveBoard:
            case SudCmdE.ValidateBoard:
                break;
            case SudCmdE.Exit:
                bRetVal = true;
                break;
            case SudCmdE.ClearAllMarkers:
                bRetVal = bRetVal && (sudControl.MarkerCount != 0);
                break;
            case SudCmdE.ClearTry:
                bRetVal = bUndoEnabled;
                break;
            case SudCmdE.ClearBoard:
                bRetVal = (sudBoard.UsedCell != 0);
                break;
            case SudCmdE.BuildABoard:
                bRetVal = true;
                break;
            case SudCmdE.Undo:
                bRetVal = bUndoEnabled;
                break;
            case SudCmdE.Redo:
                bRetVal = bRedoEnabled;
                break;
            case SudCmdE.UseValuePicker:
                bRetVal = true;
                break;
            case SudCmdE.MoveValuePicker:
                bRetVal = true;
                break;
            case SudCmdE.Help:
                bRetVal = true;
                break;
            default:
                bRetVal = false;
                break;
            }
            return(bRetVal);
        }

        /// <summary>
        /// Determine if the specified command can be executed
        /// </summary>
        /// <param name="sender">   Sender object</param>
        /// <param name="e">        Event argument</param>
        private void CmdCanExecute(object sender, CanExecuteRoutedEventArgs e) {
            string  strCmdName;
            SudCmdE eCmd;

            strCmdName      = ((RoutedUICommand)e.Command).Name;
            eCmd            = NameToCmd(strCmdName);
            e.CanExecute    = (sudControl == null) ? true : CmdCanExecute(eCmd);
        }

        /// <summary>
        /// Executing the specified command
        /// </summary>
        /// <param name="Sender">   Sender object</param>
        /// <param name="e">        Event argument</param>
        private void CmdExecuted(object Sender, ExecutedRoutedEventArgs e) {
            string  strCmdName;
            SudCmdE eCmd;

            strCmdName  = ((RoutedUICommand)e.Command).Name;
            eCmd        = NameToCmd(strCmdName);
            switch(eCmd) {
            case SudCmdE.Open:
                DoOpen();
                break;
            case SudCmdE.SaveAs:
                DoSaveAs();
                break;
            case SudCmdE.Save:
                DoSave();
                break;
            case SudCmdE.Print:
                DoPrint();
                break;
            case SudCmdE.PrintInBatch:
                DoPrintInBatch();
                break;
            case SudCmdE.GenerateABoard:
                DoGenerateABoard();
                break;
            case SudCmdE.Exit:
                DoExit();
                break;
            case SudCmdE.ClearAllMarkers:
                DoClearAllMarkers();
                break;
            case SudCmdE.ClearTry:
                DoClearTry();
                break;
            case SudCmdE.ClearBoard:
                DoClearBoard();
                break;
            case SudCmdE.BuildABoard:
                DoBuildABoard();
                break;
            case SudCmdE.VisualizeCurrent:
                DoVisualizeCurrent();
                break;
            case SudCmdE.VisualizeNext:
                DoVisualizeNext();
                break;
            case SudCmdE.VisualizePrevious:
                DoVisualizePrevious();
                break;
            case SudCmdE.Undo:
                DoUndo();
                break;
            case SudCmdE.Redo:
                DoRedo();
                break;
            case SudCmdE.AcceptOnlyValidValue:
                DoAcceptOnlyValidValue();
                break;
            case SudCmdE.UseValuePicker:
                DoUseValuePicker();
                break;
            case SudCmdE.MoveValuePicker:
                DoMoveValuePicker();
                break;
            case SudCmdE.ChangeAppearance:
                DoChangeAppearance();
                break;
            case SudCmdE.DrawUsingNumbers:
                DoDrawUsingNumbers();
                break;
            case SudCmdE.DrawUsingLetters:
                DoDrawUsingLetters();
                break;
            case SudCmdE.DrawUsingLetters16x16:
                DoDrawUsingLetters16x16();
                break;
            case SudCmdE.DrawUsingSymbols:
                DoDrawUsingSymbols();
                break;
            case SudCmdE.ShowHint:
                DoShowHint();
                break;
            case SudCmdE.SolveBoard:
                DoSolveBoard();
                break;
            case SudCmdE.ValidateBoard:
                DoValidateBoard();
                break;
            default:
                break;
            }
        }

        #endregion

        #region Commands Implementaion

        /// <summary>
        /// Refresh the state of the menu and toolbar
        /// </summary>
        private void RefreshState() {
            SudControl.SymbolTypeE  eSymbolType;
            int                     iUsedCell;
            int                     iCellCount;

            eSymbolType                         = sudControl.SymbolType;
            iCellCount                          = sudControl.Board.BoardSize;
            iUsedCell                           = sudControl.Board.UsedCell;
            tbCount.Content                     = iUsedCell.ToString() + " / " + iCellCount.ToString();

        }

        /// <summary>
        /// Set the title using the associated filename
        /// </summary>
        private void SetTitle() {
            string  strTitle;
            string  strFileName;
            int     iIndex;

            strTitle    = Title;
            iIndex      = strTitle.IndexOf(" -");
            if (iIndex != -1) {
                strTitle = strTitle.Substring(0, iIndex);
            }
            strFileName = sudControl.FileName;
            if (strFileName == null) {
                strFileName = "Untitled";
            } else {
                strFileName = System.IO.Path.GetFileNameWithoutExtension(strFileName);
            }
            Title = strTitle + " - " + strFileName;
        }
        
        /// <summary>
        /// Save the board to a file
        /// </summary>
        /// <param name="strFileName">  Name of the file where to save the game or null to ask the user</param>
        /// <returns>
        /// true if succeed, false if failed
        /// </returns>
        private bool SaveToFile(string strFileName) {
            bool                            bRetVal = true;
            Microsoft.Win32.SaveFileDialog  saveDlg;

            if (strFileName == null) {
                saveDlg = new Microsoft.Win32.SaveFileDialog();
                saveDlg.AddExtension        = true;
                saveDlg.CheckPathExists     = true;
                saveDlg.CreatePrompt        = false;
                saveDlg.DefaultExt          = "sud";
                saveDlg.Filter              = "Sudoku Files (*.sud)|*.sud";
                saveDlg.OverwritePrompt     = true;
                bRetVal                     = (saveDlg.ShowDialog(this) == true);
                if (bRetVal) {
                    strFileName = saveDlg.FileName;                
                }
            }
            if (bRetVal) {
                if (sudControl.SaveGame(strFileName, m_iTimeInSec)) {
                    SetTitle();
                    m_bIsDirty  = false;
                } else {
                    bRetVal = false;
                }
            }
            return(bRetVal);
        }
        
        /// <summary>
        /// Checks if board must be saved before loosing it
        /// </summary>
        /// <returns>
        /// true = can perform the command, false = cannot perform the command
        /// </returns>
        private bool CheckForSave() {
            bool    bRetVal;

            if (m_bIsDirty) {
                switch(MessageBox.Show(this, "The board has been modified. Do you want to save the changes?", "...",  MessageBoxButton.YesNoCancel, MessageBoxImage.Question)) {
                case MessageBoxResult.Yes:
                    bRetVal = SaveToFile(sudControl.FileName);
                    break;
                case MessageBoxResult.No:
                    bRetVal = true;
                    break;
                default:
                    bRetVal = false;
                    break;
                }
            } else {
                bRetVal = true;
            }
            return(bRetVal);
        }

        /// <summary>
        /// Open a board
        /// </summary>
        private void DoOpen() {
            Microsoft.Win32.OpenFileDialog  openDlg;
            int                             iTimeInSec;
            
            if (CheckForSave()) {
                openDlg = new Microsoft.Win32.OpenFileDialog();
                openDlg.AddExtension        = true;
                openDlg.CheckFileExists     = true;
                openDlg.CheckPathExists     = true;
                openDlg.DefaultExt          = "sud";
                openDlg.Filter              = "Sudoku Files (*.sud)|*.sud";
                openDlg.Multiselect         = false;
                if (openDlg.ShowDialog(this) == true) {
                    if (sudControl.LoadGame(openDlg.FileName, out iTimeInSec)) {
                        m_iTimeInSec    = iTimeInSec;
                        m_bIsDirty      = false;
                        RefreshState();
                        SetTitle();
                    }
                }
            }
        
        }

        /// <summary>
        /// Save the board to the specified file
        /// </summary>
        private void DoSaveAs() {
            SaveToFile(null);
        }

        /// <summary>
        /// Save the board to the current file
        /// </summary>
        private void DoSave() {
            SaveToFile(sudControl.FileName);
        }
            
        /// <summary>
        /// Print the current board
        /// </summary>
        private void DoPrint() {
            PrintCurrentBoard();
        }

        /// <summary>
        /// Print a batch of boards
        /// </summary>
        private void DoPrintInBatch() {
            dlgGenerateABoard   dlg;

            dlg         = new dlgGenerateABoard(m_rnd, m_eLevel, m_bTimeOut, m_iSqrSize, true);
            dlg.Owner   = this;
            if (dlg.ShowDialog() == true) {
                if (dlg.m_arrSudBoard.Length == 2) {
                    PrintNBoard(dlg.m_arrSudBoard,
                                0,  // Starting index
                                1,  // Nb horz boards
                                2); // Nb vert boards
                } else {
                    PrintNBoard(dlg.m_arrSudBoard,
                                0,  // Starting index
                                2,  // Nb horz boards
                                3); // Nb vert boards
                }
            }
        }

        /// <summary>
        /// Generate a board
        /// </summary>
        private void DoGenerateABoard() {
            dlgGenerateABoard   dlg;
            
            if (CheckForSave()) {
                dlg         = new dlgGenerateABoard(m_rnd, m_eLevel, m_bTimeOut, m_iSqrSize, false);
                dlg.Owner   = this;
                if (dlg.ShowDialog() == true) {
                    m_eLevel                = dlg.m_eLevel;
                    m_iSqrSize              = dlg.m_iSqrSize;
                    m_bTimeOut              = dlg.m_bTimeOut;
                    sudControl.Board        = dlg.m_arrSudBoard[0];
                    sudControl.ClearAllMarker(true);
                    sudControl.FileName     = null;
                    m_bIsDirty              = false;
                    m_iTimeInSec            = 0;
                    SetTitle();
                    RefreshState();
                }
            }
        }

        /// <summary>
        /// Exiting the program
        /// </summary>
        private void DoExit() {
            Close();
        }

        /// <summary>
        /// Clear all markers
        /// </summary>
        private void DoClearAllMarkers() {
            sudControl.ClearAllMarker(true);
            m_bIsDirty = true;
            
        }

        /// <summary>
        /// Clear try
        /// </summary>
        private void DoClearTry() {
            sudControl.UndoAllMove();
            m_bIsDirty = true;
            RefreshState();
        }

        /// <summary>
        /// Clear the full board
        /// </summary>
        private void DoClearBoard() {
            if (CheckForSave()) {
                sudControl.Clear();
                sudControl.ClearAllMarker(true);
                m_bIsDirty = false;
                RefreshState();
            }
        }

        /// <summary>
        /// Toggle Build a Board
        /// </summary>
        private void DoBuildABoard() {
            SudBoard            sudBoard;
            int                 iSolCount;
            MessageBoxResult    eRes;

            if (sudControl.BoardDesignMode) {
                sudBoard  = sudControl.Board.Clone();
                iSolCount = sudBoard.Solve(false);
                switch(iSolCount) {
                case 0:
                    eRes = MessageBox.Show("The board have no solution. Do you still want to close the design mode?", "Design Mode", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
                    break;
                case 1:
                    eRes = MessageBoxResult.Yes;
                    break;
                default:
                    eRes = MessageBox.Show("The board have more than one solution. Do you still want to close the design mode?", "Design Mode", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
                    break;
                }
                if (eRes == MessageBoxResult.Yes) {
                    sudControl.AutoHandleRightClick  = true;
                    sudControl.ShowMarker            = true;
                    sudControl.BoardDesignMode       = false;
                    sudControl.FileName              = null;
                    SetTitle();
                    RefreshState();
                }
            } else {
                if (CheckForSave()) {
                    sudControl.AutoHandleRightClick  = false;
                    sudControl.ShowMarker            = false;
                    sudControl.BoardDesignMode       = true;
                    RefreshState();
                }
            }
        
        }

        /// <summary>
        /// Visualize the current value and the area where it can be played
        /// </summary>
        private void DoVisualizeCurrent() {
            sudControl.VisualizeCurrentValue();
            RefreshState();
        }

        /// <summary>
        /// Visualize the next value and the area where it can be played
        /// </summary>
        private void DoVisualizeNext() {
            sudControl.VisualizeNextValue();
            RefreshState();
        }

        /// <summary>
        /// Visualize the previous value and the area where it can be played
        /// </summary>
        private void DoVisualizePrevious() {
            sudControl.VisualizePreviousValue();
            RefreshState();
        }

        /// <summary>
        /// Undo a move
        /// </summary>
        private void DoUndo() {
            sudControl.UndoMove();
            RefreshState();
        }

        /// <summary>
        /// Redo a move
        /// </summary>
        private void DoRedo() {
            sudControl.RedoMove();
            RefreshState();
        }

        /// <summary>
        /// Toogle the OnlyValidValue option
        /// </summary>
        private void DoAcceptOnlyValidValue() {
            sudControl.OnlyValidMove = !sudControl.OnlyValidMove;
            RefreshState();
        }

        /// <summary>
        /// Toggle the use of ValuePicker
        /// </summary>
        private void DoUseValuePicker() {
            sudControl.InputMethod  = (sudControl.InputMethod == SudControl.InputMethodE.Picker) ? SudControl.InputMethodE.Click : SudControl.InputMethodE.Picker;
            RefreshState();
        }

        /// <summary>
        /// Toggle the Show Value Picker on move option
        /// </summary>
        private void DoMoveValuePicker() {
            sudControl.ShowValuePickerOnMouseMove = !sudControl.ShowValuePickerOnMouseMove;
            RefreshState();
        }
         
        /// <summary>
        /// Change the board appearance
        /// </summary>
        private void DoChangeAppearance() {
            dlgBoardAppearance  dlg;

            dlg         = new dlgBoardAppearance(sudControl);
            dlg.Owner   = this;
            dlg.ShowDialog();
        }

        /// <summary>
        /// Select the Draw Using Numbers option
        /// </summary>
        private void DoDrawUsingNumbers() {
            sudControl.SymbolType = SudControl.SymbolTypeE.Number;
            RefreshState();
        }

        /// <summary>
        /// Select the Draw Using Numbers option
        /// </summary>
        private void DoDrawUsingLetters() {
            sudControl.SymbolType = SudControl.SymbolTypeE.Letter;
            RefreshState();
        }

        /// <summary>
        /// Select the Draw Using Letters for 4x4 option
        /// </summary>
        private void DoDrawUsingLetters16x16() {
            sudControl.SymbolType = SudControl.SymbolTypeE.LetterFor16x16;
            RefreshState();
        }

        /// <summary>
        /// Select the Draw Using Symbols option
        /// </summary>
        private void DoDrawUsingSymbols() {
            sudControl.SymbolType = SudControl.SymbolTypeE.Symbol;
            RefreshState();
        }

        /// <summary>
        /// Toggle the Show Hint option
        /// </summary>
        private void DoShowHint() {
            sudControl.ShowHint = !sudControl.ShowHint;
        }

        /// <summary>
        /// Solve the board
        /// </summary>
        private void DoSolveBoard() {
            sudControl.ClearAllMarker(true);
            sudControl.Solve();
            m_bIsDirty = true;
            RefreshState();
        }

        /// <summary>
        /// Validate the board
        /// </summary>
        private void DoValidateBoard() {
            sudControl.ValidateBoard();
        }

        
        #endregion

        #region Printing

        /// <summary>
        /// Compute the paper margin
        /// </summary>
        /// <param name="area"> Printing area</param>
        /// <returns>
        /// Margin
        /// </returns>
        private Thickness ComputeMargin(System.Printing.PrintDocumentImageableArea area) {
            double      dLeftMargin;
            double      dRightMargin;
            double      dTopMargin;
            double      dBottomMargin;

            dLeftMargin             = area.OriginWidth;
            dTopMargin              = area.OriginHeight;
            dRightMargin            = area.MediaSizeWidth - area.ExtentWidth - dLeftMargin;
            dBottomMargin           = area.MediaSizeHeight - area.MediaSizeHeight - dRightMargin;
            return(new Thickness(dLeftMargin, dTopMargin, dRightMargin, dBottomMargin));
        }

        /// <summary>
        /// Print the current board
        /// </summary>
        private void PrintCurrentBoard() {
            System.Printing.PrintDocumentImageableArea  area = null;
            System.Windows.Xps.XpsDocumentWriter        xpsDocWriter;
            SudControl                                  sudControlPrt;
            Grid                                        grid;
            Size                                        sizeGrid;

            try {
                sudControlPrt   = sudControl.CloneForPrinting(sudControl.Board, sudControl.ShowMarker, sudControl.ShowHint);
                xpsDocWriter    = System.Printing.PrintQueue.CreateXpsDocumentWriter("Sudoku", ref area);
                if (xpsDocWriter != null) {
                    sizeGrid                = new Size(area.MediaSizeWidth, area.MediaSizeHeight);
                    grid                    = new Grid();
                    grid.Children.Add(sudControlPrt);
                    sudControlPrt.Width     = Double.NaN;
                    sudControlPrt.Height    = Double.NaN;
                    sudControlPrt.Margin    = new Thickness(48);
                    grid.Margin             = ComputeMargin(area);
                    grid.Measure(sizeGrid);
                    grid.Arrange(new Rect(sizeGrid));
                    grid.UpdateLayout();
                    xpsDocWriter.Write(grid);
                }
            } catch(System.Exception) {
            }
        }


        /// <summary>
        /// Build a grid containing a batch of boards
        /// </summary>
        /// <param name="arrSudBoard">      Array of sudoku boards</param>
        /// <param name="iBoardIndex">      Starting index</param>
        /// <param name="iNbOfHorzBoard">   Number of horizontal boards</param>
        /// <param name="iNbOfVertBoard">   Number of vertical boards</param>
        /// <param name="area">             Printing area</param>
        /// <param name="bSolve">           true to solve the board</param>
        private Grid BuildNBoardPage(SudBoard[] arrSudBoard, int iBoardIndex, int iNbOfHorzBoard, int iNbOfVertBoard, System.Printing.PrintDocumentImageableArea area, bool bSolve) {
            Grid            gridRetVal;
            Size            sizeGrid;
            SudControl      ctl;
            int             iPos;

            gridRetVal        = new Grid();
            gridRetVal.Margin = ComputeMargin(area);
            for (int i = 0; i < iNbOfHorzBoard; i++) {
                gridRetVal.ColumnDefinitions.Add(new ColumnDefinition());
            }
            for (int i = 0; i < iNbOfVertBoard; i++) {
                gridRetVal.RowDefinitions.Add(new RowDefinition());
            }
            sizeGrid        = new Size(area.MediaSizeWidth, area.MediaSizeHeight);
            iPos            = iBoardIndex;
            for (int iRow = 0; iRow < iNbOfVertBoard; iRow++) {
                for (int iCol = 0; iCol < iNbOfHorzBoard; iCol++) {
                    if (bSolve) {
                        arrSudBoard[iPos].Solve(true);
                    }
                    ctl             = sudControl.CloneForPrinting(arrSudBoard[iPos++],
                                                                  false,    // ShowMarker
                                                                  false);   // ShowHint
                    ctl.Margin      = new Thickness(24);
                    ctl.Width       = Double.NaN;
                    ctl.Height      = Double.NaN;
                    gridRetVal.Children.Add(ctl);
                    Grid.SetColumn(ctl, iCol);
                    Grid.SetRow(ctl, iRow);
                }
            }
            gridRetVal.Measure(sizeGrid);
            gridRetVal.Arrange(new Rect(sizeGrid));
            gridRetVal.UpdateLayout();
            return(gridRetVal);
        }

        /// <summary>
        /// Print a batch of boards with their solution
        /// </summary>
        /// <param name="arrSudBoard">      Array of sudoku boards</param>
        /// <param name="iBoardIndex">      Starting index</param>
        /// <param name="iNbOfHorzBoard">   Number of horizontal boards</param>
        /// <param name="iNbOfVertBoard">   Number of vertical boards</param>
        public void PrintNBoard(SudBoard[] arrSudBoard, int iBoardIndex, int iNbOfHorzBoard, int iNbOfVertBoard) {
            System.Printing.PrintDocumentImageableArea                      area = null;
            System.Windows.Xps.XpsDocumentWriter                            xpsDocWriter;
            System.Windows.Documents.Serialization.SerializerWriterCollator serWriterCollator;
            Grid                                                            gridPage;

            try {
                xpsDocWriter = System.Printing.PrintQueue.CreateXpsDocumentWriter("Sudoku", ref area);
                if (xpsDocWriter != null) {
                    serWriterCollator = xpsDocWriter.CreateVisualsCollator();
                    serWriterCollator.BeginBatchWrite();
                    gridPage    = BuildNBoardPage(arrSudBoard,
                                                  iBoardIndex,
                                                  iNbOfHorzBoard,
                                                  iNbOfVertBoard,
                                                  area,
                                                  false);   // Don't solve
                    serWriterCollator.Write(gridPage);
                    gridPage    = BuildNBoardPage(arrSudBoard,
                                                  iBoardIndex,
                                                  iNbOfHorzBoard,
                                                  iNbOfVertBoard,
                                                  area,
                                                  true);   // Solve
                    serWriterCollator.Write(gridPage);  // Write second page
                    serWriterCollator.EndBatchWrite();
                }
            } catch(System.Exception) {
            }
        }

        #endregion

        #region Sinks

        /// <summary>
        /// Called each seconds
        /// </summary>
        /// <param name="sender">   Sender object</param>
        /// <param name="e">        Event arguments</param>
        void m_timer_Tick(object sender, EventArgs e) {
            if (sudControl.Board != null && !sudControl.Board.IsSolutionFound()) {
                m_iTimeInSec++;
            }
            tbTime.Content = TimeSpan.FromSeconds(m_iTimeInSec).ToString();
        }
        
        /// <summary>
        /// Called when window is loaded
        /// </summary>
        /// <param name="sender">   Sender object</param>
        /// <param name="e">        Event argument</param>
        private void Window_Loaded(object sender, RoutedEventArgs e) {
            SudBoardGenerator   sudBoardGen;

            sudBoardGen         = new SudBoardGenerator(new SudBoard[] { new SudBoard9() });
            sudBoardGen.GenerateBoards(new Random(), 0, SudBoard.LevelE.Easy, false);
            sudControl.Board = sudBoardGen.Boards[0];
            RefreshState();
            m_timer.Start();
        }

        /// <summary>
        /// Called when a user changed a board value
        /// </summary>
        /// <param name="sender">   Sender object</param>
        /// <param name="e">        Event arguments</param>
        void sudControl_BoardValueChanged(object sender, EventArgs e) {
            m_bIsDirty  = true;
            RefreshState();
        }
        
        /// <summary>
        /// Called when a board marker changed
        /// </summary>
        /// <param name="sender">   Sender object</param>
        /// <param name="e">        Event arguments</param>
        void sudControl_BoardMarkerChanged(object sender, EventArgs e) {
            m_bIsDirty  = true;
            RefreshState();
        }

        /// <summary>
        /// Called when main window is closing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
            SudokuWPF.Properties.Settings   settings;
            
            if (CheckForSave()) {
                settings                            = SudokuWPF.Properties.Settings.Default;
                settings.Level                      = (int)m_eLevel;
                settings.TimeOut10Sec               = m_bTimeOut;
                settings.Gen16x16                     = (m_iSqrSize == 4);
                settings.SymbolType                 = (int)sudControl.SymbolType;
                settings.OnlyValidMove              = sudControl.OnlyValidMove;
                settings.UseValuePicker             = (sudControl.InputMethod == SudControl.InputMethodE.Picker);
                settings.ShowValuePickerOnMove      = sudControl.ShowValuePickerOnMouseMove;
                settings.BackColorRed               = ((SolidColorBrush)sudControl.BoardBackground).Color.R;
                settings.BackColorGreen             = ((SolidColorBrush)sudControl.BoardBackground).Color.G;
                settings.BackColorBlue              = ((SolidColorBrush)sudControl.BoardBackground).Color.B;
                settings.FixFamilyName              = sudControl.FixValueFontFamily.Source;
                settings.FixColorRed                = ((SolidColorBrush)sudControl.FixValueBrush).Color.R;
                settings.FixColorGreen              = ((SolidColorBrush)sudControl.FixValueBrush).Color.G;
                settings.FixColorBlue               = ((SolidColorBrush)sudControl.FixValueBrush).Color.B;
                settings.UserFamilyName             = sudControl.UserValueFontFamily.Source;
                settings.UserColorRed               = ((SolidColorBrush)sudControl.UserValueBrush).Color.R;
                settings.UserColorGreen             = ((SolidColorBrush)sudControl.UserValueBrush).Color.G;
                settings.UserColorBlue              = ((SolidColorBrush)sudControl.UserValueBrush).Color.B;
                settings.HintFamilyName             = sudControl.HintValueFontFamily.Source;
                settings.HintColorRed               = ((SolidColorBrush)sudControl.HintValueBrush).Color.R;
                settings.HintColorGreen             = ((SolidColorBrush)sudControl.HintValueBrush).Color.G;
                settings.HintColorBlue              = ((SolidColorBrush)sudControl.HintValueBrush).Color.B;
                settings.ThinLineColorRed           = sudControl.ThinLineColor.R;
                settings.ThinLineColorGreen         = sudControl.ThinLineColor.G;
                settings.ThinLineColorBlue          = sudControl.ThinLineColor.B;
                settings.ThickLineColorRed          = sudControl.ThickLineColor.R;
                settings.ThickLineColorGreen        = sudControl.ThickLineColor.G;
                settings.ThickLineColorBlue         = sudControl.ThickLineColor.B;
                settings.Left                       = Left;
                settings.Top                        = Top;
                settings.Width                      = Width;
                settings.Height                     = Height;
                settings.Save();
            } else {
                e.Cancel = true;
            }
        }
        #endregion

    } // Class
} // Namespace
