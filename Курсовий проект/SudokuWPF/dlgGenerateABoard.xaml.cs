﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Sudoku2;

namespace SudokuWPF {
    /// <summary>
    /// Interaction logic for dlgGenerateABoard.xaml
    /// </summary>
    public partial class dlgGenerateABoard : Window {
        
        /// <summary>Level of ddifficulty of the board</summary>
        public SudBoard.LevelE                  m_eLevel;
        /// <summary>true to have a 10 sec timeout on board generation</summary>
        public bool                             m_bTimeOut;
        /// <summary>Generated sudoku board</summary>
        public SudBoard[]                       m_arrSudBoard;
        /// <summary>Board size (3 for 9x9, 4 for 16x16)</summary>
        public int                              m_iSqrSize;
        /// <summary>Random number generator</summary>
        private Random                          m_rnd;
        /// <summary>Progress monitor delegate</summary>
        private SudBoardGenerator.delProgress   m_delProgress;
        /// <summary>Boards generator</summary>
        private SudBoardGenerator               m_sudBoardGen;
        /// <summary>True to generate multi boards</summary>
        private bool                            m_bMultiBoard;
        /// <summary>true if user cancel generation</summary>
        private bool                            m_bCancel;
        /// <summary>Number of boards to generates for 9x9 boards (batch)</summary>
        private const int                       m_iMultiBoardCountFor9x9 = 6;
        /// <summary>Number of boards to generates for 16x16 boards (batch)</summary>
        private const int                       m_iMultiBoardCountFor16x16 = 2;

        /// <summary>
        /// Public ctor
        /// </summary>
        public dlgGenerateABoard() {
            InitializeComponent();
            m_eLevel        = SudBoard.LevelE.Easy;
            m_bTimeOut      = true;
            m_arrSudBoard   = null;
            m_sudBoardGen   = null;
            m_iSqrSize      = 3;
            m_bMultiBoard   = false;
            m_bCancel       = false;
            m_delProgress   = new SudBoardGenerator.delProgress(this.m_sudBoardGen_ProgressEventOnThread);
            Loaded         += new RoutedEventHandler(dlgGenerateABoard_Loaded);
        }

        /// <summary>
        /// Class ctor
        /// </summary>
        /// <param name="rnd">          Random number generator</param>
        /// <param name="eLevel">       Initial Board level</param>
        /// <param name="bTimeOut">     Initial timeout</param>
        /// <param name="iSqrSize">     Initial board size</param>
        /// <param name="bMultiBoard">  true to generate multiple boards (batch)</param>
        public dlgGenerateABoard(Random rnd, SudBoard.LevelE eLevel, bool bTimeOut, int iSqrSize, bool bMultiBoard) : this() {
            m_rnd           = rnd;
            m_eLevel        = eLevel;
            m_bTimeOut      = bTimeOut;
            m_iSqrSize      = iSqrSize;
            m_arrSudBoard   = null;
            m_bMultiBoard   = bMultiBoard;
        }
        
        /// <summary>
        /// Call when the dialog is loaded
        /// </summary>
        /// <param name="sender">   Sender object</param>
        /// <param name="e">        Event argument</param>
        private void dlgGenerateABoard_Loaded(object sender, RoutedEventArgs e) {
            switch(m_eLevel) {
            case SudBoard.LevelE.Easy:
                radioEasy.IsChecked         = true;
                break;
            case SudBoard.LevelE.Medium:
                radioMedium.IsChecked       = true;
                break;
            case SudBoard.LevelE.Difficult:
                radioDifficult.IsChecked    = true;
                break;
            default:
                radioEasy.IsChecked         = true;
                break;
            }
            if (m_bTimeOut) {
                radio10Sec.IsChecked        = true;
            } else {
                radioUnlimited.IsChecked    = true;
            }
            if (m_iSqrSize == 3) {
                radio9x9.IsChecked          = true;
            } else {
                radio16x16.IsChecked        = true;
            }
        }

        /// <summary>
        /// Handle the Progress event on the UI thread
        /// </summary>
        /// <param name="sender">   Sender object</param>
        /// <param name="e">        Event handler</param>
        private void m_sudBoardGen_ProgressEventOnThread(object sender, SudBoardGenerator.ProgressEventArg e) {
            int     iBoardSize;
            int     iBoardCount;

            if (e.BoardIndex == Int32.MaxValue) {
                butCancel.IsEnabled     = false;
                butGen.IsEnabled        = true;
                DialogResult            = !(m_bCancel && m_bMultiBoard);
            } else {
                iBoardSize              = m_iSqrSize * m_iSqrSize * m_iSqrSize * m_iSqrSize;
                progressBar1.Value      = e.Progress + e.BoardIndex * iBoardSize;
                if (m_bMultiBoard) {
                    iBoardCount = (m_iSqrSize == 3) ? m_iMultiBoardCountFor9x9 : m_iMultiBoardCountFor16x16;
                    tbText.Content = "[" + (e.BoardIndex + 1).ToString() + " / " + iBoardCount.ToString() + "] " + e.UsedCell.ToString() + " / " + e.Progress.ToString();
                } else {
                    tbText.Content  = e.UsedCell.ToString() + " / " + e.Progress.ToString();
                }
            }
        }

        /// <summary>
        /// Handle sudoku board event. Can be called from a different thread.
        /// </summary>
        /// <param name="sender">   Sender object</param>
        /// <param name="e">        Event argument</param>
        private void m_sudBoardGen_ProgressEvent(object sender, SudBoardGenerator.ProgressEventArg e) {
            this.Dispatcher.Invoke(m_delProgress, new object[] { sender, e });  // Dispatch to the UI thread
        }

        /// <summary>
        /// Cancel the board generation
        /// </summary>
        /// <param name="sender">   Sender object</param>
        /// <param name="e">        Event argument</param>
        private void butCancel_Click(object sender, RoutedEventArgs e) {
            m_bCancel = true;
            if (m_sudBoardGen == null) {
                DialogResult = false;
            } else {
                m_sudBoardGen.CancelGeneration();
                DialogResult = null;
            }
        }

        /// <summary>
        /// Generate a board
        /// </summary>
        /// <param name="sender">   Sender object</param>
        /// <param name="e">        Event handler</param>
        private void butGen_Click(object sender, RoutedEventArgs e) {
            DateTime    start;
            int         iBoardSize;
            int         iBoardCount;
            
            if (radioEasy.IsChecked == true) {
                m_eLevel = SudBoard.LevelE.Easy;
            } else if (radioMedium.IsChecked == true) {
                m_eLevel = SudBoard.LevelE.Medium;
            } else if (radioDifficult.IsChecked == true) {
                m_eLevel = SudBoard.LevelE.Difficult;
            }
            m_bTimeOut                      = (radio10Sec.IsChecked == true);
            m_iSqrSize                      = (radio9x9.IsChecked == true) ? 3 : 4;
            iBoardSize                      = (radio9x9.IsChecked == true) ? 81 : 256;
            iBoardCount                     = (m_bMultiBoard) ? ((m_iSqrSize == 3) ? m_iMultiBoardCountFor9x9 : m_iMultiBoardCountFor16x16) : 1;
            progressBar1.Minimum            = 0;
            progressBar1.Maximum            = iBoardCount * iBoardSize;
            progressBar1.Value              = 0;
            butGen.IsEnabled                = false;
            m_arrSudBoard                   = new SudBoard[iBoardCount];
            for (int iIndex = 0; iIndex < iBoardCount; iIndex++) {
                if (m_iSqrSize == 3) {
                    m_arrSudBoard[iIndex] = new SudBoard9();
                } else {
                    m_arrSudBoard[iIndex] = new SudBoard16();
                }
            }
            m_sudBoardGen                   = new SudBoardGenerator(m_arrSudBoard);
            m_sudBoardGen.ProgressEvent    += new SudBoardGenerator.delProgress(m_sudBoardGen_ProgressEvent);
            start                           = DateTime.Now;
            if (m_bTimeOut) {
                m_sudBoardGen.GenerateBoards(m_rnd, TimeSpan.FromSeconds(10), m_eLevel, true);
            } else {
                m_sudBoardGen.GenerateBoards(m_rnd, 0, m_eLevel, true);
            }
        }
    }
}
