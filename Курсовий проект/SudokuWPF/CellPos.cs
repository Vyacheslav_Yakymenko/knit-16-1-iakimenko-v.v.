﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sudoku2 {
    /// <summary>Координати комірки</summary>
    public struct CellPos {
        /// <summary>Номер рядка</summary>
        public int  X;
        /// <summary>Номер стовпця</summary>
        public int  Y;
        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="x">Номер рядка</param>
        /// <param name="y">Номер стовпця</param>
        public CellPos(int x, int y) { X = x; Y = y; }
    }
}
