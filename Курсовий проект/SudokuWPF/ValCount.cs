﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sudoku2 {
    /// <summary>
    /// Handle the count of each values for lines/columns or squares. This class is abstract to optimize some highly used methods
    /// for 9x9 and 16x16 boards.
    /// </summary>
    public abstract class ValCount {
        /// <summary>Number of possible values: 9 for 9x9 and 16 for 16x16</summary>
        protected int       m_iValCount;
        /// <summary>Count for each group position/values</summary>
        protected short[]   m_arrCount;
        /// <summary>Number of slot used per group</summary>
        protected short[]   m_arrUsedSlotPerGroup;

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="iValCount">    Nb of possible values</param>
        protected ValCount(int iValCount) {
            m_iValCount             = iValCount;
            m_arrCount              = new short[m_iValCount * m_iValCount];
            m_arrUsedSlotPerGroup   = new short[m_iValCount];
        }

        /// <summary>
        /// Copy ctor
        /// </summary>
        /// <param name="valCount"> Template</param>
        public ValCount(ValCount valCount) : this(valCount.m_iValCount) {
            valCount.CopyTo(this);
        }

        /// <summary>
        /// Copy the template value into this instance
        /// </summary>
        /// <param name="valCount"> Template</param>
        public void CopyTo(ValCount valCount) {
            if (m_iValCount != valCount.m_iValCount) {
                throw new System.ApplicationException("Different settings!");
            }
            m_arrCount.CopyTo(valCount.m_arrCount, 0);
            m_arrUsedSlotPerGroup.CopyTo(valCount.m_arrUsedSlotPerGroup, 0);
        }

        /// <summary>
        /// Return/set the count of this value for the specified slot
        /// </summary>
        /// <param name="iPos"> Group position</param>
        /// <param name="iVal"> Value to  query</param>
        public abstract int this[int iPos, int iVal] { get; }

        /// <summary>
        /// Get the value mask for this position. Each bit represent if a value is present
        /// </summary>
        /// <param name="iPos"> Group position</param>
        /// <returns>
        /// Value mask
        /// </returns>
        public int GetValueMask(int iPos) {
            int     iRetVal = 0;
            int     iBit;
            int     iStartPos;
            
            iBit        = 1;
            iStartPos   = iPos << 4;
            for (int iIndex = 0; iIndex < m_iValCount; iIndex++) {
                if (m_arrCount[iStartPos++] != 0) {
                    iRetVal |= iBit;
                }
                iBit <<= 1;
            }
            return(iRetVal);
        }

        /// <summary>
        /// Add or remove a reference to a value for a position
        /// </summary>
        /// <param name="iPos">     Group position</param>
        /// <param name="iVal">     Value</param>
        /// <param name="iInc">     -1 or 1</param>
        public abstract void Add(int iPos, int iVal, int iInc);
        
        /// <summary>
        /// Number of free slots in the group
        /// </summary>
        /// <param name="iPos"> Group position</param>
        /// <returns>
        /// Number of free slot in the group
        /// </returns>
        public int FreeSlot(int iPos) {
            return(m_iValCount - m_arrUsedSlotPerGroup[iPos]);
        }
    } // Class
} // Namespace
