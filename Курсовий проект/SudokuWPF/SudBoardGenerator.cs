﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sudoku2 {
    /// <summary>
    /// Sudoku board generator. Generates random 9x9 or 16x16 boards synchronously or asynchronously
    /// </summary>
    public class SudBoardGenerator {
        
        /// <summary>Progress event argument</summary>
        public class ProgressEventArg : System.EventArgs {
            /// <summary>Progress value</summary>
            public int      Progress;
            /// <summary>Number of cells used</summary>
            public int      UsedCell;
            /// <summary>Board index</summary>
            public int      BoardIndex;
        }

        /// <summary>
        /// Delegate for progress event
        /// </summary>
        /// <param name="sender">   Sender object</param>
        /// <param name="e">        Event argument</param>
        public  delegate void       delProgress(System.Object sender, ProgressEventArg e);

        /// <summary>Array of boards to generate</summary>
        private SudBoard[]          m_arrSudBoard;
        /// <summary>true to cancel generation</summary>
        private bool                m_bCancel;
        /// <summary>Called to advise of generation progress</summary>
        public event delProgress    ProgressEvent;

        /// <summary>
        /// Class ctor
        /// </summary>
        /// <param name="arrSudBoard">  Boards to be generated</param>
        public SudBoardGenerator(SudBoard[] arrSudBoard) {
            m_arrSudBoard   = arrSudBoard;
            m_bCancel       = false;
        }
        
        /// <summary>
        /// Cancel the boards generation
        /// </summary>
        public void CancelGeneration() {
            m_bCancel   = true;
        }

        /// <summary>
        /// true if generation has been canceled
        /// </summary>
        public bool IsCancel {
            get {
                return(m_bCancel);
            }
        }

        /// <summary>
        /// Generated boards
        /// </summary>
        public SudBoard[] Boards {
            get {
                return(m_arrSudBoard);
            }
        }

        /// <summary>
        /// Generates a sequence of shuffled number
        /// </summary>
        /// <param name="rnd">      Random generator</param>
        /// <param name="arrValues">Array to fill</param>
        /// <param name="iStart">   Starting position in the array</param>
        /// <param name="iCount">   Number of position to fill</param>
        private void GenShuffledValues(Random rnd, byte[] arrValues, int iStart, int iCount) {
            int     iPivot;
            byte    cTmp;

            for (int iIndex = 0; iIndex < iCount; iIndex++) {
                arrValues[iStart+iIndex] = (byte)iIndex;
            }
            for (int iIndex = 0; iIndex < iCount; iIndex++) {
                iPivot = rnd.Next(iCount);
                if (iPivot != iIndex) {
                    cTmp                        = arrValues[iStart+iIndex];
                    arrValues[iStart+iIndex]    = arrValues[iStart+iPivot];
                    arrValues[iStart+iPivot]    = cTmp;
                }
            }

        }
        
        /// <summary>
        /// Generates a random board completely filled
        /// </summary>
        /// <param name="sudBoard">     Sudoku board to fill</param>
        /// <param name="arrValues">    Array of shuffled values to use to assure a random board</param>
        /// <param name="iPos">         Position at which we're filling</param>
        /// <param name="iTotalPerm">   Total permutations tryed</param>
        /// <param name="iMaxPerm">     Maximum allowed permutations. Board failed if it takes more</param>
        /// <returns>
        /// true if succeed, false if dead-end and need backtracking
        /// </returns>
        private bool GenerateFullBoard(SudBoard sudBoard, byte[] arrValues, int iPos, ref int iTotalPerm, int iMaxPerm) {
            bool    bRetVal = false;
            int     iValIndex;
            
            if (iTotalPerm < iMaxPerm) {
                if (iPos < sudBoard.BoardSize) {
                    iValIndex = 0;
                    while (!bRetVal && iValIndex < sudBoard.LineSize) {
                        if (sudBoard.PlayAt(iPos, 
                                            (int)arrValues[iPos * sudBoard.LineSize + iValIndex]+1,
                                            false,   // Not a user move
                                            false,   // Don't record
                                            true)) { // Validate the move
                            iTotalPerm++;
                            bRetVal = GenerateFullBoard(sudBoard, arrValues, iPos+1, ref iTotalPerm, iMaxPerm);
                            if (!bRetVal) {
                                sudBoard.PlayAt(iPos,
                                                0,
                                                false,   // Not a user move
                                                false,   // No Undo recording
                                                false);  // No validation
                            }
                        }
                        iValIndex++;
                    }
                } else {
                    bRetVal = true;
                }
            } else {
                bRetVal = false;
            }
            return(bRetVal);
        }

        /// <summary>
        /// Generates a random board completely filled
        /// </summary>
        /// <param name="rnd">      Random number generator</param>
        /// <param name="sudBoard"> Sudoku board to fill</param>
        /// <param name="iTryCount">Number of times the board has to be generated</param>
        public void GenerateFullBoard(Random rnd, SudBoard sudBoard, out int iTryCount) {
            byte[]  arrValues;
            int     iArrSize;
            int     iTotalPerm;

            iArrSize    = sudBoard.BoardSize * sudBoard.LineSize;
            arrValues   = new byte[iArrSize];
            iTryCount   = 0;
            do {
                sudBoard.Clear();
                iTotalPerm = 0;
                iTryCount++;
                for (int iIndex = 0; iIndex < iArrSize; iIndex += sudBoard.LineSize) {
                    GenShuffledValues(rnd, arrValues, iIndex, sudBoard.LineSize);
                }
            } while (!GenerateFullBoard(sudBoard, arrValues, 0, ref iTotalPerm, 100000));
        }

        /// <summary>
        /// Trigger the Progress event
        /// </summary>
        /// <param name="e">    Event argument</param>
        protected virtual void OnProgressEvent(ProgressEventArg e) {
            if (ProgressEvent != null) {
                ProgressEvent(this, e);
            }
        }

        /// <summary>
        /// Generates a random board ready to be filled
        /// </summary>
        /// <param name="rnd">          Random number generator</param>
        /// <param name="sudBoard">     Sudoku board to generate</param>
        /// <param name="iBoardIndex">  Index of the board being generated. This index is sent as part of the progress event</param>
        /// <param name="spanTimeOut">  Amount of time allow for the search</param>
        /// <param name="iTiledCount">  Number of tiles desired</param>
        /// <param name="eLevel">       Difficulty level of the board to generate</param>
        private void GenerateBoard(Random rnd, SudBoard sudBoard, int iBoardIndex, TimeSpan spanTimeOut, int iTiledCount, SudBoard.LevelE eLevel) {
            byte[]              arrRndPos;
            int                 iIndex;
            int                 iPos;
            int                 iVal;
            int                 iTryCount;
            int                 iSolutionFound;
            DateTime            dateEnd;
            ProgressEventArg    eventArg;
            SudBoard.LevelE     eBoardLevel;

            m_bCancel           = false;
            dateEnd             = DateTime.Now + spanTimeOut;
            eventArg            = new ProgressEventArg();
            eventArg.Progress   = 0;
            eventArg.UsedCell   = sudBoard.UsedCell;
            eventArg.BoardIndex = iBoardIndex;
            OnProgressEvent(eventArg);
            GenerateFullBoard(rnd, sudBoard, out iTryCount);
            arrRndPos = new byte[sudBoard.BoardSize];
            GenShuffledValues(rnd, arrRndPos, 0, sudBoard.BoardSize);
            iIndex = 0;
            while (iIndex < sudBoard.BoardSize && DateTime.Now < dateEnd && sudBoard.UsedCell > iTiledCount && !m_bCancel) {
                iPos    = (int)arrRndPos[iIndex];
                iVal    = sudBoard[iPos];
                sudBoard.PlayAt(iPos,
                                0,
                                false,   // Not a user move
                                false,   // No undo recording
                                false);  // No validation
                iSolutionFound = sudBoard.Solve(false, out eBoardLevel);
                eventArg.Progress++;
                eventArg.UsedCell   = sudBoard.UsedCell;
                OnProgressEvent(eventArg);
#if DEBUG                
                if (iSolutionFound == 0) {
                    throw new System.ApplicationException("Coding error - Solving found 0 solution");
                }
#endif                
                if (iSolutionFound > 1 || eBoardLevel > eLevel) {
                    sudBoard.PlayAt(iPos,
                                    iVal,
                                    false,   // Not a user move
                                    false,   // No undo recording
                                    false);  // No validation
                }
                iIndex++;
            }
            eventArg.Progress = sudBoard.BoardSize;
            OnProgressEvent(eventArg);
        }

        /// <summary>
        /// Generates synchronously one or more random boards ready to be filled
        /// </summary>
        /// <param name="rnd">          Random number generator</param>
        /// <param name="spanTimeOut">  Amount of time allow for the search</param>
        /// <param name="iTiledCount">  Number of tiles desired</param>
        /// <param name="eLevel">       Difficulty level of the board to generate</param>
        private void GenerateBoards(Random rnd, TimeSpan spanTimeOut, int iTiledCount, SudBoard.LevelE eLevel) {
            int                 iIndex = 0;
            int                 iBoardCount;
            SudBoard            sudBoard;
            ProgressEventArg    e;

            iBoardCount = m_arrSudBoard.Length;
            iIndex      = 0;
            while (iIndex < iBoardCount && !m_bCancel) {
                sudBoard = m_arrSudBoard[iIndex];
                GenerateBoard(rnd, sudBoard, iIndex, spanTimeOut, iTiledCount, eLevel);
                iIndex++;
            }
            e               = new ProgressEventArg();
            e.BoardIndex    = Int32.MaxValue;
            e.Progress      = Int32.MaxValue;
            OnProgressEvent(e);
        }

        /// <summary>
        /// Generates asynchronously one or more random boards ready to be filled
        /// </summary>
        /// <param name="rnd">          Random number generator</param>
        /// <param name="spanTimeOut">  Amount of time allow for the search</param>
        /// <param name="iTiledCount">  Number of tiles desired</param>
        /// <param name="eLevel">       Difficulty level of the board to generate</param>
        private void GenerateBoardsAsync(Random  rnd, TimeSpan spanTimeOut, int iTiledCount, SudBoard.LevelE eLevel) {
            Action  delGenerateBoards;

            delGenerateBoards = () => GenerateBoards(rnd, spanTimeOut, iTiledCount, eLevel);
            Task.Factory.StartNew(delGenerateBoards);
        }        
        
        /// <summary>
        /// Generates a group of random boards ready to be filled using a timeout value
        /// </summary>
        /// <param name="rnd">          Random number generator</param>
        /// <param name="spanTimeOut">  Amount of time allow for the search</param>
        /// <param name="eLevel">       Difficulty level of the board</param>
        /// <param name="bAsync">       true to use a worker thread. ProgressEvent will be sent with a final Int32.Max value</param>
        public void GenerateBoards(Random rnd, TimeSpan spanTimeOut, SudBoard.LevelE eLevel, bool bAsync) {
            if (bAsync) {
                GenerateBoardsAsync(rnd, spanTimeOut, 0, eLevel);
            } else {
                GenerateBoards(rnd, spanTimeOut, 0, eLevel);
            }
        }

        /// <summary>
        /// Generates a group of random boards ready to be filled
        /// </summary>
        /// <param name="rnd">          Random number generator</param>
        /// <param name="iTiledCount">  Number of tiles desired</param>
        /// <param name="eLevel">       Difficulty level of the board</param>
        /// <param name="bAsync">       true to use a worker thread. ProgressEvent will be sent with a final Int32.Max value</param>
        public void GenerateBoards(Random rnd, int iTiledCount, SudBoard.LevelE eLevel, bool bAsync) {
            if (bAsync) {
                GenerateBoardsAsync(rnd, TimeSpan.FromDays(1), iTiledCount, eLevel);
            } else {
                GenerateBoards(rnd, TimeSpan.FromDays(1), iTiledCount, eLevel);
            }
        }
    } // Class
} // Namespace
