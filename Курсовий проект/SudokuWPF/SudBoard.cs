﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Sudoku2 {
    /// <summary>
    /// Реалізація карти судоку. Абстрактний клас, для створення реальних об'єктів використовуються карти 9х9 або 16х16.
    /// </summary>
    public abstract class SudBoard {

        #region TypeDefinitions
        /// <summary>Тип значення в комірці карти судоку</summary>
        public enum ValueTypeE : short {
            /// <summary>Пусто</summary>
            None    =   0,
            /// <summary>Фіксоване значення</summary>
            Fix     =   1,
            /// <summary>Значення, яке можна міняти</summary>
            User    =   2
        }

        /// <summary>Рівень складності карти судоку</summary>
        public enum LevelE {
            /// <summary>Простий</summary>
            Easy        =   0,
            /// <summary>Середній</summary>
            Medium      =   1,
            /// <summary>Продвинутий</summary>
            Difficult   =   2
        }

        /// <summary>Описує дію, яку можна відмінити</summary>
        private struct Move {
            /// <summary>
            /// Class ctor
            /// </summary>
            /// <param name="iPos">     Координати комірки</param>
            /// <param name="iOldVal">  Минуле значення</param>
            public       Move(int iPos, int iOldVal) { m_nPos = (short)iPos; m_nOldVal = (short)iOldVal; }
            /// <summary>Координати комірки</summary>
            public short m_nPos;
            /// <summary>Минуле значення, до зміни</summary>
            public short m_nOldVal;
        };
        #endregion

        #region Members
        /// <summary>Кількість заповнених значень на рядку</summary>
        protected ValCount          m_LineValCount;
        /// <summary>Кіькість заповнених значень у стовпці</summary>
        protected ValCount          m_ColValCount;
        /// <summary>Кількість заповнених значень у квадраті</summary>
        protected ValCount          m_SqrValCount;
        /// <summary>Одномірний массив -> Квадрат</summary>
        protected int[]             m_arrPosToSqr;
        /// <summary>Квадрат -> Одномірний массив</summary>
        protected int[]             m_arrSqrToPos;
        /// <summary>3 (9x9) або 4 (16x16)</summary>
        protected int               m_iSquareSize;
        /// <summary>9 (9x9) або 16 (16x16)</summary>
        protected int               m_iLineSize;
        /// <summary>81 (9x9) або 256 (16x16)</summary>
        protected int               m_iBoardSize;
        /// <summary>Значення з карти судоку</summary>
        protected short[]           m_arrBoardVal;
        /// <summary>Масив типів значень комірок</summary>
        private ValueTypeE[]        m_arrBoardValType;
        /// <summary>Перетворення з одномірного массиву в квадрат (9x9)</summary>
        private static int[]        m_arr9x9PosToSqr;
        /// <summary>Перетворення з одномірного массиву в квадрат (16x16)</summary>
        private static int[]        m_arr16x16PosToSqr;
        /// <summary>Перетворення з квадрату в одномірний массив (9x9)</summary>
        private static int[]        m_arr9x9SqrToPos;
        /// <summary>Перетворення з квадрату в одномірний массив (16x16)</summary>
        private static int[]        m_arr16x16SqrToPos;
        /// <summary>Массив значень карти, які необхідно перевіряти</summary>
        private int[]               m_arrPosToCheck;
        /// <summary>Массив значень (цифр) для кожної пустої комірки (1 bit per value)</summary>
        private int[]               m_arrValMask;
        /// <summary>Стек ходів, які можливо відмінити</summary>
        private Stack<Move>         m_stackUndoMoves;
        /// <summary>Стек ходів, які можливо повернути</summary>
        private Stack<Move>         m_stackRedoMoves;
        /// <summary>Кількість комірок, які мають не нульове значення</summary>
        private int                 m_iUsedCount;
        #endregion

        #region Constructors
        /// <summary>
        /// Static Ctor. Build the 9x9 and 16x16 position -> square and square -> position map
        /// </summary>
        static SudBoard() {
            int     iPos;

            m_arr9x9PosToSqr    = new int[81];
            m_arr16x16PosToSqr  = new int[256];
            iPos                = 0;
            for (int iLine = 0; iLine < 9; iLine++) {
                for (int iCol = 0; iCol < 9; iCol++) {
                    m_arr9x9PosToSqr[iPos++] = iLine / 3 * 3 + iCol / 3;
                }
            }
            iPos = 0;
            for (int iLine = 0; iLine < 16; iLine++) {
                for (int iCol = 0; iCol < 16; iCol++) {
                    m_arr16x16PosToSqr[iPos++] = iLine / 4 * 4 + iCol / 4;
                }
            }
            m_arr9x9SqrToPos    = new int[9];
            m_arr16x16SqrToPos  = new int[16];
            for (int iIndex = 0; iIndex < 9; iIndex++) {
                m_arr9x9SqrToPos[iIndex] = iIndex / 3 * 27 + (iIndex % 3) * 3;
            }
            for (int iIndex = 0; iIndex < 16; iIndex++) {
                m_arr16x16SqrToPos[iIndex] = iIndex / 4 * 64 + (iIndex % 4) * 4;
            }
        }

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="iSquareSize">  Square size</param>
        protected SudBoard(int iSquareSize) {
            if (iSquareSize != 3 && iSquareSize != 4) {
                throw new System.ApplicationException("3 or 4 please!");
            }
            m_iSquareSize       = iSquareSize;
            m_iLineSize         = iSquareSize * iSquareSize;
            m_iBoardSize        = m_iLineSize * m_iLineSize;
            m_arrBoardVal       = new short[m_iBoardSize];
            m_arrBoardValType   = new ValueTypeE[m_iBoardSize];
            if (iSquareSize == 3) {
                m_LineValCount      = new ValCount9();
                m_ColValCount       = new ValCount9();
                m_SqrValCount       = new ValCount9();
            } else {
                m_LineValCount      = new ValCount16();
                m_ColValCount       = new ValCount16();
                m_SqrValCount       = new ValCount16();
            }
            m_arrPosToCheck     = new int[m_iBoardSize];
            m_arrValMask        = new int[m_iBoardSize];
            m_arrPosToSqr       = (iSquareSize == 3) ? m_arr9x9PosToSqr : m_arr16x16PosToSqr;
            m_arrSqrToPos       = (iSquareSize == 3) ? m_arr9x9SqrToPos : m_arr16x16SqrToPos;
            m_stackUndoMoves    = new Stack<Move>(m_iBoardSize);
            m_stackRedoMoves    = new Stack<Move>(m_iBoardSize);
            m_iUsedCount        = 0;
        }
        #endregion

        #region Operations
        /// <summary>
        /// Copy the content of the board into the specified one
        /// </summary>
        /// <param name="brd">  Destination board</param>
        public void CopyTo(SudBoard brd) {
            Move[]  arrMove;
            
            if (brd.m_iSquareSize != m_iSquareSize) {
                throw new System.ApplicationException("Different board settings!");
            }
            m_arrBoardVal.CopyTo(brd.m_arrBoardVal, 0);
            m_arrBoardValType.CopyTo(brd.m_arrBoardValType, 0);
            m_LineValCount.CopyTo(brd.m_LineValCount);
            m_ColValCount.CopyTo(brd.m_ColValCount);
            m_SqrValCount.CopyTo(brd.m_SqrValCount);
            arrMove                 = m_stackUndoMoves.ToArray();
            Array.Reverse(arrMove);
            brd.m_stackUndoMoves    = new Stack<Move>(arrMove);
            arrMove                 = m_stackRedoMoves.ToArray();
            Array.Reverse(arrMove);
            brd.m_stackRedoMoves    = new Stack<Move>(arrMove);
            brd.m_iUsedCount        = m_iUsedCount;
        }
        
        /// <summary>
        /// Clone a sudoku board
        /// </summary>
        /// <returns>
        /// New clone of the sudoku board
        /// </returns>
        public abstract SudBoard Clone();

        /// <summary>
        /// Clear the content of the board
        /// </summary>
        public void Clear() {
            UndoAllMove(true);
            for (int iPos = 0; iPos < m_iBoardSize; iPos++) {
                if (m_arrBoardVal[iPos] != 0) {
                    PlayAt(iPos, 0, false, false, false);
                }
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Square size (3 for 9x9 or 4 for 16x16)
        /// </summary>
        public int SquareSize {
            get {
                return(m_iSquareSize);
            }
        }

        /// <summary>
        /// Line size (9 for 9x9 or 16 for 16x16). It's also the number of possible values
        /// </summary>
        public int LineSize {
            get {
                return(m_iLineSize);
            }
        }
        
        /// <summary>
        /// Number of values in the board (81 or 256)
        /// </summary>
        public int BoardSize {
            get {
                return(m_iBoardSize);
            }
        }

        /// <summary>
        /// Number of cell with a non-zero value
        /// </summary>
        public int UsedCell {
            get {
                return(m_iUsedCount);
            }
        }

        /// <summary>
        /// Number of cells with a zero value
        /// </summary>
        public int FreeCell {
            get {
                return(m_iBoardSize - m_iUsedCount);
            }
        }

        /// <summary>
        /// Is a correct solution has been found?
        /// </summary>
        public bool IsSolutionFound() {
            bool    bRetVal;
            int     iIndex;
            
            bRetVal = true;
            iIndex  = 0;
            while (bRetVal && iIndex < m_iLineSize) {
                if (m_LineValCount.FreeSlot(iIndex) != 0 ||
                    m_SqrValCount.FreeSlot(iIndex)  != 0 ||
                    m_ColValCount.FreeSlot(iIndex)  != 0) {
                    bRetVal = false;
                } else {
                    iIndex++;
                }
            }
            return(bRetVal);
        }
        #endregion

        #region Indexers
        /// <summary>
        /// Default indexer. Returns the value at the specified cell position
        /// </summary>
        /// <param name="ptCellPos">    Cell position</param>
        /// <returns>
        /// Value at position
        /// </returns>
        public int this[CellPos ptCellPos] {
            get {
                return(m_arrBoardVal[ptCellPos.Y * m_iLineSize + ptCellPos.X]);
            }
        }

        /// <summary>
        /// Default indexer. Returns the value at the specified cell absolute position
        /// </summary>
        /// <param name="iPos">     Position (0..max)</param>
        /// <returns>
        /// Value at position
        /// </returns>
        public int this[int iPos] {
            get {
                return(m_arrBoardVal[iPos]);
            }
        }
        #endregion

        #region Information
        /// <summary>
        /// Type of the value at specified cell
        /// </summary>
        /// <param name="ptCellPos">    Cell position</param>
        /// <returns>
        /// Type of value
        /// </returns>
        public ValueTypeE ValueType(CellPos ptCellPos) {
            return(m_arrBoardValType[ptCellPos.Y * m_iLineSize + ptCellPos.X]);
        }

        /// <summary>
        /// Get the square position (0..max) from cell position
        /// </summary>
        /// <param name="ptCellPos">    Cell position</param>
        /// <returns>
        /// Square position
        /// </returns>
        public int GetSquareFromPos(CellPos ptCellPos) {
            return(m_arrPosToSqr[ptCellPos.Y * m_iLineSize + ptCellPos.X]);
        }

        /// <summary>
        /// Get the position of the first cell of a square
        /// </summary>
        /// <param name="iSqr"> Square (0..max)</param>
        /// <returns>
        /// Cell position
        /// </returns>
        public CellPos GetPosFromSquare(int iSqr) {
            int     iPos;

            iPos = m_arrSqrToPos[iSqr];
            return(new CellPos(iPos % m_iLineSize, iPos / m_iLineSize));
        }
        
        /// <summary>
        /// Return the number of valid moves at the specified position
        /// </summary>
        /// <param name="ptCellPos">    Cell position</param>
        /// <returns>
        /// Number of possible moves
        /// </returns>
        public int GetPossibleValCount(CellPos ptCellPos) {
            int     iRetVal;
            
            iRetVal = 0;
            for (int iVal = 1; iVal <= m_iLineSize; iVal++) {
                if (IsValueValid(ptCellPos, iVal)) {
                    iRetVal++;
                }
            }
            return(iRetVal);
        }

        /// <summary>
        /// Find the list of valid moves for the specified cell
        /// </summary>
        /// <param name="ptCellPos">    Cell position</param>
        /// <returns>
        /// Bit mask of valid values
        /// </returns>
        public int GetPossibleValMask(CellPos ptCellPos) {
            int     iRetVal = 0;
            int     iOldVal;
            int     iValBit;

            if (ValueType(ptCellPos) != ValueTypeE.Fix) {
                iOldVal = this[ptCellPos];
                if (iOldVal != 0) {
                    PlayAt(ptCellPos,
                           0,       // Value
                           false,   // Not a user move
                           false,   // No recording
                           false);  // No validation
                }
                iValBit = 1;
                for (int iVal = 1; iVal <= m_iLineSize; iVal++) {
                    if (IsValueValid(ptCellPos, iVal)) {
                        iRetVal |= iValBit;
                    }
                    iValBit <<= 1;
                }
                if (iOldVal != 0) {
                    PlayAt(ptCellPos,
                           iOldVal,
                           true,    // User move
                           false,   // No recording
                           false);  // No validation
                }
            }
            return(iRetVal);
        }
        #endregion

        #region Moves
        /// <summary>
        /// Checks if a value is valid for the specified cell. A valid value doesn't duplicate a value in a square, line or column.
        /// </summary>
        /// <param name="iPos"> Position of the cell (0..max)</param>
        /// <param name="iVal"> Value (1..max)</param>
        /// <returns>
        /// true if valid, false if not.
        /// </returns>
        protected abstract bool IsValueValid(int iPos, int iVal);

        /// <summary>
        /// Checks if a value is valid for the specified cell. A valid value doesn't duplicate a value in a square, line or column.
        /// </summary>
        /// <param name="ptCellPos">    Cell position</param>
        /// <param name="iVal">         Value (1..max)</param>
        /// <returns>
        /// true if valid, false if not.
        /// </returns>
        public bool IsValueValid(CellPos ptCellPos, int iVal) {
            return(IsValueValid(ptCellPos.Y * m_iLineSize + ptCellPos.X, iVal));
        }
        
        /// <summary>
        /// Play at the specified position
        /// </summary>
        /// <param name="iPos">         Position (0..max)</param>
        /// <param name="iVal">         Value to play (0 to undo move)</param>
        /// <param name="bUser">        true if user playing, false for fix move</param>
        /// <param name="bRecordMove">  true to record the move</param>
        /// <param name="bOnlyIfValid"> true if can play only valid move</param>
        /// <returns>
        /// true if move succeed, false if not
        /// </returns>
        public virtual bool PlayAt(int iPos, int iVal, bool bUser, bool bRecordMove, bool bOnlyIfValid) {
            bool        bRetVal = false;
            int         iSqr;
            int         iLine;
            int         iCol;
            int         iOldVal;

#if DEBUG
            if (iVal < 0 || iVal > m_iLineSize || iPos >= m_iBoardSize) {
                throw new System.ApplicationException("Coding error. Out of bound #2");
            }
#endif
            iSqr     = m_arrPosToSqr[iPos];
            iLine    = iPos / m_iLineSize;
            iCol     = iPos % m_iLineSize;
            iOldVal  = m_arrBoardVal[iPos];
            if (iVal == 0) {    // Empty a cell
                if (iOldVal != 0) {
                    m_SqrValCount.Add(iSqr,   iOldVal, -1);
                    m_LineValCount.Add(iLine, iOldVal, -1);
                    m_ColValCount.Add(iCol,   iOldVal, -1);
                    m_arrBoardVal[iPos]     = 0;
                    m_arrBoardValType[iPos] = ValueTypeE.None;
                    m_iUsedCount--;
                    bRetVal                 = true;
                } else {
                    bRetVal                 = false;
                }
            } else {            // Set a cell value
                if (iOldVal != 0) {
                    m_SqrValCount.Add(iSqr,   iOldVal, -1);
                    m_LineValCount.Add(iLine, iOldVal, -1);
                    m_ColValCount.Add(iCol,   iOldVal, -1);
                }
                if (!bOnlyIfValid       ||                    
                    (m_SqrValCount[iSqr, iVal]   == 0  &&
                     m_LineValCount[iLine, iVal] == 0  &&
                     m_ColValCount[iCol, iVal]   == 0)) {
                     m_arrBoardVal[iPos]        = (short)iVal;
                     m_arrBoardValType[iPos]    = bUser ? ValueTypeE.User : ValueTypeE.Fix;
                    bRetVal                     = true;
                    m_SqrValCount.Add(iSqr,   iVal, 1);
                    m_LineValCount.Add(iLine, iVal, 1);
                    m_ColValCount.Add(iCol,   iVal, 1);
                    if (iOldVal == 0) {
                        m_iUsedCount++;
                    }
                } else {
                    bRetVal = false;
                    if (iOldVal != 0) {
                        m_SqrValCount.Add(iSqr,   iOldVal, 1);
                        m_LineValCount.Add(iLine, iOldVal, 1);
                        m_ColValCount.Add(iCol,   iOldVal, 1);
                    }
                }
            }
            if (bRetVal && bRecordMove) {
                m_stackUndoMoves.Push(new Move(iPos, iOldVal));
                m_stackRedoMoves.Clear();
            }            
            return(bRetVal);
        }

        /// <summary>
        /// Playing at the specified position
        /// </summary>
        /// <param name="iPos">         Position to play at</param>
        /// <param name="iVal">         Value to play</param>
        /// <param name="bOnlyIfValid"> true if only valid move can be accepted</param>
        /// <returns>
        /// true if move made
        /// </returns>
        protected bool PlayAt(int iPos, int iVal, bool bOnlyIfValid) {
            bool    bRetVal;

            bRetVal = PlayAt(iPos,
                             iVal,
                             true,    // User move
                             true,    // Record undo
                             bOnlyIfValid);
            return(bRetVal);
        }

        
        /// <summary>
        /// Play at the specified position
        /// </summary>
        /// <param name="ptCellPos">    Cell position</param>
        /// <param name="iVal">         Value to play (0 to undo move)</param>
        /// <param name="bUser">        true if user playing, false for fix move</param>
        /// <param name="bRecordMove">  true to record the move</param>
        /// <param name="bOnlyIfValid"> true if can play only valid move</param>
        /// <returns>
        /// true if move succeed, false if not
        /// </returns>
        public bool PlayAt(CellPos ptCellPos, int iVal, bool bUser, bool bRecordMove, bool bOnlyIfValid) {
            return(PlayAt(ptCellPos.Y * m_iLineSize + ptCellPos.X, iVal, bUser, bRecordMove, bOnlyIfValid));
        }


        /// <summary>
        /// Make a move
        /// </summary>
        /// <param name="ptCellPos">    Cell position</param>
        /// <param name="iVal">         Value of the move (1..max)</param>
        /// <param name="bOnlyIfValid"> true if only valid move can be accepted</param>
        /// <returns>
        /// true if move made
        /// </returns>
        public bool PlayAt(CellPos ptCellPos, int iVal, bool bOnlyIfValid) {
            return(PlayAt(ptCellPos.Y * m_iLineSize + ptCellPos.X, iVal, bOnlyIfValid));
        }

        /// <summary>
        /// Get the next playable cell value
        /// </summary>
        /// <param name="ptCellPos">    Cell position</param>
        /// <param name="bOnlyIfValid"> true to get next valid value, false to get the next value</param>
        /// <returns></returns>
        public int GetNextCellVal(CellPos ptCellPos, bool bOnlyIfValid) {
            int         iRetVal;
            int         iOldVal;
            ValueTypeE  eOldType;
            
            iOldVal = this[ptCellPos];
            if (bOnlyIfValid) {
                eOldType    = ValueType(ptCellPos);
                if (iOldVal != 0) {
                    PlayAt(ptCellPos,
                           0,
                           false,   // Fix
                           false,   // No undo recording
                           false);  // No validation
                }
                iRetVal = iOldVal + 1;
                while (iRetVal <= m_iLineSize && bOnlyIfValid && !IsValueValid(ptCellPos, iRetVal)) {
                    iRetVal++;
                }
                if (iOldVal != 0) {
                    PlayAt(ptCellPos,
                           iOldVal,
                           (eOldType == ValueTypeE.User),
                           false,   // No undo recording
                           false);  // No validation
                }
            } else {
                iRetVal = iOldVal + 1;
            }
            if (iRetVal > m_iLineSize) {
                iRetVal = 0;
            }
            return(iRetVal);
        }
        
        /// <summary>
        /// Play the next cell value
        /// </summary>
        /// <param name="ptCellPos">    Cell position</param>
        /// <param name="bUser">        true if user move</param>
        /// <param name="bRecordMove">  true to record the move</param>
        /// <param name="bOnlyIfValid"> true to only play valid move</param>
        /// <returns>
        /// true if succeed, false if not
        /// </returns>
        public bool PlayNextCellVal(CellPos ptCellPos, bool bUser, bool bRecordMove, bool bOnlyIfValid) {
            bool    bRetVal;
            int     iOldVal;
            int     iNewVal;
            int     iPos;
            int     iDummy;
            
            if (ValueType(ptCellPos) == ValueTypeE.Fix) {
                bRetVal = false;
            } else {
                iOldVal = this[ptCellPos];
                iNewVal = GetNextCellVal(ptCellPos, bOnlyIfValid);
                if (iOldVal == iNewVal) {
                    bRetVal = false;
                } else {
                    iPos = ptCellPos.Y * m_iLineSize + ptCellPos.X;
                    if (m_stackUndoMoves.Count != 0 && m_stackUndoMoves.Peek().m_nPos == (short)iPos) {
                        UndoMove(out iDummy, false);
                    }
                    bRetVal = (iNewVal == this[ptCellPos]) ? true : PlayAt(ptCellPos, iNewVal, bUser, bRecordMove, bOnlyIfValid);
                }
            }
            return(bRetVal);
        }
        
        /// <summary>
        /// Play the next cell value
        /// </summary>
        /// <param name="ptCellPos">    Cell position</param>
        /// <param name="bOnlyIfValid"> true to only play valid move</param>
        /// <returns>
        /// true if succeed, false if not
        /// </returns>
        public bool PlayNextCellVal(CellPos ptCellPos, bool bOnlyIfValid) {
            return(PlayNextCellVal(ptCellPos, true, true, bOnlyIfValid));
        }

        /// <summary>
        /// Undo the last move
        /// </summary>
        /// <param name="iPos">         Returned the undone move position</param>
        /// <param name="bRedoable">    true to put the undone move in the redoable moves</param>
        /// <returns>
        /// true if succeed, false if not
        /// </returns>
        protected bool UndoMove(out int iPos, bool bRedoable) {
            bool        bRetVal;
            Move        moveToUndo;
            Move        moveToRedo;

            if (m_stackUndoMoves.Count == 0) {
                iPos    = -1;
                bRetVal = false;
            } else {
                moveToUndo  = m_stackUndoMoves.Pop();
                iPos        = moveToUndo.m_nPos;
                if (bRedoable) {
                    moveToRedo              = moveToUndo;
                    moveToRedo.m_nOldVal    = (short)this[iPos];
                    m_stackRedoMoves.Push(moveToRedo);
                }
                bRetVal = PlayAt(moveToUndo.m_nPos,
                                 moveToUndo.m_nOldVal,
                                 true,      // User
                                 false,     // Don't record the move
                                 false);    // No validation
            }
            return(bRetVal);
        }

        /// <summary>
        /// Undo the last move
        /// </summary>
        /// <param name="bRedoable">    true if the undone move can be redone</param>
        /// <returns>
        /// true if succeed, false if not
        /// </returns>
        protected bool UndoMove(bool bRedoable) {
            int iDummy;
            
            return(UndoMove(out iDummy, bRedoable));
        }
        
        /// <summary>
        /// Undo a move
        /// </summary>
        /// <param name="ptCellPos">    Cell position</param>
        /// <param name="bRedoable">    true if the undone move can be redone</param>
        /// <returns>
        /// true if succeed, false if failed
        /// </returns>
        public bool UndoMove(out CellPos ptCellPos, bool bRedoable) {
            bool    bRetVal;            
            int     iPos;
            
            bRetVal     = UndoMove(out iPos, bRedoable);
            ptCellPos   = new CellPos(iPos % m_iLineSize, iPos / m_iLineSize);
            if (!bRedoable) {
                m_stackRedoMoves.Clear();
            }
            return(bRetVal);
        }

        /// <summary>
        /// Undo all user moves
        /// </summary>
        /// <param name="bRedoable">    true if the undone move can be redone</param>
        public void UndoAllMove(bool bRedoable) {
            int     iPos;

            while (UndoMove(out iPos, bRedoable));
            if (!bRedoable) {
                m_stackRedoMoves.Clear();
            }
        }
        
        /// <summary>
        /// Redo a move
        /// </summary>
        /// <param name="ptCellPos">    Cell position</param>
        /// <returns>
        /// true if succeed, false if failed
        /// </returns>
        public bool RedoMove(out CellPos ptCellPos) {
            bool    bRetVal;            
            int     iPos;
            int     iOldVal;
            int     iVal;
            Move    move;
            
            ptCellPos = new CellPos();
            if (m_stackRedoMoves.Count == 0) {
                bRetVal = false;
            } else {
                move        = m_stackRedoMoves.Pop();
                iPos        = move.m_nPos;
                iVal        = move.m_nOldVal;
                ptCellPos.X = iPos % m_iLineSize;
                ptCellPos.Y = iPos / m_iLineSize;
                iOldVal     = m_arrBoardVal[iPos];
                bRetVal     = PlayAt(iPos,
                                     iVal,
                                     true,      // User move
                                    false,     // Don't record the move
                                    false);    // No validation
                if (bRetVal) {
                    m_stackUndoMoves.Push(new Move(iPos, iOldVal));
                }
            }
            return(bRetVal);
        }

        /// <summary>
        /// Number of moves which can be undone
        /// </summary>
        public int UndoMoveCount {
            get {
                return(m_stackUndoMoves.Count);
            }
        }
        
        /// <summary>
        /// Number of moves which can be redone
        /// </summary>
        public int RedoMoveCount {
            get {
                return(m_stackRedoMoves.Count);
            }
        }

        /// <summary>
        /// Number of user moves
        /// </summary>
        public int MoveCount {
            get {
                return(m_stackUndoMoves.Count);
            }
        }
        #endregion

        #region Solving
        /// <summary>
        /// Find the list of valid values for each empty cell. Each cell values are expressed using a bit mask
        /// </summary>
        /// <returns>
        /// Number of empty cells or -1 if at least one cell cannot accept any valid values
        /// </returns>
        private int FillListOfValidValuesForEmptyCells() {
            int     iRetVal;
            int     iValMask;
            int     iPos;
            
            iPos        = 0;
            iRetVal     = 0;
            while (iPos < m_iBoardSize && iRetVal != -1) {
                if (m_arrBoardVal[iPos] == 0) {
                    iValMask    = 0;
                    for (int iVal = 1; iVal <= m_iLineSize; iVal++) {
                        if (IsValueValid(iPos, iVal)) {
                            iValMask |= 1 << (iVal - 1);
                        }
                    }
                    if (iValMask == 0) {
                        iRetVal                     = -1;
                    } else {
                        m_arrValMask[iRetVal]       = iValMask;
                        m_arrPosToCheck[iRetVal++]  = iPos;
                    }
                }
                iPos++;
            }
            return(iRetVal);
        }

        /// <summary>
        /// Play values which can only be in single cell per zone (line/column or square)
        /// </summary>
        /// <returns>
        /// true if ok, false if a value cannot be played anywhere in a line/column or square
        /// </returns>
        private bool FillValueWhichCanBePlayedInSingleCellOfAZone() {
            bool    bRetVal = true;
            bool    bContinue;
            int     iPos;
            int     iPlayingPos;
            int     iValCount;
            int     iZoneIndex;
            int     iLineIndex;
            int     iColIndex;
            int     iVal;
            bool    bLine;
            bool    bColumn;
            bool    bSquare;
            
            do {
                bContinue = false;
                iZoneIndex = 0;
                while (iZoneIndex < m_iLineSize && bRetVal) {
                    bColumn     = (m_ColValCount.FreeSlot(iZoneIndex)  != 0);
                    bLine       = (m_LineValCount.FreeSlot(iZoneIndex) != 0);
                    bSquare     = (m_SqrValCount.FreeSlot(iZoneIndex)  != 0);
                    if (bColumn || bLine || bSquare) {
                        iVal        = 1;
                        while (iVal <= m_iLineSize && bRetVal) {
                            if (bLine && bRetVal && m_LineValCount[iZoneIndex, iVal] == 0) {
                                iPos        = iZoneIndex * m_iLineSize;
                                iPlayingPos = -1;
                                iValCount   = 0;
                                for (int iIndex = 0; iIndex < m_iLineSize; iIndex++) {
                                    if (m_arrBoardVal[iPos] == 0 && IsValueValid(iPos, iVal)) {
                                        iValCount++;
                                        iPlayingPos = iPos;
                                    }
                                    iPos++;
                                }
                                switch(iValCount) {
                                case 0:
                                    bRetVal = false;
                                    break;
                                case 1:
                                    bContinue = PlayAt(iPlayingPos, iVal, false);
                                    break;
                                default:
                                    break;
                                }
                            }
                            if (bColumn && bRetVal && m_ColValCount[iZoneIndex, iVal] == 0) {
                                iPos        = iZoneIndex;
                                iPlayingPos = -1;
                                iValCount   = 0;
                                for (int iIndex = 0; iIndex < m_iLineSize; iIndex++) {
                                    if (m_arrBoardVal[iPos] == 0 && IsValueValid(iPos, iVal)) {
                                        iValCount++;
                                        iPlayingPos = iPos;
                                    }
                                    iPos += m_iLineSize;
                                }
                                switch(iValCount) {
                                case 0:
                                    bRetVal = false;
                                    break;
                                case 1:
                                    bContinue = PlayAt(iPlayingPos, iVal, false);
                                    break;
                                default:
                                    break;
                                }
                            }
                            if (bSquare && bRetVal && m_SqrValCount[iZoneIndex, iVal] == 0) {
                                iPos        = m_arrSqrToPos[iZoneIndex];
                                iPlayingPos = -1;
                                iValCount   = 0;
                                for (iLineIndex = 0; iLineIndex < m_iSquareSize; iLineIndex++) {
                                    for (iColIndex = 0; iColIndex < m_iSquareSize; iColIndex++) {
                                        if (m_arrBoardVal[iPos] == 0 && IsValueValid(iPos, iVal)) {
                                            iValCount++;
                                            iPlayingPos = iPos;
                                        }
                                        iPos++;
                                    }
                                    iPos += m_iLineSize - m_iSquareSize;
                                }
                                switch(iValCount) {
                                case 0:
                                    bRetVal = false;
                                    break;
                                case 1:
                                    bContinue = PlayAt(iPlayingPos, iVal, false);
                                    break;
                                default:
                                    break;
                                }
                            }
                            iVal++;
                        }
                    }
                    iZoneIndex++;
                }
            } while (bRetVal && bContinue);
            return(bRetVal);
        }

        /// <summary>
        /// Fill the obvious cells.
        /// </summary>
        /// <param name="iPosCount">    Number if position/value mask in array.</param>
        /// <param name="iBestIndex">   Index in the array of the best position with the least number of possible value</param>
        /// <param name="bMediumLevel"> true if at least one value has been found using a slot with only one possible playable value</param>
        /// <returns>
        /// true if ok, false if board unsolvable
        /// </returns>
        /// <remarks>
        /// Obvious position are:
        ///     Values which can only be played in one cell of a line, row or square
        ///     Cells in which only one value can be played
        /// </remarks>
        private bool FillObviousCells(int iPosCount, out int iBestIndex, out bool bMediumLevel) {
            bool    bRetVal = true;
            bool    bContinue;
            int     iPos;
            int     iValMask;
            int     iValBit;
            int     iValCount;
            int     iVal;
            int     iBestCount;
            int     iIndex;
            
            bMediumLevel    = false;
            do {
                iBestIndex  = -1;
                iBestCount  = 999;
                bContinue   = false;
                if (FreeCell != 0) {
                    bRetVal = FillValueWhichCanBePlayedInSingleCellOfAZone();
                }
                if (bRetVal && FreeCell != 0) {
                    iIndex = 0;
                    while (iIndex < iPosCount && bRetVal) {
                        iPos        = m_arrPosToCheck[iIndex];
                        if (m_arrBoardVal[iPos] == 0) {
                            iValMask    = m_arrValMask[iIndex];
                            iValBit     = 1;
                            iValCount   = 0;
                            iVal        = 0;
                            for (int iValIndex = 1; iValIndex <= m_iLineSize; iValIndex++) {
                                if ((iValMask & iValBit) != 0   &&
                                    m_arrBoardVal[iPos]  == 0   &&
                                    IsValueValid(iPos, iValIndex)) {
                                    iVal    = iValIndex;
                                    iValCount++;
                                }
                                iValBit <<= 1;
                            }
                            switch(iValCount) {
                            case 0:
                                bRetVal         = false;
                                break;
                            case 1:
                                bContinue       = PlayAt(iPos, iVal, false);
                                bMediumLevel    = true;
                                break;
                            default:
                                if (iValCount < iBestCount) {
                                    iBestCount  = iValCount;
                                    iBestIndex  = iIndex;
                                }
                                break;
                            }
                        }
                        iIndex++;
                    }
                }
            } while (bRetVal && bContinue);
            return(bRetVal);
        }

        /// <summary>
        /// Solve the board using backtracking if obvious strategy doesn't work
        /// </summary>
        /// <param name="iPosCount">            Number if position/value mask in array</param>
        /// <param name="bStopAtFirstSolution"> Stop after find a solution if true, continue to check if more than one solution exist if false</param>
        /// <param name="eLevel">               Level of difficulty of the board</param>
        /// <returns>
        /// Number of solutions found
        /// </returns>
        private int SolveBacktrack(int iPosCount, bool bStopAtFirstSolution, out LevelE eLevel) {
            int     iRetVal = 0;
            int     iBestIndex;
            int     iPos;
            int     iValMask;
            int     iValBit;
            int     iVal;
            int     iSolutionCount;
            int     iStackPos;
            LevelE  eDummy;
            bool    bMediumLevel;
            
            if (FillObviousCells(iPosCount, out iBestIndex, out bMediumLevel)) {
                if (iBestIndex == -1) {
                    eLevel          = (bMediumLevel) ? LevelE.Medium : LevelE.Easy;
                    iRetVal++;
                } else {
                    eLevel          = LevelE.Difficult;
                    iPos            = m_arrPosToCheck[iBestIndex];
                    iValMask        = m_arrValMask[iBestIndex];
                    iValBit         = 1;
                    iVal            = 1;
                    iSolutionCount  = (bStopAtFirstSolution) ? 1 : 2;
                    while (iVal <= m_iLineSize && iRetVal < iSolutionCount) {
                        if ((iValMask & iValBit) != 0) {
                            iStackPos = m_stackUndoMoves.Count;
                            if (PlayAt(iPos, iVal, true)) {
                                iRetVal += SolveBacktrack(iPosCount, bStopAtFirstSolution, out eDummy);
                                if (iRetVal == 0 || !bStopAtFirstSolution) {
                                    while (m_stackUndoMoves.Count != iStackPos && UndoMove(false));
                                }
                            }
                        }
                        iValBit <<= 1;
                        iVal++;
                    }
                }
            } else {
                eLevel = LevelE.Easy;
            }
            return(iRetVal);
        }


        /// <summary>
        /// Solve the board
        /// </summary>
        /// <param name="bStopAtFirstSolution"> true to stop at the first solution found</param>
        /// <param name="eLevel">               Difficulty level of the board</param>
        /// <returns>
        /// Number of solution founds (0=None,1=One,2=More than one)
        /// </returns>
        /// <remarks>
        /// Level:
        ///     Easy:       No value has been found using a slot with only one possible playable value and no backtracking
        ///     Medium:     At least one value has been found using a slot with only one possible playable value, no backtracking
        ///     Difficult:  Backtracking (computer guess a value)
        /// </remarks>
        public int Solve(bool bStopAtFirstSolution, out LevelE eLevel) {
            int     iRetVal;
            int     iPosCount;
            
            eLevel      = LevelE.Easy;
            UndoAllMove(false);  // Discard  user move which can be wrong
            iPosCount = FillListOfValidValuesForEmptyCells();
            if (iPosCount > -1) {
                iRetVal = SolveBacktrack(iPosCount, bStopAtFirstSolution, out eLevel);
                if (iRetVal != 1 || !bStopAtFirstSolution) {
                    UndoAllMove(false);
                }
            } else {
                iRetVal = 0;
            }
            return(iRetVal);
        }

        /// <summary>
        /// Solve the board
        /// </summary>
        /// <param name="bStopAtFirstSolution"> true to stop at the first solution found</param>
        /// <returns>
        /// Number of solution founds (0=None,1=One,2=More than one)
        /// </returns>
        public int Solve(bool bStopAtFirstSolution) {
            LevelE  eLevel;
            
            return(Solve(bStopAtFirstSolution, out eLevel));
        }
        #endregion

        #region Serialization
        /// <summary>
        /// Load the undo stack from a serialized buffer
        /// </summary>
        /// <param name="arrBinary">    Serialized stack</param>
        public void LoadUndoStackFromBinary(byte[] arrBinary) {
            Move        move;
            Stack<Move> stackMoves;
            int         iIndex;
            bool        bError;
            
            if (arrBinary.Length == m_stackUndoMoves.Count * 2) {
                move        = new Move();
                stackMoves  = new Stack<Move>(m_stackUndoMoves.Count);
                bError      = false;
                iIndex      = 0;
                while (iIndex < arrBinary.Length && !bError) {
                    move.m_nPos     = (short)arrBinary[iIndex++];
                    move.m_nOldVal  = (short)arrBinary[iIndex++];
                    if (move.m_nPos     < 0 || move.m_nPos      >= m_iBoardSize ||
                        move.m_nOldVal  < 0 || move.m_nOldVal   >  m_iLineSize) {
                        bError = true;
                    } else {
                        stackMoves.Push(move);
                    }
                }
                if (!bError) {
                    m_stackUndoMoves = stackMoves;
                }
            }
        }
        
        /// <summary>
        /// Save the content of the undo stack into a binary buffer
        /// </summary>
        /// <returns>
        /// Serialized stack
        /// </returns>
        public byte[] SaveUndoStackToBinary() {
            byte[]  arrRetVal;
            Move[]  arrMove;
            int     iSize;
            int     iPos;
            
            iSize       = m_stackUndoMoves.Count;
            arrRetVal   = new byte[iSize * 2];
            if (iSize != 0) {
                arrMove = m_stackUndoMoves.ToArray();
                iPos    = 0;
                for (int iIndex = 0; iIndex < iSize; iIndex++) {
                    arrRetVal[iPos++] = (byte)arrMove[iIndex].m_nPos;
                    arrRetVal[iPos++] = (byte)arrMove[iIndex].m_nOldVal;
                }
            }
            return(arrRetVal);
        }
        #endregion

    } // Class
} // Namespace
