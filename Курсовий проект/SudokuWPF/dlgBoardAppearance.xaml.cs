﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SudokuWPF {
    /// <summary>
    /// Handle the board appearance
    /// </summary>
    public partial class dlgBoardAppearance : Window {
        
        #region Property Object

        /// <summary>
        /// Class uses to read/write the property
        /// </summary>
        private class PropObj {
            /// <summary>Font family used for fix value</summary>
            private System.Drawing.Font     m_fontFix;
            /// <summary>Color used to fix value</summary>
            private System.Drawing.Color    m_colorFix;
            /// <summary>Font family used for for user value</summary>
            private System.Drawing.Font     m_fontUser;
            /// <summary>Color used for user value</summary>
            private System.Drawing.Color    m_colorUser;
            /// <summary>Font family used for hint value</summary>
            private System.Drawing.Font     m_fontHint;
            /// <summary>Color used for hint value</summary>
            private System.Drawing.Color    m_colorHint;
            /// <summary>Color used to draw the thick lines of the board frame</summary>
            private System.Drawing.Color    m_colorThickLine;
            /// <summary>Color used to draw the thin lines of the board frame</summary>
            private System.Drawing.Color    m_colorThinLine;
            /// <summary>Board Background color</summary>
            private System.Drawing.Color    m_colorBack;

            /// <summary>
            /// Font used to draw fix value. Font will be forced to bold
            /// </summary>
            [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
            [Browsable(true)]
            [Bindable(true)]
            [DefaultValue(false)]
            [Category("Appearance")]
            [Description("Font family used to draw fix value. Font will be forced to bold")]
            public  System.Drawing.Font     FixFont { get {return(m_fontFix); } set { m_fontFix = value; } }

            /// <summary>
            /// Color used to draw fix value
            /// </summary>
            [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
            [Browsable(true)]
            [Bindable(true)]
            [DefaultValue(false)]
            [Category("Appearance")]
            [Description("Color used to draw fix value")]
            public  System.Drawing.Color    FixColor { get {return(m_colorFix); } set { m_colorFix = value; } }

            /// <summary>
            /// Font family used to draw user value. Font will be forced to regular
            /// </summary>
            [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
            [Browsable(true)]
            [Bindable(true)]
            [Category("Appearance")]
            [Description("Font used to draw user value. Font will be forced to bold")]
            public  System.Drawing.Font     UserFont { get {return(m_fontUser); } set { m_fontUser = value; } }

            /// <summary>
            /// Color used to draw user value
            /// </summary>
            [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
            [Browsable(true)]
            [Bindable(true)]
            [Category("Appearance")]
            [Description("Color used to draw user value")]
            public  System.Drawing.Color    UserColor { get {return(m_colorUser); } set { m_colorUser = value; } }

            /// <summary>
            /// Font used to draw hint value
            /// </summary>
            [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
            [Browsable(true)]
            [Bindable(true)]
            [DefaultValue(false)]
            [Category("Appearance")]
            [Description("Font family used to draw hint value")]
            public  System.Drawing.Font     HintFont { get {return(m_fontHint); } set { m_fontHint = value; } }

            /// <summary>
            /// Color used to draw hint value.
            /// </summary>
            [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
            [Browsable(true)]
            [Bindable(true)]
            [DefaultValue(false)]
            [Category("Appearance")]
            [Description("Color used to draw hint value")]
            public  System.Drawing.Color    HintColor { get {return(m_colorHint); } set { m_colorHint = value; } }

            /// <summary>
            /// Color used to draw the thick lines of the board frame
            /// </summary>
            [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
            [Browsable(true)]
            [Bindable(true)]
            [Category("Appearance")]
            [Description("Color used to draw the thick lines of the board frame")]
            public  System.Drawing.Color    ThickLineColor { get {return(m_colorThickLine);} set {m_colorThickLine = value; } }

            /// <summary>
            /// Color used to draw the thin lines of the board frame
            /// </summary>
            [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
            [Browsable(true)]
            [Bindable(true)]
            [Category("Appearance")]
            [Description("Color used to draw the thin lines of the board frame")]
            public  System.Drawing.Color    ThinLineColor { get {return(m_colorThinLine);} set {m_colorThinLine = value; } }

            /// <summary>
            /// Background color
            /// </summary>
            [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
            [Browsable(true)]
            [Bindable(true)]
            [Category("Appearance")]
            [Description("Board Background color")]
            public  System.Drawing.Color    BackColor { get {return(m_colorBack);} set {m_colorBack = value; } }
        }

        #endregion

        /// <summary>
        /// Subclass the PropertyGrid class to disable the event button
        /// </summary>
        private class OurPropertyGrid : System.Windows.Forms.PropertyGrid {
            public OurPropertyGrid() : base() {
                ShowEventsButton(false);
            }
        }
        #region Fields

        /// <summary>Property grid</summary>
        private OurPropertyGrid     m_propertyGrid1;
        /// <summary>Object being edited by the property grid</summary>
        private PropObj             m_propObj;
        /// <summary>Sudoku control uses to show the current setting</summary>
        private SudControl          m_sudControl;
        /// <summary>Father sudoku control. The properties change will be reflected to this control if accepted.</summary>
        private SudControl          m_sudControlFather;
        /// <summary>True if some changes need to be commit</summary>
        private bool                m_bIsDirty;

        #endregion

        #region Ctors

        /// <summary>
        /// Class ctor
        /// </summary>
        public dlgBoardAppearance() {
            System.Windows.Forms.Integration.WindowsFormsHost   host;

            InitializeComponent();
            m_propObj                   = new PropObj();
            m_propertyGrid1             = new OurPropertyGrid();
            m_propertyGrid1.Location    = new System.Drawing.Point(0,0);
            m_propertyGrid1.Name        = "propertyGrid1";
            m_propertyGrid1.Size        = new System.Drawing.Size(100, 100);
            m_propertyGrid1.TabIndex    = 1;
            m_propertyGrid1.Dock        = System.Windows.Forms.DockStyle.Fill;
            host                        = new System.Windows.Forms.Integration.WindowsFormsHost();
            host.Child                  = m_propertyGrid1;
            borderPropertyGrid.Child    = host;
            m_sudControl                = new SudControl();
            m_sudControl.Name           = "SudControl1";
            m_sudControl.Width          = Double.NaN;
            m_sudControl.Height         = Double.NaN;
            m_sudControl.FontFamily     = FontFamily;
            m_sudControl.FontSize       = FontSize;
            m_sudControl.Background     = Background;
            m_sudControl.BoardBackground= Background;
            m_sudControl.Board          = null;
            borderSudBoard.Child        = m_sudControl;
            m_propObj                   = new PropObj();
            m_bIsDirty                  = false;
            Loaded                     += new RoutedEventHandler(dlgBoardAppearance_Loaded);
            RefreshState();
        }

        /// <summary>
        /// Class ctor
        /// </summary>
        /// <param name="sudControlFather"> Father control</param>
        public dlgBoardAppearance(SudControl sudControlFather) : this() {
            m_sudControlFather                      = sudControlFather;
            m_propObj.FixFont                       = FormFontFromFontFamily(sudControlFather.FixValueFontFamily, 12, System.Drawing.FontStyle.Bold);
            m_propObj.FixColor                      = FormColorFromBrush(sudControlFather.FixValueBrush);
            m_propObj.UserFont                      = FormFontFromFontFamily(sudControlFather.UserValueFontFamily, 12, System.Drawing.FontStyle.Regular);
            m_propObj.UserColor                     = FormColorFromBrush(sudControlFather.UserValueBrush);
            m_propObj.HintFont                      = FormFontFromFontFamily(sudControlFather.HintValueFontFamily, 3, System.Drawing.FontStyle.Regular);
            m_propObj.HintColor                     = FormColorFromBrush(sudControlFather.HintValueBrush);
            m_propObj.ThickLineColor                = FormColorFromColor(sudControlFather.ThickLineColor);
            m_propObj.ThinLineColor                 = FormColorFromColor(sudControlFather.ThinLineColor);
            m_propObj.BackColor                     = FormColorFromBrush(sudControlFather.BoardBackground);
            m_sudControl.Board                      = sudControlFather.Board.Clone();
            m_sudControl.SymbolType                 = sudControlFather.SymbolType;
            m_sudControl.OnlyValidMove              = sudControlFather.OnlyValidMove;
            m_sudControl.InputMethod                = sudControlFather.InputMethod;
            m_sudControl.ShowHint                   = true;
            m_sudControl.ShowValuePickerOnMouseMove = sudControlFather.ShowValuePickerOnMouseMove;
            SetPreviewControlFromPropObj();
        }
        #endregion

        /// <summary>
        /// Gets a System.Drawing.Font from a WPF font family
        /// </summary>
        /// <param name="fontFamily">   WPF Font family</param>
        /// <param name="iSize">        Font size</param>
        /// <param name="fontStyle">    Font style</param>
        /// <returns>
        /// System.Drawing.Font
        /// </returns>
        private System.Drawing.Font FormFontFromFontFamily(FontFamily fontFamily, int iSize, System.Drawing.FontStyle fontStyle) {
            return(new System.Drawing.Font(new System.Drawing.FontFamily(fontFamily.Source), iSize, fontStyle));
        }

        /// <summary>
        /// Gets a System.Drawing.Color from a WPF color
        /// </summary>
        /// <param name="color">    WPF color</param>
        /// <returns>
        /// System.Drawing.Color
        /// </returns>
        private System.Drawing.Color FormColorFromColor(Color color) {
            return(System.Drawing.Color.FromArgb(color.R, color.G, color.B));
        }

        /// <summary>
        /// Gets a System.Drawing.Color from a WPF brush
        /// </summary>
        /// <param name="brush">    WPF brush (SolidColorBrush)</param>
        /// <returns></returns>
        private System.Drawing.Color FormColorFromBrush(Brush brush) {
            Color   color;

            color = ((SolidColorBrush)brush).Color;
            return(System.Drawing.Color.FromArgb(color.R, color.G, color.B));
        }

        /// <summary>
        /// Gets a WPF font family from a System.Drawing.Font
        /// </summary>
        /// <param name="font"> System.Drawing.Font</param>
        /// <returns>
        /// WPF font family
        /// </returns>
        private FontFamily FontFamilyFromFormFont(System.Drawing.Font font) {
            return(new FontFamily(font.FontFamily.Name));
        }

        /// <summary>
        /// Gets a WPF brush from a System.Drawing.Color
        /// </summary>
        /// <param name="color">    System.Drawing.Color</param>
        /// <returns>
        /// WPF brush
        /// </returns>
        private Brush BrushFromFormColor(System.Drawing.Color color) {
            return(new SolidColorBrush(Color.FromRgb(color.R, color.G, color.B)));
        }

        /// <summary>
        /// Gets a WPF color from a System.Drawing.Color
        /// </summary>
        /// <param name="color">    System.Drawing.Color</param>
        /// <returns>
        ///  WPF color
        /// </returns>
        private Color ColorFromFormColor(System.Drawing.Color color) {
            return(Color.FromRgb(color.R, color.G, color.B));
        }

        /// <summary>
        /// Set the visualizer control to reflect the current state of prop object
        /// </summary>
        private void SetPreviewControlFromPropObj() {
            m_sudControl.FixValueFontFamily     = FontFamilyFromFormFont(m_propObj.FixFont);
            m_sudControl.FixValueBrush          = BrushFromFormColor(m_propObj.FixColor);
            m_sudControl.UserValueFontFamily    = FontFamilyFromFormFont(m_propObj.UserFont);
            m_sudControl.UserValueBrush         = BrushFromFormColor(m_propObj.UserColor);
            m_sudControl.HintValueFontFamily    = FontFamilyFromFormFont(m_propObj.HintFont);
            m_sudControl.HintValueBrush         = BrushFromFormColor(m_propObj.HintColor);
            m_sudControl.ThickLineColor         = ColorFromFormColor(m_propObj.ThickLineColor);
            m_sudControl.ThinLineColor          = ColorFromFormColor(m_propObj.ThinLineColor);
            m_sudControl.BoardBackground        = BrushFromFormColor(m_propObj.BackColor);
        }

        /// <summary>
        /// Refresh the button state
        /// </summary>
        private void RefreshState() {
            butApply.IsEnabled    = m_bIsDirty;
        }

        /// <summary>
        /// Called when the form is loaded
        /// </summary>
        /// <param name="sender">   Sender object</param>
        /// <param name="e">        Event argument</param>
        void dlgBoardAppearance_Loaded(object sender, RoutedEventArgs e) {
            m_propertyGrid1.SelectedObject          = m_propObj;
            m_propertyGrid1.PropertyValueChanged   += new System.Windows.Forms.PropertyValueChangedEventHandler(propertyGrid1_PropertyValueChanged);
        }

        /// <summary>
        /// Called when a property value is changed
        /// </summary>
        /// <param name="sender">   Sender object</param>
        /// <param name="e">        Event argument</param>
        private void propertyGrid1_PropertyValueChanged(object sender, System.Windows.Forms.PropertyValueChangedEventArgs e) {
            switch(e.ChangedItem.Label) {
            case "FixFont":
            case "FixColor":
            case "UserFont":
            case "UserColor":
            case "HintFont":
            case "HintColor":
            case "ThickLineColor":
            case "ThinLineColor":
            case "BackColor":
                SetPreviewControlFromPropObj();
                m_bIsDirty = true;
                break;
            default:
                break;
            }
            RefreshState();
        }

        /// <summary>
        /// Reset the value to te original one
        /// </summary>
        private void Reset() {
            FontFamily  fontFamily;

            fontFamily                          = SudControl.CreateFontFamily("Microsoft Sans Serif");
            m_propertyGrid1.SelectedObject      = null;
            m_propObj.FixFont                   = FormFontFromFontFamily(fontFamily, 12, System.Drawing.FontStyle.Bold);
            m_propObj.FixColor                  = System.Drawing.Color.FromArgb(128, 0, 0);
            m_propObj.UserFont                  = FormFontFromFontFamily(fontFamily, 12, System.Drawing.FontStyle.Regular);
            m_propObj.UserColor                 = System.Drawing.Color.FromArgb(174, 128, 124);
            m_propObj.HintFont                  = FormFontFromFontFamily(fontFamily, 4, System.Drawing.FontStyle.Regular);
            m_propObj.HintColor                 = System.Drawing.Color.FromArgb(192, 192, 255);
            m_propObj.ThickLineColor            = System.Drawing.Color.FromArgb(184, 134, 11);
            m_propObj.ThinLineColor             = System.Drawing.Color.FromArgb(218, 165, 32);
            m_propObj.BackColor                 = System.Drawing.Color.FromArgb(250, 235, 215);
            m_propertyGrid1.SelectedObject      = m_propObj;
            SetPreviewControlFromPropObj();
            m_bIsDirty                          = true;
            RefreshState();
        }

        /// <summary>
        /// Apply the change to the father control
        /// </summary>
        private void ApplyChange() {
            m_sudControlFather.FixValueFontFamily   = m_sudControl.FixValueFontFamily;
            m_sudControlFather.FixValueBrush        = m_sudControl.FixValueBrush;
            m_sudControlFather.UserValueFontFamily  = m_sudControl.UserValueFontFamily;
            m_sudControlFather.UserValueBrush       = m_sudControl.UserValueBrush;
            m_sudControlFather.HintValueFontFamily  = m_sudControl.HintValueFontFamily;
            m_sudControlFather.HintValueBrush       = m_sudControl.HintValueBrush;
            m_sudControlFather.ThickLineColor       = m_sudControl.ThickLineColor;
            m_sudControlFather.ThinLineColor        = m_sudControl.ThinLineColor;
            m_sudControlFather.BoardBackground      = m_sudControl.BoardBackground;
            m_bIsDirty                              = false;
            RefreshState();
        }

        /// <summary>
        /// Called when the Ok button is pressed
        /// </summary>
        /// <param name="sender">   Sender object</param>
        /// <param name="e">        Event argument</param>
        private void butOk_Click(object sender, RoutedEventArgs e) {
            if (m_bIsDirty) {
                ApplyChange();
            }
            Close();
        }

        /// <summary>
        /// Called when the Apply button is pressed
        /// </summary>
        /// <param name="sender">   Sender object</param>
        /// <param name="e">        Event argument</param>
        private void butApply_Click(object sender, RoutedEventArgs e) {
            ApplyChange();
        }

        /// <summary>
        /// Called when the Reset button is pressed
        /// </summary>
        /// <param name="sender">   Sender object</param>
        /// <param name="e">        Event argument</param>
        private void butReset_Click(object sender, RoutedEventArgs e) {
            Reset();
        }

    } // Class
} // Namespace
