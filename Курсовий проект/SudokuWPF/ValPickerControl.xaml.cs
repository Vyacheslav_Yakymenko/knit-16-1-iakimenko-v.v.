﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Sudoku2;

namespace SudokuWPF {
    /// <summary>
    /// Interaction logic for ValPickerControl.xaml
    /// </summary>
    public partial class ValPickerControl : UserControl {
        
        #region Types Definition
        
        /// <summary>Board type (9x9 or 16x16)</summary>
        public enum BoardTypeE {
            /// <summary>9x9 board</summary>
            v9x9 = 0,
            /// <summary>16x16 board</summary>
            v16x16 = 1
        };

        /// <summary>
        /// Event for ValueSelected event
        /// </summary>
        public class ValueSelectedEventArg : System.EventArgs {
            /// <summary>Specified the value which has been picked</summary>
            public int      Value;
            /// <summary>
            /// Class ctor
            /// </summary>
            /// <param name="iVal">     Selected Value</param>
            public ValueSelectedEventArg(int iVal) { Value = iVal; }
        }
        
        #endregion

        #region Cell Implementation
        
        private class Cell : Border {
            /// <summary>Standard border thickness if visible</summary>
            private static Thickness    m_thicknessStd = new Thickness(2,2,2,2);
            /// <summary>Standard border thickness if visible</summary>
            private static Thickness    m_thicknessHidden = new Thickness(0,0,0,0);
            /// <summary>Red solid brush</summary>
            private static Brush        m_brushRed = new SolidColorBrush(Color.FromRgb(255, 0, 0));
            /// <summary>Green solid brush</summary>
            private static Brush        m_brushGreen = new SolidColorBrush(Color.FromRgb(0, 255, 0));
            /// <summary>Blue solid brush</summary>
            private static Brush        m_brushBlue = new SolidColorBrush(Color.FromRgb(0, 0, 255));
            /// <summary>Father value picker control</summary>
            private ValPickerControl    m_valPickerCtl;
            /// <summary>Child TextBlock</summary>
            private TextBlock           m_textBlock;
            /// <summary>Cell position</summary>
            private CellPos             m_tCellPos;
            
            /// <summary>
            /// Class ctor
            /// </summary>
            /// <param name="valPickerCtl"> Father control</param>
            /// <param name="tCellPos">     Cell position</param>
            public Cell(ValPickerControl valPickerCtl, CellPos tCellPos) {
                CornerRadius                    = new CornerRadius(30);
                m_valPickerCtl                  = valPickerCtl;
                m_tCellPos                      = tCellPos;
                m_textBlock                     = new TextBlock();
                m_textBlock.TextAlignment       = TextAlignment.Center;
                m_textBlock.HorizontalAlignment = HorizontalAlignment.Stretch;
                m_textBlock.VerticalAlignment   = VerticalAlignment.Center;
                m_textBlock.MouseUp            += new MouseButtonEventHandler(m_textBlock_MouseUp);
                RefreshState();
                Child                           = m_textBlock;
            }

            /// <summary>
            /// Reset the size of the font
            /// </summary>
            private void ResetFontSize() {
                int     iSqrSize;
                
                iSqrSize    = (m_valPickerCtl.BoardType == BoardTypeE.v9x9) ? 3 : 4;
                if (m_textBlock.FontSize !=  ActualWidth && ActualWidth != 0) {
                    if (iSqrSize == 4 && m_valPickerCtl.SymbolType == SudControl.SymbolTypeE.Number) {
                        m_textBlock.FontSize = ActualWidth / 1.2;
                    } else {
                        m_textBlock.FontSize = ActualWidth;
                    }
                }
            }
            
            /// <summary>
            /// Refresh the state of the cell
            /// </summary>
            public void RefreshState() {
                int                     iPos;
                int                     iSqrSize;
                int                     iRedMask;
                int                     iGreenMask;
                int                     iBlueMask;
                int                     iBitMask;
                SudControl.SymbolTypeE  eSymbolType;
                Brush                   brush;
                bool                    bValid;
                
                iSqrSize                = (m_valPickerCtl.BoardType == BoardTypeE.v9x9) ? 3 : 4;
                eSymbolType             = m_valPickerCtl.SymbolType;
                iPos                    = (m_tCellPos.Y * iSqrSize + m_tCellPos.X);
                iBitMask                = 1 << iPos;
                bValid                  = ((m_valPickerCtl.m_iValMask & iBitMask) != 0);
                iRedMask                = m_valPickerCtl.RedMarkerMask;
                iGreenMask              = m_valPickerCtl.GreenMarkerMask;
                iBlueMask               = m_valPickerCtl.BlueMarkerMask;
                m_textBlock.FontWeight  = bValid ? FontWeights.Bold : FontWeights.Normal;
                brush                   = bValid ? m_valPickerCtl.ValidValueBrush : m_valPickerCtl.UnvalidValueBrush;
                if (m_textBlock.Foreground != brush) {
                    m_textBlock.Foreground = brush;
                }
                if ((iRedMask & iBitMask) != 0) {
                    BorderThickness = m_thicknessStd;
                    BorderBrush     = m_brushRed;
                } else if ((iGreenMask & iBitMask) != 0) {
                    BorderThickness = m_thicknessStd;
                    BorderBrush     = m_brushGreen;
                } else if ((iBlueMask & iBitMask) != 0) {
                    BorderThickness = m_thicknessStd;
                    BorderBrush     = m_brushBlue;
                } else {
                    BorderThickness = m_thicknessHidden;
                }
                if (eSymbolType == SudControl.SymbolTypeE.Symbol) {
                    m_textBlock.Text = ((Char)(iPos + 64)).ToString();
                } else {
                    m_textBlock.Text = (eSymbolType == SudControl.SymbolTypeE.Number || (iSqrSize == 3 && eSymbolType == SudControl.SymbolTypeE.LetterFor16x16)) ? (iPos + 1).ToString() : ((Char)(iPos + (int)'A')).ToString();
                }
                if (ActualWidth != 0) {
                    ResetFontSize();
                }
            }

            /// <summary>
            /// Recompute the font size when the value picker size changed
            /// </summary>
            /// <param name="sizeInfo"> New size</param>
            protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo) {
                ResetFontSize();
                base.OnRenderSizeChanged(sizeInfo);
            }

            /// <summary>
            /// Called when a cell is clicked
            /// </summary>
            /// <param name="sender">   Sender object</param>
            /// <param name="e">        Event parameter</param>
            void m_textBlock_MouseUp(object sender, MouseButtonEventArgs e) {
                int     iSqrSize;
                int     iPos;
                int     iValBit;
                
                e.Handled   = true;
                iSqrSize    = (m_valPickerCtl.BoardType == BoardTypeE.v9x9) ? 3 : 4;
                iPos        = m_tCellPos.Y * iSqrSize + m_tCellPos.X;
                if (e.ChangedButton == MouseButton.Left) {
                    m_valPickerCtl.OnValueSelected(new ValueSelectedEventArg(iPos + 1));
                } else if (e.ChangedButton == MouseButton.Right) {
                    iValBit = 1 << iPos;
                    if ((m_valPickerCtl.m_iRedMarkerMask & iValBit) != 0) {
                        m_valPickerCtl.m_iRedMarkerMask &= ~iValBit;
                    } else if ((m_valPickerCtl.m_iGreenMarkerMask & iValBit) != 0) {
                        m_valPickerCtl.m_iGreenMarkerMask &= ~iValBit;
                    } else if ((m_valPickerCtl.m_iBlueMarkerMask & iValBit) != 0) {
                        m_valPickerCtl.m_iBlueMarkerMask &= ~iValBit;
                    } else {
                        switch(Keyboard.Modifiers) {
                        case ModifierKeys.Shift:
                            m_valPickerCtl.m_iBlueMarkerMask |= iValBit;
                            break;
                        case ModifierKeys.Control:
                            m_valPickerCtl.m_iRedMarkerMask |= iValBit;
                            break;
                        default:
                            m_valPickerCtl.m_iGreenMarkerMask |= iValBit;
                            break;
                        }
                    }
                    RefreshState();
                }
            }
        } // Class
        
        #endregion
        
        #region Dependency Properties

        /// <summary>Defines the SymbolType dependency property</summary>
        public static readonly DependencyProperty   SymbolTypeProperty = SudControl.SymbolTypeProperty.AddOwner(typeof(ValPickerControl),
                                                                                                                new PropertyMetadata(SudControl.SymbolTypeE.Number, new PropertyChangedCallback(OnCellStateChanged)));
        /// <summary>Defines the BoardType dependency property</summary>
        public static readonly DependencyProperty   BoardTypeProperty = DependencyProperty.Register("BoardType",
                                                                                                    typeof(BoardTypeE),
                                                                                                    typeof(ValPickerControl),
                                                                                                    new PropertyMetadata(BoardTypeE.v9x9, new PropertyChangedCallback(OnPickerChanged)));
        /// <summary>Defines the ValidValueBrush dependency property</summary>
        public static readonly DependencyProperty   ValidValueBrushProperty = DependencyProperty.Register("ValidValueBrush",
                                                                                                          typeof(Brush),
                                                                                                          typeof(ValPickerControl),
                                                                                                          new PropertyMetadata(new SolidColorBrush(Color.FromRgb(132, 85, 83)), new PropertyChangedCallback(OnCellStateChanged)));
        /// <summary>Defines the UnvalidValueBrush dependency property</summary>
        public static readonly DependencyProperty   UnvalidValueBrushProperty = DependencyProperty.Register("UnvalidValueBrush",
                                                                                                            typeof(Brush),
                                                                                                            typeof(ValPickerControl),
                                                                                                            new PropertyMetadata(new SolidColorBrush(Color.FromRgb(191, 141, 137)), new PropertyChangedCallback(OnCellStateChanged)));

        #endregion
        
        #region Fields
        
        /// <summary>
        /// Delegates for ValueSelected event
        /// </summary>
        /// <param name="sender">           Sender object</param>
        /// <param name="e">                Event arguments</param>
        public delegate void                ValueSelectedHandler(object sender, ValueSelectedEventArg e);
        /// <summary>Event trigger when a valid value is selected</summary>
        public event ValueSelectedHandler   ValueSelected;
        /// <summary>List of valid value (1 bit per value)</summary>
        private int                         m_iValMask;
        /// <summary>Mask for green marker (1 bit per value)</summary>
        private int                         m_iGreenMarkerMask;
        /// <summary>Mask for blue marker (1 bit per value)</summary>
        private int                         m_iBlueMarkerMask;
        /// <summary>Mask for red marker (1 bit per value)</summary>
        private int                         m_iRedMarkerMask;
        /// <summary>Array of cells</summary>
        private Cell[,]                     m_arrCells;
        
        #endregion
        
        #region Ctor
        
        /// <summary>
        /// Class ctor
        /// </summary>
        public ValPickerControl() {
            InitializeComponent();
            m_iValMask          = -1;
            m_iRedMarkerMask    = 0;
            m_iGreenMarkerMask  = 0;
            m_iBlueMarkerMask   = 0;
            m_arrCells          = null;
            RefreshPicker();
        }

        #endregion
        
        #region Properties Notification

        /// <summary>
        /// Notification when cell state need to be refreshed
        /// </summary>
        /// <param name="o">    Dependency object</param>
        /// <param name="e">    Event argument</param>
        protected static void OnCellStateChanged(DependencyObject o, DependencyPropertyChangedEventArgs e) {
            ValPickerControl    valPickerCtl;

            valPickerCtl = o as ValPickerControl;
            if (valPickerCtl != null) {
                valPickerCtl.RefreshCellStates();
            }
        }

        /// <summary>
        /// Notification when value picker control need to be rebuild
        /// </summary>
        /// <param name="o">    Dependency object</param>
        /// <param name="e">    Event argument</param>
        protected static void OnPickerChanged(DependencyObject o, DependencyPropertyChangedEventArgs e) {
            ValPickerControl    valPickerCtl;

            valPickerCtl = o as ValPickerControl;
            if (valPickerCtl != null) {
                valPickerCtl.RefreshPicker();
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Value bit mask. 1 bit per allowed value
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public int ValueMask {
            get {
                return(m_iValMask);
            }
            set {
                m_iValMask = value;
            }
        }

        /// <summary>
        /// Red marker bit mask. 1 bit per value
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public int RedMarkerMask {
            get {
                return(m_iRedMarkerMask);
            }
            set {
                m_iRedMarkerMask = value;
            }
        }
        
        /// <summary>
        /// Green marker bit mask. 1 bit per value
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public int GreenMarkerMask {
            get {
                return(m_iGreenMarkerMask);
            }
            set {
                m_iGreenMarkerMask = value;
            }
        }

        /// <summary>
        /// Blue marker bit mask. 1 bit per value
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public int BlueMarkerMask {
            get {
                return(m_iBlueMarkerMask);
            }
            set {
                m_iBlueMarkerMask = value;
            }
        }

        /// <summary>
        /// Type of symbols used to represent values (digits, letters or symbols)
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Browsable(true)]
        [Bindable(true)]
        [DefaultValue(false)]
        [Category("Behavior")]
        [Description("Type of symbol use to draw the board")]
        public SudControl.SymbolTypeE SymbolType {
            get {
                return((SudControl.SymbolTypeE)GetValue(SymbolTypeProperty));
            }
            set {
                SetValue(SymbolTypeProperty, value);
            }
        }

        /// <summary>
        /// Type of board (9x9 or 16x16)
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Browsable(true)]
        [Bindable(true)]
        [DefaultValue(false)]
        [Category("Behavior")]
        [Description("Type of board: 9x9 or 16x16")]
        public BoardTypeE BoardType {
            get {
                return((BoardTypeE)GetValue(BoardTypeProperty));
            }
            set {
                SetValue(BoardTypeProperty, value);
            }
        }

        /// <summary>
        /// Brush used to draw valid values
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Browsable(true)]
        [Bindable(true)]
        [DefaultValue(false)]
        [Category("Behavior")]
        [Description("Brush used to draw valid values")]
        public Brush ValidValueBrush {
            get {
                return((Brush)GetValue(ValidValueBrushProperty));
            }
            set {
                SetValue(ValidValueBrushProperty, value);
            }
        }

        /// <summary>
        /// Brush used to draw invalid values
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Browsable(true)]
        [Bindable(true)]
        [DefaultValue(false)]
        [Category("Behavior")]
        [Description("Brush used to draw invalid values")]
        public Brush UnvalidValueBrush {
            get {
                return((Brush)GetValue(UnvalidValueBrushProperty));
            }
            set {
                SetValue(UnvalidValueBrushProperty, value);
            }
        }

        #endregion        

        #region Refresh
        
        /// <summary>
        /// Refresh the board contents
        /// </summary>
        private void RefreshPicker() {
            int             iSqrSize;
            int             iLineSize;
            CellPos         tCellPos;
            Cell            cell;
            
            gridRoot.Children.Clear();
            gridRoot.ColumnDefinitions.Clear();
            gridRoot.RowDefinitions.Clear();
            iSqrSize    = (BoardType == BoardTypeE.v9x9) ? 3 : 4;
            iLineSize   = iSqrSize * iSqrSize;
            m_arrCells  = new Cell[iSqrSize, iSqrSize];
            for (int i = 0; i < iSqrSize; i++) {
                gridRoot.ColumnDefinitions.Add(new ColumnDefinition());
                gridRoot.RowDefinitions.Add(new RowDefinition());
            }
            tCellPos = new CellPos();
            for (tCellPos.Y = 0; tCellPos.Y < iSqrSize; tCellPos.Y++) {
                for (tCellPos.X = 0; tCellPos.X < iSqrSize; tCellPos.X++) {
                    cell                                = new Cell(this, tCellPos);
                    m_arrCells[tCellPos.X, tCellPos.Y]  = cell;
                    Grid.SetColumn(cell, tCellPos.X);
                    Grid.SetRow(cell, tCellPos.Y);
                    gridRoot.Children.Add(cell);
                }
            }
        }
        
        /// <summary>
        /// Refresh the state of all cells
        /// </summary>
        public void RefreshCellStates() {
            foreach (Cell cell in m_arrCells) {
                cell.RefreshState();
            }
        }
        
        #endregion

        #region Event Triggering
        
        /// <summary>
        /// Trigger the SelectedValue argument
        /// </summary>
        /// <param name="e">    Event argument</param>
        protected virtual void OnValueSelected(ValueSelectedEventArg e) {
            if (ValueSelected != null) {
                ValueSelected(this, e);
            }
        }
        
        #endregion
        
    } // Class
} // Namespace
