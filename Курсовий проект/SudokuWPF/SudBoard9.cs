﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sudoku2 {
    /// <summary>
    /// Implements a 9x9 sudoku board
    /// </summary>
    public class SudBoard9 : SudBoard {
        
        /// <summary>
        /// Class ctor
        /// </summary>
        public SudBoard9() : base(3) {
        }
        
        /// <summary>
        /// Copy ctor
        /// </summary>
        /// <param name="brd">  Template board</param>
        public SudBoard9(SudBoard9 brd) : this() {
            brd.CopyTo(this);
        }

        /// <summary>
        /// Clone the sudoku board
        /// </summary>
        /// <returns>
        /// Clone of the sudoku board
        /// </returns>
        public override SudBoard Clone() {
            return(new SudBoard9(this));
        }

        /// <summary>
        /// Checks if a value is valid for the specified cell. A valid value doesn't duplicate a value in a square, line or column.
        /// </summary>
        /// <param name="iPos">Position of the cell (0..max)</param>
        /// <param name="iVal">Value (1..max)</param>
        /// <returns>true if valid, false if not.
        /// </returns>
        protected override bool IsValueValid(int iPos, int iVal) {
            bool    bRetVal = false;
            int     iSqr;
            int     iLine;
            int     iCol;

#if DEBUG
            if (iVal <= 0 || iVal > m_iLineSize || iPos >= m_iBoardSize || m_arrBoardVal[iPos] != 0) {
                throw new System.ApplicationException("Coding error. Out of bound");
            }
#endif
            iSqr = m_arrPosToSqr[iPos];
            iLine   = iPos / 9;
            iCol    = iPos % 9;
            if (m_SqrValCount[iSqr, iVal]   == 0  &&
                m_LineValCount[iLine, iVal] == 0  &&
                m_ColValCount[iCol, iVal]   == 0) {
                bRetVal = true;
            }
            return(bRetVal);
        }
    } // Class
} // Namespace
