﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyCollectionBl
{
    public class MyCollection<TValue>:IMyCollection<TValue>,IEnumerable<TValue> 
    {
        private List<TValue> _list;
        public List<TValue> List
        {
            get { return _list; }
            set { _list = value; }
        }
        public TValue this[int i]
        {
            get { return _list[i]; }
            set { _list[i]=value; }
        }
        public MyCollection()
        {
            _list = new List<TValue>();
        }
        IEnumerator<TValue> IEnumerable<TValue>.GetEnumerator()
        {
            return _list.GetEnumerator();
        }
        public IEnumerator GetEnumerator()
        {
            return GetEnumerator();
        }
        public void AddValue(TValue choose)
        {
                _list.Add(choose);
        }
        public void SortCollection(IComparer<TValue> comparison, Func<TValue,bool> validateCount)
        {
            var query = _list.Where(validateCount);
            _list = query.ToList<TValue>();
            _list.Sort(0, _list.Count,comparison);
        }
        public override string ToString()
        {
            StringBuilder result = new StringBuilder("Array:");
            foreach (var value in _list)
                result.Append($"{value} ");
            return result.ToString();
        }
    }
}
