﻿using System;


namespace CollectionApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var firstExample = new CollectionManager<int>();
                var numbers = new int[] { 4, 5, 1, 255, 1, 4, 5 };
                firstExample.Generate(numbers);
                firstExample.Sort(med => med > 3);
                firstExample.Show();
                var secondExample = new CollectionManager<char>();
                var signs = new char[] { 'g', 't', 'q', 't' };
                secondExample.Generate(signs);
                secondExample.Sort(med => med > 'k');
                secondExample.Show();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.ReadKey();
            }
        }
    }
}
