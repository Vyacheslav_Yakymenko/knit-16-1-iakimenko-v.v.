﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication31
{
    class Program
    {
        abstract class Person
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }

            public Person(string name, string surname)
            {
                FirstName = name;
                LastName = surname;
            }

            public void Display()
            {
                Console.WriteLine(FirstName + " " + LastName);
            }
        }

        class Client : Person
        {
            public int Sum { get; set; }    // сумма на счету

            public Client(string name, string surname, int sum)
                : base(name, surname)
            {
                Sum = sum;
            }
        }

        class Employee : Person
        {
            public string Position { get; set; } // должность

            public Employee(string name, string surname, string position)
                : base(name, surname)
            {
                Position = position;
            }
        }
    }
}
